import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'serviceDurationFormat'})
export class ServiceDurationFormatPipe implements PipeTransform {
    transform(value: string): string {
        let response = '';
        let time = value.split(':');
        if (time.length > 1) {
            if (parseInt(time[1], null) > 0) {
                response = value;
            } else {
                response = time[0];
            }
        } else {
            response = value;
        }
        return response;
    }
}
