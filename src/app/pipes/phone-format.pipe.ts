import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'phoneFormat'})
export class PhoneFormatPipe implements PipeTransform {
    transform(value: any): any {
        let response = '';
        let numbers = value.replace(/\D/g, '');
        let char = {0:'(',3:') ',6:' - '};
        for (var i = 0; i < numbers.length; i++) {
            response += (char[i]||'') + numbers[i];
        }
        return response;
    }
}
