import { Pipe, PipeTransform } from '@angular/core';
import {CommonService} from '../services/common.service';

@Pipe({name: 'coastFormat'})
export class CoastFormatPipe implements PipeTransform {
    transform(value: any): any {
        return CommonService.formatCoast(value);
    }
}
