import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Globalization } from '@ionic-native/globalization/ngx';
import { MomentModule } from 'ngx-moment';
import { ImageCropperModule } from 'ngx-image-cropper';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Network } from '@ionic-native/network/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuardService } from './services/auth-guard.service';
import { SocketService } from './services/socket-service.service';
import { AuthGuardNoDemoService } from './services/auth-guard-no-demo.service';
import { ApiService } from './services/api-service.service';
import { PushNotificationsService } from './services/push-notifications.service';
import { CommonService } from './services/common.service';
import { LocalNotificationService } from './services/local-notification.service';
import { SharedService } from './services/shared.service';
import { HttpClientModule } from '@angular/common/http';
import { HintComponent } from './snippets/popup/hint/hint.component';
import { SearchComponent } from './snippets/popup/search/search.component';
import { SearchAddressComponent } from './snippets/popup/search/search-address/search-address.component';
import { SearchServiceComponent } from './snippets/popup/search/search-service/search-service.component';
import { SearchClientsComponent } from './snippets/popup/search/search-clients/search-clients.component';
import { SidebarComponent } from './snippets/sidebar/sidebar.component';
import { AlertComponent } from './snippets/popup/alert/alert.component';

import { AngularFireModule } from '@angular/fire';
import {AngularFireDatabaseModule, AngularFireDatabase} from '@angular/fire/database';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireMessaging} from '@angular/fire/messaging';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';

export const firebaseConfig = {
    apiKey: 'AIzaSyCyzuhb-5rnV006HAv4PYGySbs-2PoJAP4',
    authDomain: 'localhost',
    databaseURL: 'https://flaber-48461.firebaseio.com',
    projectId: 'flaber-48461',
    storageBucket: 'flaber-48461.appspot.com',
    messagingSenderId: '918616834002',
    // appId: '1:918616834002:android:2d88c4a73bcd6113'
    appId: '1:918616834002:web:5d15bb61e411affe66d20b'
};

@NgModule({
  declarations: [AppComponent, HintComponent, SidebarComponent, AlertComponent, SearchComponent,
      SearchAddressComponent, SearchServiceComponent, SearchClientsComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, MomentModule,
      ImageCropperModule, FormsModule, AngularFireModule.initializeApp(firebaseConfig),
      AngularFireDatabaseModule,
      AngularFireAuthModule,
      // PushNotificationsService
  ],
  providers: [
    StatusBar, SplashScreen, Globalization, AuthGuardService, ApiService, CommonService,
    SharedService, AuthGuardNoDemoService, AngularFireDatabase, AngularFireMessaging, SocketService,
      LocalNotifications, BackgroundMode, Network, PushNotificationsService, Push,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
