import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthGuardNoDemoService } from './services/auth-guard-no-demo.service';

const routes: Routes = [
  // { path: '', redirectTo: 'personal-information', pathMatch: 'full' },
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', loadChildren: './pages/main/main.module#MainPageModule', canActivate: [AuthGuardService] },
  { path: 'login', loadChildren: './pages/auth/login.module#LoginPageModule' },
  // { path: 'personal-information', loadChildren: './pages/personal-information/personal-information.module#PersonalInformationPageModule' },
  { path: 'personal-information', loadChildren: './pages/personal-information/personal-information.module#PersonalInformationPageModule', canActivate: [AuthGuardNoDemoService] },
  // { path: 'services-page', loadChildren: './pages/personal-information/services-page/services-page.module#ServicesPagePageModule' },
  { path: 'services-page', loadChildren: './pages/personal-information/services-page/services-page.module#ServicesPagePageModule', canActivate: [AuthGuardNoDemoService] },
  { path: 'select-category', loadChildren: './pages/personal-information/select-category/select-category.module#SelectCategoryPageModule', canActivate: [AuthGuardNoDemoService] },
  { path: 'profile-main', loadChildren: './pages/profile/profile-main/profile-main.module#ProfileMainPageModule', canActivate: [AuthGuardService] },
  { path: 'profile-personal', loadChildren: './pages/profile/profile-personal/profile-personal.module#ProfilePersonalPageModule', canActivate: [AuthGuardService] },
  { path: 'profile-billing', loadChildren: './pages/profile/profile-billing/profile-billing.module#ProfileBillingPageModule', canActivate: [AuthGuardService] },
  { path: 'view-work/:id', loadChildren: './pages/works/view-work/view-work.module#ViewWorkPageModule', canActivate: [AuthGuardService] },
  { path: 'works-gallery', loadChildren: './pages/works/works-gallery/works-gallery.module#WorksGalleryPageModule', canActivate: [AuthGuardService] },
  { path: 'new-work', loadChildren: './pages/works/new-work/new-work.module#NewWorkPageModule', canActivate: [AuthGuardService] },
  { path: 'order-delete', loadChildren: './pages/works/order-delete/order-delete.module#OrderDeletePageModule', canActivate: [AuthGuardService] },
  { path: 'settings-main', loadChildren: './pages/settings/settings-main/settings-main.module#SettingsMainPageModule', canActivate: [AuthGuardService] },
  { path: 'settings-password-change', loadChildren: './pages/settings/settings-password-change/settings-password-change.module#SettingsPasswordChangePageModule', canActivate: [AuthGuardService] },
  { path: 'settings-lang', loadChildren: './pages/settings/settings-lang/settings-lang.module#SettingsLangPageModule', canActivate: [AuthGuardService] },
  { path: 'settings-payment', loadChildren: './pages/settings/settings-payment/settings-payment.module#SettingsPaymentPageModule', canActivate: [AuthGuardService] },
  { path: 'my-services-main', loadChildren: './pages/my-services/my-services-main/my-services-main.module#MyServicesMainPageModule', canActivate: [AuthGuardService] },
  { path: 'record-info/:id', loadChildren: './pages/record-info/record-info.module#RecordInfoPageModule', canActivate: [AuthGuardService] },
  { path: 'records-archive', loadChildren: './pages/profile/records-archive/records-archive.module#RecordsArchivePageModule' },
  { path: 'notifications', loadChildren: './pages/notifications/notifications.module#NotificationsPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
