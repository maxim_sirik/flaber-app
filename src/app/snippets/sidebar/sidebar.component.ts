import {Component, HostBinding, OnInit} from '@angular/core';
import {SharedService} from '../../services/shared.service';
import {CommonService} from '../../services/common.service';
import {ApiService} from '../../services/api-service.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
    public show = false;
    public worker: any;
    public menuList = {
        top: [
            {
                title: this.comS.lang['sidebar_my_services'],
                left_icons: 'icon-left__middle icon-options',
                right_icons: 'icon-right__middle icon-chevron-right',
                action: 'link',
                route: '/my-services-main'
            },
            {
                title: this.comS.lang['sidebar_my_works'],
                left_icons: 'icon-left__middle icon-case',
                right_icons: 'icon-right__middle icon-chevron-right',
                action: 'link',
                route: '/works-gallery'
            },
            {
                title: this.comS.lang['sidebar_profile'],
                left_icons: 'icon-left__middle icon-user',
                right_icons: 'icon-right__middle icon-chevron-right',
                action: 'link',
                route: '/profile-main'
            },
            {
                title: this.comS.lang['sidebar_my_schedule'],
                left_icons: 'icon-left__middle icon-calendar',
                right_icons: 'icon-right__middle icon-chevron-right',
                action: 'link',
                route: '/main/schedule'
            },
            {
                title: this.comS.lang['sidebar_reviews'],
                left_icons: 'icon-left__middle icon-star',
                right_icons: 'icon-right__middle icon-chevron-right',
                action: 'link',
                route: ''
            },
            {
                title: this.comS.lang['sidebar_settings'],
                left_icons: 'icon-left__middle icon-settings',
                right_icons: 'icon-right__middle icon-chevron-right',
                action: 'link',
                route: '/settings-main'
            }
        ],
        bottom: [
            {
                title: this.comS.lang['sidebar_help'],
                left_icons: 'icon-left__middle icon-help',
                right_icons: 'icon-right__middle icon-chevron-right',
                action: 'link',
                route: ''
            },
            {
                title: this.comS.lang['sidebar_create_company'],
                left_icons: 'icon-left__middle icon-add-file',
                right_icons: 'icon-right__middle icon-chevron-right',
                action: 'link',
                route: ''
            },
            {
                title: this.comS.lang['sidebar_logout'],
                left_icons: 'icon-left__middle icon-exit',
                right_icons: 'icon-right__middle icon-chevron-right',
                action: 'logout',
                route: ''
            }
        ]
    };
    public notifications = {
       messages: 3,
       notifications: 2
    };

    public user = {
        firstName: 'Надежда',
        lastName: 'Кадышева',
        city: 'Харьков'
    };

    @HostBinding('class') sidebarClass = 'app_sidebar_index';

    constructor(public shared: SharedService,
                public comS: CommonService,
                public apiS: ApiService) {
        this.shared.sidebarStatusChanged.subscribe((data) => {
            this.show = data ? data : false;
            if (this.show) {
                this.getWorkerInfo();
            }
            // this.comS.setStatusBarStyle(this.show ? '#F5F6F7' : '#fff');
        });



    }

  ngOnInit() {

  }

  getWorkerInfo() {
      if (!this.comS.updateProfileWorker()) {
          // this.worker = this.comS.workerForProfile;
          this.apiS.get_my_profile().subscribe(
              success => {
                  if (success['status']) {
                      this.comS.workerForProfile = success['text']['info'];
                      localStorage.setItem('worker', JSON.stringify(success['text']['info']));

                      this.comS.updateProfileWorker();
                  }
              }, error => {
                  if (error.error) {
                      this.comS.showHint(error.error.text.errors);
                  }
              }
          );
      }

      console.log(this.worker, 'this.worker');
  }

  toNotifications(page: any) {
        this.comS.toggleSidebar(false);
        this.comS.pushPage(page);
  }

  make_action(item) {
      if (item.action === 'logout') {
        this.apiS.logout().subscribe(
            success => {
                if (success['status']) {
                    this.show = false;
                    this.comS.logoutUser();
                }
            }, error => {
                // показать ошибку логаута
              console.log(error);
            }
        );
      } else if (item.action === 'link') {
        this.comS.pushPage(item.route);
        this.show = false;
      }
  }

}
