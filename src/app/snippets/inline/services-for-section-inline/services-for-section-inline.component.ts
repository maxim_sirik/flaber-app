import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {CommonService} from '../../../services/common.service';

@Component({
  selector: 'app-services-for-section-inline',
  templateUrl: './services-for-section-inline.component.html',
  styleUrls: ['./services-for-section-inline.component.scss'],
})
export class ServicesForSectionInlineComponent implements OnInit {

    @Input() services: any;
    @Output() serviceChanged = new EventEmitter();

    constructor(public comS: CommonService) { }

    ngOnInit() {}

}
