import {Component, OnInit, Input, Output, EventEmitter, OnDestroy} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommonService} from '../../../services/common.service';
import {SharedService} from '../../../services/shared.service';
import {ApiService} from '../../../services/api-service.service';

@Component({
  selector: 'app-new-record-inline',
  templateUrl: './new-record-inline.component.html',
  styleUrls: ['./new-record-inline.component.scss']
})
export class NewRecordInlineComponent implements OnInit, OnDestroy {
	@Input() section: any;
	@Input() provider: any;
	@Input() duration: any;
	@Output() serviceDataChanged = new EventEmitter();
	@Output() initDurationPicker = new EventEmitter();
    public serviceForm: FormGroup;
    public focusObject = null;
    public focusOnField = false;
    public serviceId: any;
    public isCustomService = false;
    public isEditingProvider = false;
    public onlineRegister: any = true;
    public validateClicked = false;
    public services = [
        {
            formName: 'service_name',
            labelClassName: 'service_name',
            type: 'text',
            placeholder: 'Название услуги',
            hasRestriction: false,
            restriction: 10000
        },
        {
            formName: 'time',
            labelClassName: 'time',
            type: 'text',
            placeholder: 'Продолжительность',
            hasRestriction: false,
            // noNeedToValidate: true,
            restriction: 10000
        },
        {
            formName: 'cost',
            labelClassName: 'cost',
            type: 'number',
            placeholder: 'Стоимость',
            hasRestriction: false,
            restriction: 10000
        },
        {
            formName: 'describe',
            labelClassName: 'describe',
            type: 'text',
            placeholder: 'Описание',
            hasRestriction: true,
            restriction: 300
        }
    ];

    constructor(public fb: FormBuilder, public comS: CommonService, public shared: SharedService, public apiS: ApiService) {
        this.serviceForm = fb.group({
            'service_name': ['', Validators.required],
            'time': ['', Validators.required],
            'cost': ['', Validators.required],
            'describe': ['']
        });

        this.shared.serviceNameOnSearchSelected.subscribe(
            data => {
                if (data['name']) {
                    this.serviceForm.controls['service_name'].setValue(data['name']);
                    this.serviceId = data['id'];
                    this.isCustomService = (data['id'] === null);
                }
            }
        );

        this.comS.updateWorker();

        this.onChanges();
	}

    onChanges(): void {
        this.serviceForm.get('describe').valueChanges.subscribe(val => {
            setTimeout(() => {
                this.comS.resizeTextArea();
            }, 300);
        });
        this.serviceForm.get('time').valueChanges.subscribe(val => {
            let temp = new Date(this.duration);
        });
    }

	ngOnInit() {
        this.isEditingProvider = this.provider !== null;
        console.log(this.provider, 'this.provider init');
        if (this.isEditingProvider) {
            if (this.provider['name']) {
                this.serviceForm.controls['service_name'].setValue(this.provider['name']);
            }
            if (this.provider['price']) {
                this.serviceForm.controls['cost'].setValue(CommonService.formatCoast(this.provider['price']));
            }
            if (this.provider['description']) {
                this.serviceForm.controls['describe'].setValue(this.provider['description']);
            }
            if (this.provider['can_record_online'] !== null) {
                this.onlineRegister = this.provider['can_record_online'];
            }
        }
	}

	ngOnDestroy() {
        this.shared.serviceNameOnSearch.next({});
    }

	cancelEnter() {
		this.serviceDataChanged.emit(null);
	}

	saveService() {
        this.serviceForm.controls['time'].setValue(this.duration ? this.comS.formatDateTime(this.duration, 'H:mm') : '');
        this.validateClicked = true;
        console.log(this.comS.formatDateTime(this.duration, 'H:mm'), this.serviceForm.controls, 'this.serviceForm');
        if (this.serviceForm.valid) {
            if (parseFloat(this.serviceForm.controls['cost'].value) > 0) {
                if (!this.isEditingProvider) { // добавление нового провайдера
                    if (this.isCustomService) {
                        this.addCustomService();
                    } else {
                        this.addProvider();
                    }
                } else { // редактирование уже существующего
                    this.updateProvider();
                }
            } else {
                this.comS.showHint(this.comS.lang['error_zero_price']);
            }
		}
	}

    fieldOnFocus(service: any, ev: any) {
        if (service['formName'] === 'category') {
            this.comS.pushPage('/select-category');
        } else {
            this.focusObject = service;
            this.focusOnField = true;
        }
    }

    fieldOutOfFocus(formName) {
        this.focusOnField = false;
        setTimeout(() => {
            if (!this.focusOnField) {
                this.focusObject = null;
            }
        }, 200);
    }

    showSearch() {
		this.comS.showSearch({
			text: this.serviceForm.controls['service_name'].value,
			show: true,
			searchComponent: 'service',
			category: this.section ? this.section['id'] : null
		});
    }

    openPicker(selector) {
        if (!this.duration) {
            let temp_d = new Date();
            temp_d.setHours(0, 5, 0, 0);
            this.initDurationPicker.emit(temp_d.toISOString());
        }
        setTimeout(() => {
            CommonService.showPicker(selector);
        }, 50);
    }

    clearFieldValue(field) {
        field.setValue('');
    }

    addProvider() {
        let provider = {
            'section_id': this.section['id'],
            'service_id': this.serviceId,
            'worker_id': this.comS.workerInfo['id'],
            'time': this.serviceForm.controls['time'].value,
            'price':  parseFloat(this.serviceForm.controls['cost'].value).toFixed(2),
            'description':  this.serviceForm.controls['describe'].value,
            'can_record_online': this.onlineRegister ? 1 : 0
        };

        this.apiS.add_provider(provider).subscribe(
            success => {
                if (success['status']) {
                    this.serviceDataChanged.emit({service: success['text']['service_provider'], isCustom: false});
                }
            }, error => {
                if (error['error']) {
                    this.comS.showHint(error['error']['text']['errors']);
                }
            }
        );
    }

    addCustomService() {
        let service = {
            'section_id': this.section['id'],
            'name': this.serviceForm.controls['service_name'].value,
            'worker_id': this.comS.workerInfo['id'],
            'time': this.serviceForm.controls['time'].value,
            'price':  parseFloat(this.serviceForm.controls['cost'].value).toFixed(2),
            'description':  this.serviceForm.controls['describe'].value,
            'can_record_online': this.onlineRegister ? 1 : 0
        };

        this.apiS.add_new_service(service).subscribe(
            success => {
                if (success['status']) {
                    this.serviceDataChanged.emit({service: success['text']['service'], isCustom: true});
				}
            }, error => {
                if (error['error']) {
                    this.comS.showHint(error['error']['text']['errors']);
                }
            }
        );
	}

	updateProvider() {
        let service = {
            'section_id': this.section['id'],
            'name': this.serviceForm.controls['service_name'].value,
            'worker_id': this.comS.workerInfo['id'],
            'time': this.serviceForm.controls['time'].value,
            'price':  parseFloat(this.serviceForm.controls['cost'].value).toFixed(2),
            'description':  this.serviceForm.controls['describe'].value,
            'can_record_online': this.onlineRegister ? 1 : 0
        };
        this.apiS.update_provider(this.provider['provider_id'], service).subscribe(
            success => {
                if (success['status']) {
                    this.serviceDataChanged.emit({service: success['text']['service'], isCustom: false, isUpdated: true});
                }
            }, error => {
                if (error['error']) {
                    this.comS.showHint(error['error']['text']['errors']);
                }
            }
        );
    }

    getIntFieldValue(field: any) {
        return parseInt(field, null);
    }

    expandTextArea(ev: any) {
        console.log(ev, ev.target.offsetHeight, ev.target.clientHeight, 'ev.target.scrollHeight');
        // ev.target.style.height = '5px';
        ev.target.style.height = (ev.target.scrollHeight) + 'px';
    }



}
