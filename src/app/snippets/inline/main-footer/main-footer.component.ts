import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ApiService} from '../../../services/api-service.service';
import {SharedService} from '../../../services/shared.service';
import {CommonService} from '../../../services/common.service';

@Component({
  selector: 'app-main-footer',
  templateUrl: './main-footer.component.html',
  styleUrls: ['./main-footer.component.scss'],
})
export class MainFooterComponent implements OnInit {
    @Input() tab: any;
    public tabs = {
        left: [
            {
                route: '/main/records',
                classNames: 'menu-bottom-icon menu-bottom__calendar icon-calendar',
                title: this.comS.lang['tab_title_records'],
                tab: 'records',
                headerTitle: this.comS.lang['nav_title_records']
            },
            {
                route: '/main/messages',
                classNames: 'menu-bottom-icon menu-bottom__messages',
                child: '<i class="fal fa-comment-alt-lines"></i>',
                title: this.comS.lang['tab_title_chat'],
                tab: 'messages',
                headerTitle: this.comS.lang['nav_title_messages']
            },

        ],
        right: [
            {
                route: '/main/contacts',
                classNames: 'menu-bottom-icon menu-bottom__user icon-user',
                title: this.comS.lang['tab_title_contacts'],
                tab: 'contacts',
                headerTitle: this.comS.lang['nav_title_contacts']
            },
            {
                route: '/notifications',
                classNames: 'menu-bottom-icon menu-bottom__schedule icon-notification',
                title: this.comS.lang['tab_title_schedule'],
                tab: 'schedule',
                headerTitle: this.comS.lang['nav_title_schedule']
            }
        ]
    };
    constructor(public shared: SharedService,
                public comS: CommonService,
                public apiS: ApiService) {

        // this.shared.mainTabChanged.subscribe((data) => {
        //     this.currentTab = data;
        // });

        // this.shared.showNewRecord.subscribe(
        //     data => {
        //         this.showNewRecordModal = data['show'] !== null ? data['show'] : false;
        //     }
        // );
        //
        // this.shared.quickAccessOverlayShow.subscribe(
        //     overlay => {
        //         this.showQuickAccess = typeof overlay['show'] !== 'undefined' ? overlay['show'] : false;
        //     }
        // );

        // this.currentTabObj = this.tabs.left[0];


    }

    ngOnInit() {}

    selectTab(obj) {
        this.comS.pushPage(obj['route']);
        // this.currentTab = obj['tab'];
        // this.changedTab = obj['tab'];
        // this.currentTabObj = obj;
    }

    openNewRecord() {
        this.shared.newRecord.next({show: true});
    }

}
