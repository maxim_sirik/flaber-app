import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Draggable } from '@shopify/draggable';
import {CommonService} from '../../../../services/common.service';
import {SharedService} from '../../../../services/shared.service';
import {Subscription} from 'rxjs';
import {ApiService} from '../../../../services/api-service.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
})
export class CalendarComponent implements OnInit {

  @Input() calendar: any;
  @Output() calendarClosed = new EventEmitter();
  @Output() dataSelected = new EventEmitter();
  @Output() dataSwiped = new EventEmitter();
  public prevClientY: any;
  public prevClientX: any;
  public delta: any;
  public deltaX: any;
  public prevDeltaX: any;
  public onClickAction = 0;
  public onClickActionStarted = false;
  public onClickActionCallback: any;
  public dragStatus: any;
  public dragStatusX: any;
  public swipeDays: any = [];
  public busyDays: any = [];

  constructor(public comS: CommonService,
              private shared: SharedService,
              private apiS: ApiService) {
      this.initCalendar();
  }

  initCalendar(init: boolean = true) {
    if (this.calendar) {
        this.onWeekSwipe();
        this.getBusyDays();
        this.calendar.month = this.comS.getCalendar(this.calendar.selectedDate);
        this.getSwipeDays(init);
    } else {
      setTimeout(() => {
        this.initCalendar(init);
      }, 100);
    }
  }

  ngOnInit() {
  }

  onWeekSwipe(ev: any = null) { // 4 - слева на право; 2 - справа на лево
      this.calendar.weekDelta = this.calendar.weekDelta + (ev ? (ev.direction === 4 ? -1 : (ev.direction === 2 ? 1 : 0 )) : 0) * 7;
      let selectedWeek = this.comS.getMomentObject(this.calendar.selectedDate).add(this.calendar.weekDelta, 'd');
      this.calendar.week = this.comS.getSelectedWeekDays(selectedWeek.format('YYYY-MM-DD'));
  }
  onMonthSwipe(ev: any = null) { // 4 - слева на право; 2 - справа на лево
      console.log(ev, 'onMonthSwipe');
      this.calendar.monthDelta = this.calendar.monthDelta + (ev ? (ev.direction === 4 ? -1 : (ev.direction === 2 ? 1 : 0 )) : 0) * 1;
      this.getMonthData();
  }
  getMonthData() {
      let selectedMonth = this.comS.getMomentObject(this.calendar.selectedMonth).add(this.calendar.monthDelta, 'M');
      this.calendar.selectedMonth = selectedMonth.format('YYYY-MM');
      this.calendar.monthDelta = 0;
      this.calendar.month = this.comS.getCalendar(this.calendar.selectedMonth);
      this.getBusyDays();
  }
  changeMonth(delta: any) {
      this.calendar.monthDelta += delta;
      this.getMonthData();
  }
  swipeUp(ev: any) {
      this.calendar.currentState = 'week';
      if (this.calendar.currentState === 'no_calendar') {
        this.calendarClosed.emit(this.calendar);
      }
  }
  swipeDown(ev: any) {
      this.calendar.currentState = 'month';
  }

  getBusyDays(date = null) {
      if (this.calendar) {
          this.apiS.get_busy_days(this.calendar.selectedMonth).subscribe(
              success => {
                  this.busyDays = success['status'] ? success['text']['result'] : [];
              }, error => {
                  if (error.error) {
                      this.comS.showHint(error.error.text.errors);
                  }
              }
          );
      }
  }

  selectDate(day: any, source: any) {
    if (typeof day === 'object') {
      this.calendar.selectedDate = day.fullDate ? day.fullDate : day.format('YYYY-MM-DD');
      this.calendar.selectedMonth = this.comS.getMomentObject(this.calendar.selectedDate).format('YYYY-MM');
      this.calendar.monthDelta = 0;
      this.calendar.weekDelta = 0;
      this.initCalendar();
      this.dataSelected.emit(this.calendar.selectedDate);
      if (source === 'month') {
          this.dragEndHandler(-1);
      }
    }
  }

  dragMonthStart(ev: any, direction: any) {
      this.dragStatus = 'drag';
      // this.prevClientY = ev.changedTouches[0].clientY;
      this.dragEndHandler(direction === 'down' ? 1 : -1);
  }

  dragMonthCalendar(ev: any) {
    let dragItem = document.querySelector('.calendar-monthly') as HTMLElement;
    if (dragItem) {
        if (this.prevClientY) {
            this.delta = ev.changedTouches[0].clientY - this.prevClientY;
            this.prevClientY = ev.changedTouches[0].clientY;
            let transformY = parseInt(window.getComputedStyle(dragItem, null).getPropertyValue('transform').split(',')[5], null);
            let blockHeight = parseInt(window.getComputedStyle(dragItem, null).getPropertyValue('height'), null);
            if (this.delta > 0) {
                if (transformY < 0) {
                    dragItem.style.transform = 'matrix(1, 0, 0, 1, 0, ' + (transformY + this.delta)  + ')';
                    dragItem.style.opacity = 1 + (transformY + this.delta) / blockHeight + '';
                }
            } else {
                if (transformY >= (blockHeight * -1)) {
                    dragItem.style.transform = 'matrix(1, 0, 0, 1, 0, ' + (transformY + this.delta) + ')';
                    dragItem.style.opacity = 1 + (transformY + this.delta) / blockHeight + '';
                }
            }
        } else {
            this.prevClientY = ev.changedTouches[0].clientY;
        }
    }
  }

    dragEndHandler(ev: any) {
        if (ev && typeof ev === 'number') {
            this.delta = ev;
            this.dragStatus = 'drag';
        }
        if (this.dragStatus === 'drag') {
            this.dragStatus = 'finish';
            let dragItem = document.querySelector('.calendar-monthly') as HTMLElement;
            let blockHeight = parseInt(window.getComputedStyle(dragItem, null).getPropertyValue('height'), null);
            if (this.delta > 0) {
                dragItem.style.transform = 'matrix(1, 0, 0, 1, 0, 0)';
                dragItem.style.opacity = '1';
            } else if (this.delta < 0) {
                dragItem.style.transform = 'translateY(' + (blockHeight * -1) + 'px)';
                dragItem.style.opacity = '0';
            }
        }
    }

  dragWeekDayStart(ev: any) {
      // setTimeout(() => {
      //     // console.log('начало нажатия');
      //     if (!this.onClickAction) {
      // this.onClickActionStarted = true;
      // this.changeDay();
      this.dragStatusX = 'drag';
      this.prevClientX = ev.changedTouches[0].clientX;
      //     }
      // }, 200);


  }



  dragWeekDay(ev: any) {
      if (this.prevClientX) {
          this.deltaX = ev.changedTouches[0].clientX - this.prevClientX;
          this.prevClientX = ev.changedTouches[0].clientX;
          let blockWidth = document.body.offsetWidth;
          this.comS.calendarOnSwipe(this.deltaX * 100 / blockWidth, blockWidth);
      } else {
          this.prevClientX = ev.changedTouches[0].clientX;
      }
      this.onClickActionStarted = true;
  }

  dragWeekDayEnd(ev: any) {
      if (this.onClickActionStarted) { // этим я пока убрал возможность переключения дней в режиме "неделя" при нажатии(свайп при этом работает)
          if (this.dragStatusX === 'drag') {
              this.dragStatusX = 'finish';
              this.comS.calendarOnSwipeEnd(this.deltaX);
              setTimeout(() => {
                  this.calendar.selectedDate = this.comS.getMomentObject(this.calendar.selectedDate).add(this.deltaX < 0 ? 1 : -1, 'days').format('YYYY-MM-DD');
                  this.calendar.selectedMonth = this.comS.getMomentObject(this.calendar.selectedDate).format('YYYY-MM');
                  this.calendar.monthDelta = 0;
                  this.calendar.weekDelta = 0;
                  this.initCalendar(false);
                  this.dataSwiped.emit({date: this.calendar.selectedDate, delta: this.deltaX < 0 ? 'more' : 'less'});
              }, 500);
          }
      }

      this.onClickActionStarted = false;
  }

    changeDay() {
      setTimeout(() => {
          if (this.onClickActionStarted) {
              this.onClickAction += 20;
              this.changeDay();
          } else {
            return '';
          }
      }, 20);
    }

    getSwipeDays(init: boolean = true) {
      if (init) {
          this.swipeDays = [];
          for (let i = -2; i <= 2; i++) {
              let day = this.comS.getMomentObject(this.calendar.selectedDate);
              let newDay = day.add(i, 'days');
              this.swipeDays.push({fullFormat: newDay.format('YYYY-MM-DD'), dayMonth: newDay, nearest: this.isNearestDate(newDay)});
          }
      } else {
          if (this.swipeDays.length === 5) {
              let day = this.comS.getMomentObject(this.calendar.selectedDate);
              let newDay = day.add(this.deltaX < 0 ? 2 : -2, 'days');
              if (this.deltaX < 0) {
                  this.swipeDays.splice(0, 1);
              } else {
                  this.swipeDays.splice(this.swipeDays.length - 1, 1);
              }
              this.swipeDays[this.deltaX < 0 ? 'push' : 'unshift']({fullFormat: newDay.format('YYYY-MM-DD'), dayMonth: newDay, nearest: this.isNearestDate(newDay)});
          }
      }
    }

    isNearestDate(date: any) {
      let formated = date.format('YYYY-MM-DD');
      if (this.comS.getMomentObject().format('YYYY-MM-DD') === formated) {
          return 1;
      } else if (this.comS.getMomentObject().add(-1, 'days').format('YYYY-MM-DD') === formated) {
          return 0;
      } else if (this.comS.getMomentObject().add(1, 'days').format('YYYY-MM-DD') === formated) {
          return 2;
      } else {
          return null;
      }
    }

    getNearestWord(date: any) {
      let arr = date.split(' ');
      return arr.length > 0 ? arr[0].replace(/,/g, '') : '';
    }

}
