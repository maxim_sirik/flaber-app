import {Component, EventEmitter, Input, Output, OnInit} from '@angular/core';
import {CommonService} from '../../services/common.service';
import {SharedService} from '../../services/shared.service';

@Component({
  selector: 'app-header-menu',
  templateUrl: './header-menu.component.html',
  styleUrls: ['./header-menu.component.scss'],
})
export class HeaderMenuComponent implements OnInit {
    @Input() config: any;
    @Output() calendarSelectDate = new EventEmitter();
    @Output() calendarDateSwiped = new EventEmitter();

    public calendarValues: any = {
        currentState: 'week',
        month: null,
        week: null,
        weekDelta: 0,
        monthDelta: 0,
        selectedDate: this.comS.getMomentObject().format('YYYY-MM-DD'),
        selectedMonth: this.comS.getMomentObject().format('YYYY-MM')
    };
    constructor(public comS: CommonService,
                public shared: SharedService) {
      this.shared.hideHeaderToShowNewRecordHandler.subscribe((show) => {
          if (this.config && this.config.showCalendar) {
                this.calendarValues.currentState = !show ? 'week' : 'no_calendar';
          }
      });
    }

    ngOnInit() {
    }

    openSidebar() {
      this.comS.toggleSidebar(true);
    }

    buttonsCallbackThrower(method) {
        this.shared.alertCallbackMethod.next({ methodName: method});
    }

    calendarClosedHandler(ev: any) {
        this.calendarValues = ev;
    }

    dataSelectedHandler(ev: any) {
        this.calendarSelectDate.emit(ev);
    }

    dateSwipedHandler(ev: any) {
        this.calendarDateSwiped.next(ev);
    }

}
