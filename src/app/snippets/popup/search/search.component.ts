import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {SharedService} from '../../../services/shared.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {

  public searchComponent = 'service';
  public show = false;
  public text = '';
  public searchField: any;
  public isMasters: any;
  public category: any = null;

  constructor(public comS: CommonService,
              public shared: SharedService) {
    this.shared.searchStatusChanged.subscribe(
        obj => {
            this.show = typeof obj.show !== 'undefined' ? obj.show : false;
            this.searchComponent = typeof obj.searchComponent !== 'undefined' ? obj.searchComponent : 'service';
            this.category = typeof obj.category !== 'undefined' ? obj.category : null;
            this.text = typeof obj.text !== 'undefined' ? obj.text : '';
            this.isMasters = typeof obj.isMasters !== 'undefined' ? obj.isMasters : false;
            this.initComponent();
        }
    );
  }

  ngOnInit() {}

  closeSearch() {
    this.show = false;
    this.searchContent('');
  }

  initComponent() {
    if (this.show) {
      let input = document.querySelector('.name-input') as HTMLElement;
      if (input) {
          this.searchContent(this.searchComponent === 'client' ? '' : this.text);
          input.focus();
      } else {
        setTimeout(() => {
          this.initComponent();
        }, 50);
      }

    }
  }

  searchContent(value: any = null) {
    if (value !== null) {
        this.searchField = value;
    }
    console.log('this.searchField=' + value);
    this.shared.searchFinished.next({
        component: this.searchComponent,
        array: this.searchField,
        category: this.category,
        isMasters: this.isMasters
    });
  }

}
