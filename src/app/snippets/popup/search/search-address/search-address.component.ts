import {Component, OnDestroy, OnInit} from '@angular/core';
import {SharedService} from '../../../../services/shared.service';
import {ApiService} from '../../../../services/api-service.service';
import {CommonService} from '../../../../services/common.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-search-address',
  templateUrl: './search-address.component.html',
  styleUrls: ['./search-address.component.scss'],
})
export class SearchAddressComponent implements OnInit, OnDestroy {

    public addressFields: any = {};
    public searchData: any = [];
    public searchSub: Subscription;
    public allowedPlaces: any = ['building'];
    constructor(public shared: SharedService,
                private apiS: ApiService,
                public comS: CommonService) {
        this.searchSub = this.shared.searchFinishedObserver.subscribe(
            data => {
                if (data['component'] === 'address') {
                    if (data['array'] && data['array'].replace(/\s/g, '').length) {
                        this.searchAddressQuery(data['array']);
                    }
                }
            }
        );
    }

  ngOnInit() {}

  ngOnDestroy() {
      this.searchSub.unsubscribe();
  }

    searchAddressQuery(query: any) {
        this.apiS.get_address_data(query).subscribe(
            success => {
                // this.searchData = success;
                this.filterSearchResults(success);
            }, error => {
                this.searchData = [];
            }
        );
    }

    filterSearchResults(data: any) {
        this.searchData = [];
        for (let place of data) {
            if (this.allowedPlaces.indexOf(place['class']) !== -1) {
                this.searchData.push(place);
            }
        }
    }

    selectAddressResult(result: any) {
        this.addressFields['lat'] = result['lat'];
        this.addressFields['lng'] = result['lon'];
        this.addressFields['country_code'] = result['address']['country_code'];
        this.addressFields['city'] = result['address']['city'] ? result['address']['city'] : (result['address']['city_district'] ? result['address']['city_district'] : (result['address']['village'] ? result['address']['village'] : result['address']['state']));
        this.addressFields['address'] = result['display_name'];
        this.shared.addressOnSearch.next(this.addressFields);
        this.comS.showSearch({});
    }

}
