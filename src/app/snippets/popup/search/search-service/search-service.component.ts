import {Component, OnDestroy, OnInit} from '@angular/core';
import {SharedService} from '../../../../services/shared.service';
import {CommonService} from '../../../../services/common.service';
import {ApiService} from '../../../../services/api-service.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-search-service',
  templateUrl: './search-service.component.html',
  styleUrls: ['./search-service.component.scss'],
})
export class SearchServiceComponent implements OnInit, OnDestroy {

  public results: any;
  public phrase: any = '';
  public services = [];
  public sections: any;
  public category: any;
  public isMasters = false;
  public mastersServicesGetted = false;
  public noAvailableService = false;
  public servicesGetted = false;
  public sub: Subscription;

  constructor(public shared: SharedService,
              public comS: CommonService,
              private apiS: ApiService) {
      this.sub = this.shared.searchFinishedObserver.subscribe(
          data => {
            if (data['component'] === 'service') {
                if (data['array'] && data['array'].replace(/\s/g, '').length) {
                    this.phrase = data['array'];
                }
                this.isMasters = typeof data['isMasters'] !== 'undefined' ? data['isMasters'] : false;
                if (data['category'] && this.category !== data['category']) {
                    this.category = data['category'];
                    this.getServicesList();
                }
                if (this.isMasters && !this.mastersServicesGetted) {
                    this.getMastersServices();
                }
                if (this.phrase) {
                    this.filterResults(this.services);
                }
            }
          }
      );
      this.comS.updateWorker();
      this.getServicesList();
      this.getServicesSections();
  }

  ngOnInit() {}

  ngOnDestroy() {
      this.sub.unsubscribe();
      this.shared.searchFinished.next({});
  }

  getServicesList() {
      this.noAvailableService = false;
      this.servicesGetted = false;
      this.services = [];
      if (this.category) {
          this.getServicesByCategoryId(this.category);
      } else {
          this.getAllAvailableServices();
      }
  }

  getServicesByCategoryId(category: any) {
      this.apiS.get_services_by_category_id(category).subscribe(
          success => {
              if (success['status']) {
                  this.services = success['text']['sections'];
                  this.servicesGetted = true;
                  this.filterResults(this.services);
              }
          }, error => {
              if (error.error) {
                  this.comS.showHint(error.error.text.errors);
              }
          }
      );
  }

  getAllAvailableServices() {
      this.apiS.get_all_available_services().subscribe(
          success => {
              if (success['status']) {
                  this.services = success['text']['services'];
                  this.servicesGetted = true;
              }
          }, error => {
              if (error.error) {
                  this.comS.showHint(error.error.text.errors);
              }
          }
      );
  }

  getMastersServices() {
      this.noAvailableService = false;
      this.apiS.get_worker_services_new_work(this.comS.workerInfo['id']).subscribe(
          success => {
              if (success['status']) {
                  this.services = success['text']['service_providers'];
                  this.servicesGetted = true;
                  this.mastersServicesGetted = true;
              }
          }, error => {
              if (error.error) {
                  this.comS.showHint(error.error.text.errors);
              }
          }
      );
  }

  filterResults(data: any) {
      console.log(data, 'filterResults');
      this.results = [];
      if (this.servicesGetted) {
              // if (this.phrase) {
              // if (this.phrase && this.phrase.length > 0) {
                  if (data.length > 0) {
                      if (data) {
                          console.log(data, 'on filterResults');
                          for (const result of data) {
                              if ((result.name.toLowerCase().indexOf(this.phrase.toLowerCase()) !== -1) || (!this.phrase) || (this.phrase.length === 0)) {
                                  this.results.push(result);
                              }
                          }
                          this.noAvailableService = (this.results.length === 0);
                      } else {
                          this.noAvailableService = true;
                      }
                  } else {
                        this.noAvailableService = true;
                  }
              // } else {
              //     this.noAvailableService = false;
              // }
      } else {
          setTimeout(() => {
              this.filterResults(this.services);
          }, 50);
      }
  }

  getServicesSections() {
      this.apiS.get_services_sections().subscribe(
          success => {
              if (success['status']) {
                  this.sections = success['text']['sections'];
              }
          }, error => {
              if (error.error) {
                  this.comS.showHint(error.error.text.errors);
              }
          }
      );
  }

  selectResult(result: any) {

      let selectedSection: any = null;
      if (this.sections) {
          this.sections.forEach((section) => {
              if (section['id'] === result['section_id']) {
                  selectedSection = {name: section['name'], id: section['id']};
              }
          });
      }

      this.shared.serviceNameOnSearch.next({name: result['name'], id: result['id'], section: selectedSection});
      this.comS.showSearch({});
  }

  addCustomService() {
      this.shared.serviceNameOnSearch.next({name: this.phrase, id: null, section: null});
      this.comS.showSearch({});
  }

}
