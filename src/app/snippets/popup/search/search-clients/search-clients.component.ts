import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../../services/api-service.service';
import {SharedService} from '../../../../services/shared.service';
import {CommonService} from '../../../../services/common.service';

@Component({
  selector: 'app-search-clients',
  templateUrl: './search-clients.component.html',
  styleUrls: ['./search-clients.component.scss'],
})
export class SearchClientsComponent implements OnInit {

    public customersFiltered: any = [];
    public phrase: any;
    public customers = [];
    public customersGetted: any = false;

    constructor(public shared: SharedService,
                public comS: CommonService,
                private apiS: ApiService) {
        this.shared.searchFinishedObserver.subscribe(
            data => {
                if (data['component'] === 'client') {
                    if (data['array'] && data['array'].replace(/\s/g, '').length) {
                        this.phrase = data['array'];
                        if (!this.customersGetted) {
                            this.getCustomers();
                        }
                        this.filterUsers();
                    }
                }
            }
        );
    }

    getCustomers() {
        this.apiS.get_customers().subscribe(
            success => {
                if (success['status']) {
                    this.customers = success['text']['customers'];
                    this.customersGetted = true;
                    this.filterUsers();
                }
            }, error => {
                this.customersGetted = true;
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }

    filterUsers() {
      if (this.customersGetted) {
          this.customersFiltered = [];
          if (this.phrase && this.phrase.length > 0) {
              for (let i = 0; i < this.customers.length; i++) {
                  if (this.customers[i]['first_name'] && this.customers[i]['first_name'].toLowerCase().indexOf(this.phrase.toLowerCase()) !== -1) {
                      this.customersFiltered.push(this.customers[i]);
                  } else if (this.customers[i]['last_name'] && this.customers[i]['last_name'].toLowerCase().indexOf(this.phrase.toLowerCase()) !== -1) {
                      this.customersFiltered.push(this.customers[i]);
                  } else if (this.customers[i]['phone'] && this.customers[i]['phone'].toLowerCase().indexOf(this.phrase.toLowerCase()) !== -1) {
                      this.customersFiltered.push(this.customers[i]);
                  }
              }
          } else {
              this.customersFiltered = this.customers;
          }
      } else {
        setTimeout(() => {
          this.filterUsers();
        }, 100);
      }

    }

    ngOnInit() {}

    customerChanged(customer: any) {
        this.shared.clientOnSearch.next(customer);
        this.comS.showSearch({});
    }

}
