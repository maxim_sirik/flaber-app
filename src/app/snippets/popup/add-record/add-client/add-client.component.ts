import { Component, OnInit } from '@angular/core';
import {SharedService} from '../../../../services/shared.service';
import {ApiService} from '../../../../services/api-service.service';
import {CommonService} from '../../../../services/common.service';
import Inputmask from 'inputmask';


@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss'],
})
export class AddClientComponent implements OnInit {

  public showNewClient = false;
  public customers = [];
  public customerSelected = false;
  public isOnloadMoreCustomers = false;
  public isLastPage = false;
  public currentPage = 1;
  public currentCustomer: any = null;
  public searchClients: any;
  public customersFiltered = [];
  public im: any;


  constructor(public shared: SharedService,
              public apiS: ApiService,
              public comS: CommonService) {
    this.resetState();
    this.getCustomers();
    this.initScrollLoadListener();
  }

  ngOnInit() {
      this.im = new Inputmask(this.comS.phoneMask);
  }

  initScrollLoadListener() {
      let containerElement = document.querySelector('app-add-client .tab-content__container');

      if (containerElement) {
          containerElement.addEventListener('scroll', (ev) => { // пагинация контактов по скроллу
              let target = ev.target as HTMLElement;
              if (target) {
                if (target.scrollHeight - target.offsetHeight === target.scrollTop) {
                  if (!this.isOnloadMoreCustomers && !this.isLastPage) {
                    this.getCustomers(this.currentPage + 1);
                  }

                }
              }
          });
      } else {
        setTimeout(() => {
          this.initScrollLoadListener();
        }, 100);
      }
  }

  resetState() {
    this.showNewClient = false;
    this.currentCustomer = null;
    this.customers = [];
  }

  getCustomers(page: any = null) {
    this.isOnloadMoreCustomers = true;
    this.apiS.get_customers(page).subscribe(
      success => {
        if (success['status']) {
          if (success['text']['customers'] && success['text']['customers']['data']) {
            for (let customer of success['text']['customers']['data']) {
              this.customers.push(customer);
            }
          }
          if (success['text']['customers'] && success['text']['customers']['current_page']) {
            this.isLastPage = success['text']['customers']['current_page'] === success['text']['customers']['last_page'];
            this.currentPage = success['text']['customers']['current_page'];
          }
          // this.customers = success['text']['customers'];
          this.filterUsers();
        }
        this.isOnloadMoreCustomers = false;
      }, error => {
        this.isOnloadMoreCustomers = false;
        if (error.error) {
          this.comS.showHint(error.error.text.errors);
        }
      }
    );
  }

  selectCustomer() {
    this.shared.newRecordUser.next(this.currentCustomer);
  }

  customerChanged(customer: any) {
    this.currentCustomer = customer;
    this.customerSelected = true;
  }

  filterUsers() {
    this.customersFiltered = [];
    if (this.searchClients && this.searchClients.length > 0) {
      for (let i = 0; i < this.customers.length; i++) {
        if (this.customers[i]['first_name'] && this.customers[i]['first_name'].toLowerCase().indexOf(this.searchClients.toLowerCase()) !== -1) {
          this.customersFiltered.push(this.customers[i]);
        } else if (this.customers[i]['last_name'] && this.customers[i]['last_name'].toLowerCase().indexOf(this.searchClients.toLowerCase()) !== -1) {
          this.customersFiltered.push(this.customers[i]);
        } else if (this.customers[i]['phone'] && this.customers[i]['phone'].toLowerCase().indexOf(this.searchClients.toLowerCase()) !== -1) {
          this.customersFiltered.push(this.customers[i]);
        }
      }
    } else {
      this.customersFiltered = this.customers;
    }

    this.initPhoneMask();
  }

  initPhoneMask() {
      // app-add-client
      setTimeout(() => {
          for (let item of this.customersFiltered) {
              // phone
              let selector = document.querySelector('app-add-client [data-custid="' + item.id + '"] .phone input');
              if (selector) {
                  this.im.mask(selector);
                  selector['inputmask'].setValue(item['phone'].replace('380', ''));
              }
          }
      }, 300);
  }

}
