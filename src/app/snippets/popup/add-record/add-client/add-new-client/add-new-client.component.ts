import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import {CommonService} from '../../../../../services/common.service';
import {ApiService} from '../../../../../services/api-service.service';
import {SharedService} from '../../../../../services/shared.service';
import Inputmask from 'inputmask';

@Component({
  selector: 'app-add-new-client',
  templateUrl: './add-new-client.component.html',
  styleUrls: ['./add-new-client.component.scss'],
})
export class AddNewClientComponent implements OnInit {

  public clientForm: FormGroup;
  public focusObject = null;
  public focusOnField = false;
  public validateClicked = false;
  public im: any;
  public errors = {
    phone: false
  };

    public personalArray = [
        {
            formName: 'first_name',
            type: 'text',
            placeholder: this.comS.lang['label_client_first_name'],
            hasRestriction: false,
            restriction: 10000
        },
        {
            formName: 'last_name',
            className: 'surname-input',
            labelClassName: 'icon-user surname',
            type: 'text',
            placeholder: this.comS.lang['label_client_last_name'],
            hasRestriction: false,
            restriction: 10000
        },
        {
            formName: 'phone',
            type: 'tel',
            placeholder: this.comS.lang['label_phone'],
            hasRestriction: false,
            restriction: 300
        }
    ];

  constructor(public fb: FormBuilder,
              public comS: CommonService,
              public apiS: ApiService,
              public shared: SharedService) {
      this.clientForm = fb.group({
          'first_name': [''],
          'last_name': [''],
          'phone': ['', Validators.required],
      });
  }

  ngOnInit() {
      this.im = new Inputmask(this.comS.phoneMask);
      this.initPhoneMaskField();
  }

  initPhoneMaskField() {
      let selector = document.querySelector('[data-form-name="phone"]');
      if (selector) {
          this.im.mask(selector);
      } else {
          setTimeout(() => {
              this.initPhoneMaskField();
          }, 100);
      }
  }

  addNewClient() {
    if (this.clientForm.valid) {
      this.validateClicked = false;
      let selector = document.querySelector('[data-form-name="phone"]');
      let object = {
        'first_name': this.clientForm.controls['first_name'].value.length > 0 ? this.clientForm.controls['first_name'].value : null,
        'last_name': this.clientForm.controls['last_name'].value.length > 0 ? this.clientForm.controls['last_name'].value : null,
        'phone': '380' + selector['inputmask'].unmaskedvalue()
      };
      this.apiS.add_customer(object).subscribe(
        success => {
          if (success['status']) {
            this.shared.newRecordUser.next(success['text']['customer']);
          }
        }, error => {
          this.comS.showHint(error.error['text']['errors']);
        }
      );
    } else {
      this.validateClicked = true;
      this.comS.showHint(this.comS.lang['error_fill_fields']);
    }
  }

    fieldOnFocus(field: any) {
        this.focusObject = field;
        this.focusOnField = true;
    }

    fieldOutOfFocus() {
        this.focusOnField = false;
        setTimeout(() => {
            if (!this.focusOnField) {
                this.focusObject = null;
            }
        }, 200);
    }

    clearFieldValue(field) {
        field.setValue('');
    }

}
