import {Component, Input, OnInit} from '@angular/core';
import {SharedService} from '../../../../services/shared.service';
import {CommonService} from '../../../../services/common.service';
import {ApiService} from '../../../../services/api-service.service';


@Component({
  selector: 'app-add-service',
  templateUrl: './add-service.component.html',
  styleUrls: ['./add-service.component.scss'],
})
export class AddServiceComponent implements OnInit {

    @Input() services: any;

	public masterServices = [];
	public JSON = JSON;
	public selectedServices = [];

    constructor(public shared: SharedService,
    			public comS: CommonService,
    			public apiS: ApiService) {
        this.resetState();
        this.initPage();
    }

    ngOnInit() {
    	console.log(this.services, 'services');
	}

    resetState() {
    }

	initPage() {
	  if (this.comS.updateWorker()) {
	      this.getServices();
	  } else {
	      this.comS.pushPage('/main');
	  }

  	}

  	selectService() {
  		let elements = document.querySelectorAll('input[name="selectedService"]:checked');
  		let servicesToCheck = [];
		if (elements && elements.length > 0) {
			for (let i = 0; i < elements.length; i++) {
				let element = elements[i] as HTMLElement;
				servicesToCheck.push({
                    id: element['value'],
                    name: element.getAttribute('data-service-name')
				});
			}
			this.shared.newRecordService.next(servicesToCheck);
		} else {
		  	this.comS.showHint(this.comS.lang['error_empty_service']);
		}
  	}

  	isSelectedService(id: any) {
    	let resp = false;
    	if (this.selectedServices.indexOf(id) !== -1 || this.services.indexOf(id) !== -1) {
            resp = true;
		}
		return resp;
	}

	getServices() {
		this.masterServices = [];
		this.apiS.get_services(this.comS.workerInfo['id']).subscribe(
		success => {
				if (success['status']) {
				  this.masterServices = success['text']['sections'];
				}
			}, error => {
			  	console.log(error);
			}
		);
	}

}
