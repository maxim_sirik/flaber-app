import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SharedService} from '../../../services/shared.service';
import {CommonService} from '../../../services/common.service';
import {ApiService} from '../../../services/api-service.service';
import Inputmask from 'inputmask';

@Component({
  selector: 'app-add-record',
  templateUrl: './add-record.component.html',
  styleUrls: ['./add-record.component.scss'],
})
export class AddRecordComponent implements OnInit {
  public im: any;
  public show = false;
  public showSingle = true;
  public currentTab = null;
  public selectedTimeFrom = true;
  public newRecordId: any;
  public isMoveRecord = false;
  public currentHour: any = new Date().getHours();
  public tabs = [
      {
          name: this.comS.lang['tab_new_record_1'],
          id: 1
      },
      {
          name: this.comS.lang['tab_new_record_2'],
          id: 2
      },
      {
          name: this.comS.lang['tab_new_record_3'],
          id: 3
      },
      {
          name: this.comS.lang['tab_new_record_4'],
          id: 4
      }
  ];

  public recordInfo = {
      client: null,
      services: {
        ids: [],
        name: ''
      },
      worker: null,
      time: {
        date: this.comS.formatDateTime(new Date().setHours(0, 0, 0, 0), 'YYYY-MM-DD'),
        time: this.comS.formatDateTime(new Date().setHours(0, 0, 0, 0), 'H:mm'),
        finish_time: null,
        finish_date: null
      }
  };

  constructor(public shared: SharedService,
              public comS: CommonService,
              public apiS: ApiService) {
      this.shared.showNewRecord.subscribe(
          data => {
              this.show = data['show'] !== null ? data['show'] : false;
              if (data['time']) {
                  this.recordInfo.time.time = data['time'];
              }
              if (data['finish_time']) {
                  this.recordInfo.time.finish_time = data['finish_time'];
              }
              if (data['finish_date']) {
                  this.recordInfo.time.finish_date = data['finish_date'];
              }
              if (data['date']) {
                  this.recordInfo.time.date = data['date'];
              }
              this.isMoveRecord = typeof data['moveRecord'] !== 'undefined' ? data['moveRecord'] : false;
              if (this.isMoveRecord) {
                  if (this.currentTab !== 2) {
                      this.changeTab(2, true);
                  }
              }

              if (this.recordInfo.time.date === this.comS.getMomentObject().format('YYYY-MM-DD')) {
                  let initTime = this.recordInfo.time.time.split(':');
                  let initialHour = parseInt(initTime[0], null);

                  if (initialHour < this.currentHour) {
                      this.recordInfo.time.time = this.currentHour + ':' + initTime[1];
                  }
              }

              this.shared.hideHeaderToShowNewRecord.next(this.show);
              this.addBlur(this.show);
              if (this.show) {
                this.comS.updateWorker();
                this.recordInfo.worker = {
                  id: this.comS.workerInfo ? this.comS.workerInfo.id : null,
                  first_name: this.comS.userInfo['first_name'],
                  last_name: this.comS.userInfo['last_name'],
                  photo_url: this.comS.userInfo['photo_url']
                };
              } else {
                this.resetModalData();
              }
          }
      );

      this.shared.newRecordUserSelected.subscribe(
        client => {
          if (client['id']) {
            this.recordInfo.client = client;
            this.changeTab(1);
          }
        }
      );

      this.shared.newRecordServiceSelected.subscribe(
        service => {
            this.recordInfo.services.ids = [];
            this.recordInfo.services.name = '';
            if (service && service.length >= 0) {
                for (let serviceItem of service) {
                    this.recordInfo.services.ids.push(parseInt(serviceItem['id'], null));
                    this.recordInfo.services.name += (this.recordInfo.services.name.length > 0 ? ', ' : '') + serviceItem['name'];
                }
                this.changeTab(4, true, true);
            }
          // if (service['id']) {
          //     // this.recordInfo.services.ids = [];
          //     if (!this.recordInfo.services.ids.includes(parseInt(service['id'], null))) {
          //         this.recordInfo.services.ids.push(parseInt(service['id']));
          //         this.recordInfo.services.name += (this.recordInfo.services.name.length > 0 ? ', ' : '') + service['name'];
          //         this.changeTab(4, true, true);
          //     } else {
          //         this.comS.showHint(this.comS.lang['error_service_already_added']);
          //     }
          // }
        }
      );

      this.shared.newRecordTimeSelected.subscribe(
        time => {
          if (time['date']) {
              if (this.isMoveRecord) {
                  this.shared.recordMove.next(time);
                  this.closeNewRecordModal();
              } else {
                  this.recordInfo.time = time;
                  this.changeTab(2);
              }
          }
        }
      );

      this.shared.alertCallbackMethodFired.subscribe(
          methodObj => {
              if (methodObj['methodName']) {
                  if (typeof this[methodObj['methodName']] !== 'undefined') {
                      this[methodObj['methodName']]();
                  }
              }
          }
      );
  }

  addBlur(show: boolean) {
      let main = document.querySelectorAll('main') as NodeListOf<Element>;
      if (main) {
          main.forEach((item) => {
              item.classList[show ? 'add' : 'remove']('blur');
          });

      }
  }

  ngOnInit() {
      this.im = new Inputmask(this.comS.phoneMask);
  }

  initPhoneMask() {
      setTimeout(() => {
          let selector = document.querySelector('.fill-in__list .phone input');
          if (selector) {
              this.im.mask(selector);
              selector['inputmask'].setValue(this.recordInfo.client['phone'].replace('380', ''));
          }
      }, 300);
  }

  closeNewRecordModal() {
    this.shared.newRecord.next({});
  }

  addNewRecord() {
        let recordBody = {
            'customer_id': this.recordInfo.client['id'],
            'date': this.recordInfo.time.date,
            'time': this.recordInfo.time.time,
            'by_master':1, // если добавляет сам мастер. если этого параметра нет или он равен 0 - считается, что запись создал пользователь
            'finish_time': this.recordInfo.time.finish_time ? this.recordInfo.time.finish_time : null,
            // 'company_id': 1,  (optional)
            'worker_id': this.recordInfo.worker['id'],
            'service_providers': this.recordInfo.services.ids
        };

        this.apiS.add_new_record(recordBody).subscribe(
            success => {
                if (success['status']) {
                  this.showSingle = false;
                  this.shared.newRecordAdded.next({added: true});
                  this.newRecordId = success['text']['record']['id'];
                }
            }, error => {
                if (error.error) {
                    if (error.error.text.errors['existing_record_id']) {
                        this.comS.showHint(error.error.text.errors['message'], {text: 'Нажмите, чтобы просмотреть информацию о записи', url: '/record-info/' + error.error.text.errors['existing_record_id']});
                    } else {
                        this.comS.showHint(error.error['text']['errors']);
                    }
                }
            }
        );
  }

  changeTab(id: any, toChange: any = false, reset: any = false) {
    if ((id !== 3 && !this.isMoveRecord) || toChange) {
      this.currentTab = reset ? null : ((this.currentTab && this.currentTab === id) ? null : id);
      this.shared.newRecordModalResetTabs.next({});
    }
    this.initPhoneMask();
  }

  selectTimeTab(tabFrom: boolean = true) {
      this.changeTab(2);
      this.selectedTimeFrom = tabFrom;
  }

  goToRecord() {
      this.comS.pushPage('record-info/' + this.newRecordId);
      this.closeNewRecordModal();
  }

  resetModalData() {
    this.recordInfo = {
      client: null,
      services: {
        ids: [],
        name: ''
      },
      worker: null,
      time: {
        date: this.comS.formatDateTime(new Date().setHours(0, 0, 0, 0), 'YYYY-MM-DD'),
        time: this.comS.formatDateTime(new Date().setHours(0, 0, 0, 0), 'H:mm'),
        finish_time: null,
        finish_date: null
      }
    };
    this.show = false;
    this.showSingle = true;
    this.currentTab = null;
  }

}
