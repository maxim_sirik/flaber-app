import { Component, OnInit, ViewChild, ElementRef, Input} from '@angular/core';
import {SharedService} from '../../../../services/shared.service';
import {CommonService} from '../../../../services/common.service';
import {ApiService} from '../../../../services/api-service.service';
import { TapticEngine } from '@ionic-native/taptic-engine/ngx';

@Component({
  selector: 'app-add-time',
  templateUrl: './add-time.component.html',
  styleUrls: ['./add-time.component.scss'],
})

export class AddTimeComponent implements OnInit {

  @Input() selectedDate: any;
  @Input() initTabFrom: any;
  @ViewChild('day_picker') day_picker: ElementRef;
  @ViewChild('hour_picker') hour_picker: ElementRef;
  @ViewChild('minute_picker') minute_picker: ElementRef;

  public selectedTime = {
    date: null,
    time: {
      hour: null,
      minute: null
    },
    finish_time: {
      hour: null,
      minute: null
    },
    valid: false
  };
  public customInited = {
      date: false,
      hours: false,
      minutes: false
  };
  public currentHour: any = new Date().getHours();
  public activeTabFrom = true;
  public minutesArrayFull = ['00', '05', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55'];
  public minutesArrayZero = ['00'];
  public dayIndexTranformed = 0;
  public currentDayIndex = 3;
  public calendarDays = {
    formated: [],
    raw: []
  };

  public data = [
    {
      list: this.calendarDays.formated,
      currentIndex: 3
    }
  ];
  public data1 = [
    {
      list: [],
      currentIndex: 0
    }
  ];
  public data2 = [
    {
      list: this.minutesArrayFull,
      currentIndex: 0
    }
  ];

  constructor(public shared: SharedService,
              public comS: CommonService,
              public apiS: ApiService,
              private taptic: TapticEngine) {}

  ngOnInit() {
    this.activeTabFrom = this.initTabFrom;
    this.resetState();
    this.initCalendar();
  }

  initCalendar(changeTab: any = false) {
    this.fillCalendarDays(changeTab);
    this.getScheduleByDay();
  }

  resetState() {
    // this.lastDayIndex = this.currentDayIndex;
  }

  changeActiveTab(isFrom: boolean) {
    this.activeTabFrom = isFrom;
    this.initCalendar(true);
    // this.setHourPickerData();
  }

  fillCalendarDays(changeTab: any = false) {
    this.calendarDays.formated = [];
    this.calendarDays.raw = [];
    let toDateRaw = this.activeTabFrom ? (this.selectedDate['date'] ? this.selectedDate['date'] : '') : (this.selectedDate['finish_date'] ? this.selectedDate['finish_date'] : (this.selectedDate['date'] ? this.selectedDate['date'] : this.comS.getMomentObject().format('YYYY-MM-DD')));
    let today = this.comS.getMomentObject();
    let toDate = this.comS.getMomentObject(toDateRaw);
    let floatDiff = toDate.diff(today, 'days', true);
    this.dayIndexTranformed = Math.ceil(floatDiff) ? Math.ceil(floatDiff) : 0;
    this.customInited.date = true;

    for (let i = -3; i <= 3; i++) {
        let dayGetted = this.comS.getDaysBeforeAfter(i + this.dayIndexTranformed);
        this.calendarDays.raw.push(dayGetted.format('LLLL'));
        if (this.comS.isCurrentDay(dayGetted)) {
            this.calendarDays.formated.push(this.comS.getMomentObject().calendar().split(' ')[0].replace(/,/g, ''));
        } else {
            this.calendarDays.formated.push(this.localeBeforeFormat(dayGetted).format('dd D MMM'));
        }
    }
    this.data = [
      {
        list: this.calendarDays.formated,
        currentIndex: 3
      }
    ];
    if (typeof this.day_picker !== 'undefined') {
      this.day_picker['currentIndexList'] = [3];
      this.currentDayIndex = 3;
    }

    this.setSelectedDay(this.calendarDays.raw[3]);
    this.selectedTime.valid = false;
  }

  localeBeforeFormat(date: any) {
    return date.locale(this.comS.appLang);
  }

  getScheduleByDay(date: any = new Date()) {
    this.apiS.get_worker_schedule().subscribe(
      success => {
        if (success['status']) {
          let dayId = this.comS.getDayId(date);
          dayId = dayId === 0 ? 7 : dayId;
          this.setHourPickerData(success['text']['worktimes'][dayId]);
        }
      }, error => {
        this.setHourPickerData();
      }
    );
  }

  setHourPickerData(daySchedule: any = null) {
    let timeArray = [];
    let initialHour = 0;
    let initialMinute = '00';
    let initialHourIndex = 0;
    let initialMinuteIndex = 0;
    let startTime = 0;
    let finishTime = 24;



    if (this.selectedDate['time'] && !this.customInited.hours) {
        let currentDay = this.comS.getMomentObject().format('YYYY-MM-DD');
        let initTime = this.selectedDate['time'].split(':');
        initialHour = parseInt(initTime[0], null);
        initialMinute = initTime[1];
        // if (this.selectedDate['date'] === currentDay) {
        //     if (initialHour < this.currentHour) {
        //         initialHour = this.currentHour;
        //         this.selectedDate['time'] = initialHour + ':' + initialMinute;
        //     }
        // }
        console.log(this.selectedDate, currentDay, 'this.selectedDate');

        this.customInited.hours = true;
    } else {
      if (this.activeTabFrom) {
          initialHour = this.selectedTime.time.hour ? this.selectedTime.time.hour : 0;
      } else {
          initialHour = (new Date(this.comS.formatDateTime(this.selectedDate['finish_date'], 'YYYY-MM-DD')) > new Date(this.comS.formatDateTime(this.selectedDate['date'], 'YYYY-MM-DD'))) ? 0 : ((this.selectedTime.time.hour && this.selectedTime.time.hour !== finishTime) ? this.selectedTime.time.hour : 0);
      }
      initialMinute = this.selectedTime.time.minute ? this.selectedTime.time.minute : '00';
    }

    if (!this.activeTabFrom) {
        startTime = initialHour;
    }

    initialMinuteIndex = this.minutesArrayFull.indexOf(initialMinute);

    for (let i = startTime; i <= finishTime; i++) {
      if (this.activeTabFrom) {
          if (i === initialHour) {
              initialHourIndex = i - startTime;
          }
      } else {
          if (this.selectedDate['finish_time']) {
              if (i === parseInt(this.selectedDate['finish_time'].split(':')[0], null)) {
                  initialHourIndex = i - startTime;
                  initialHour = i;
              }
          } else {
              initialHourIndex = 0;
          }

      }

      timeArray.push(i < 10 ? '0' + i : i);
      // timeArray.push(i < 10 ? '0' + i : i);
    }

    this.setSelectedHour(initialHour);
    this.setSelectedMinute(initialMinute);
    this.selectedTime.valid = true;

    this.data1 = [
      {
        list: timeArray,
        currentIndex: initialHourIndex
      }
    ];

    if (typeof this.hour_picker !== 'undefined') {
      this.hour_picker['currentIndexList'] = [initialHourIndex];
    }

    if (timeArray.length === 0) { // мастер не работает в выбранный день
      this.selectedTime.valid = false;
      this.data2 = [
        {
          list: [],
          currentIndex: initialMinuteIndex
        }
      ];
      
    } else {
      let list = this.minutesArrayFull;
      if (!this.activeTabFrom) { // в случае, если у нас уже выбрано время начала записи
        if (this.selectedTime.time.hour === initialHour) {
            let temp = this.minutesArrayFull;
            list = [];
            for (let min of this.minutesArrayFull) {
                if (parseInt(initialMinute, null) <= parseInt(min, null)) {
                    list.push(min);
                }
            }
            if (list.length > 0) {
                this.setSelectedMinute(list[0]);
            }
        }
        initialMinuteIndex = 0;
      }
      this.data2 = [
        {
          list,
          currentIndex: initialMinuteIndex
        }
      ];
    }

    if (typeof this.minute_picker !== 'undefined') {
      this.minute_picker['currentIndexList'] = [initialMinuteIndex];
    }
  }

  onChangeDayHandler(ev: any) {

    let date = this.calendarDays.raw[ev['iIndex']];
    let dateToSelect = (new Date(this.comS.formatDateTime(date, 'YYYY-MM-DD')) > new Date(this.comS.getMomentObject().format('YYYY-MM-DD'))) ? date : this.comS.getMomentObject().format('YYYY-MM-DD');
    if (this.activeTabFrom) {
        this.selectedDate['date'] = dateToSelect;
    } else {
        this.selectedDate['finish_date'] = (new Date(this.comS.formatDateTime(dateToSelect, 'YYYY-MM-DD')) > new Date(this.comS.formatDateTime(this.selectedDate['date'], 'YYYY-MM-DD'))) ? dateToSelect : this.selectedDate['date'];
    }
    this.taptic.selection();
    this.dayIndexTranformed += ev['iIndex'] - this.currentDayIndex;
    this.currentDayIndex = ev['iIndex'];
    this.getScheduleByDay(new Date(dateToSelect));
    this.fillCalendarDays();
  }
  onChangeHourHandler(ev: any) {
    this.taptic.selection();
    if (typeof this.hour_picker !== 'undefined') {
      this.setSelectedHour(this.hour_picker['data'][0]['list'][ev['iIndex']]);
      this.setSelectedMinute('00');
      this.selectedTime.valid = true;
      if (ev['iIndex'] === this.hour_picker['data'][0]['list'].length - 1) {
        this.data2 = [
          {
            list: this.minutesArrayZero,
            currentIndex: 0
          }
        ];
      } else {
        this.data2 = [
          {
            list: this.minutesArrayFull,
            currentIndex: 0
          }
        ];
      }
      if (typeof this.minute_picker !== 'undefined') {
        this.minute_picker['currentIndexList'] = [0];
      }
    }
  }
  onChangeMinuteHandler(ev: any) {
    this.taptic.selection();
    if (typeof this.minute_picker !== 'undefined') {
      this.setSelectedMinute(this.minute_picker['data'][0]['list'][ev['iIndex']]);
      this.selectedTime.valid = true;
    }
  }

  setSelectedDay(day: any) {
    if (this.activeTabFrom) {
        this.selectedTime.date = day;
    }
  }

  setSelectedHour(hour: any) {
    this.selectedTime[this.activeTabFrom ? 'time' : 'finish_time'].hour = parseInt(hour, null);
  }

  setSelectedMinute(minute: any) {
    this.selectedTime[this.activeTabFrom ? 'time' : 'finish_time'].minute = minute;
  }

  addTime() {
    let start = this.selectedTime.time.hour ? (this.selectedTime.time.hour + ':' + this.selectedTime.time.minute) : (this.selectedDate['time'] ? this.selectedDate['time'] : null);
    let finish = this.selectedTime.finish_time.hour ? (this.selectedTime.finish_time.hour + ':' + this.selectedTime.finish_time.minute) : (this.selectedDate['finish_time'] ? this.selectedDate['finish_time'] : null);
    let formattedStartDate = (this.comS.formatDateTime(this.selectedDate['date'], 'YYYY-MM-DD'));
    let formattedFinishDate = (this.comS.formatDateTime(this.selectedDate['finish_date'], 'YYYY-MM-DD'));
    let resetFinish = false;
    if (formattedFinishDate === formattedStartDate) {
        if (start && finish) {
            let startDis = start.split(':');
            let finishDis = finish.split(':');


            if (parseInt(startDis[0], null) === parseInt(finishDis[0], null)) {
                if (parseInt(startDis[1], null) > parseInt(finishDis[1], null)) {
                    resetFinish = true; // в случае, когда время начала записи больше, чем время конца - мы сбрасываем время конца
                }
            } else if (parseInt(startDis[0], null) > parseInt(finishDis[0], null)) {
                resetFinish = true; // в случае, когда время начала записи больше, чем время конца - мы сбрасываем время конца
            }
        }
    } else if (formattedFinishDate < formattedStartDate) {
        resetFinish = true;
    }
    this.shared.newRecordTime.next({
        date: this.comS.formatDateTime(this.selectedTime.date, 'YYYY-MM-DD'),
        time: start,
        finish_time: !resetFinish ? finish : start,
        finish_date: !resetFinish ? this.comS.formatDateTime(this.selectedDate['finish_date'], 'YYYY-MM-DD') : this.comS.formatDateTime(this.selectedTime.date, 'YYYY-MM-DD')
    });
  }

}
