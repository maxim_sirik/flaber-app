import { Component, OnInit } from '@angular/core';
import {SharedService} from '../../../../services/shared.service';

@Component({
  selector: 'app-add-worker',
  templateUrl: './add-worker.component.html',
  styleUrls: ['./add-worker.component.scss'],
})
export class AddWorkerComponent implements OnInit {

    constructor(public shared: SharedService) {
        this.resetState();
    }

    ngOnInit() {}

    resetState() {

    }

}
