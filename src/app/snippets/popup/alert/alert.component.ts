import {Component, OnDestroy, OnInit} from '@angular/core';
import {SharedService} from '../../../services/shared.service';
import {CommonService} from '../../../services/common.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent implements OnInit {

  public textFieldValue: any;
  public focusObject: any;
  public focusOnField: any = false;
  public alert_ = {
      // show: false,
      // title: 'Поздравляем',
      // text: {
      //     top: 'Вы только что успешно авторизовались в системе',
      //     message: 'Вы можете продолжить регистрацию, заполнив необходимую информацию (это займёт пару минут) или перейти к ознакомлению интерфейса и возможностям приложения, после чего заполнить не достающее данные для завершения регистрации'
      // },
      // buttons: {
      //     success: {
      //         title: 'Продолжить',
      //         callback: () => {
      //             console.log('success callback');
      //         }
      //     },
      //     cancel: {
      //         title: 'Ознакомиться',
      //         callback: () => {
      //             console.log('cancel callback');
      //         }
      //     },
      //     containerClassName: ''
      // }
  };

  constructor(private shared: SharedService,
              public comS: CommonService) {
      this.shared.alertStatusChanged.subscribe((data) => {
          console.log(data, 'on alertStatusChanged');
          this.fieldToDefault();
          this.alert_ = data;
      });
  }

  ngOnInit() {}

  buttonsCallbackThrower(method) {
      if (this.alert_['textField']) {
          this.saveLocalField();
      }
      this.shared.alertCallbackMethod.next({ methodName: method});
  }
  fieldToDefault() {
      this.textFieldValue = null;
      this.focusObject = null;
      this.focusOnField = false;
  }

    fieldOnFocus(field: any) {
        this.focusObject = field;
        this.focusOnField = true;
    }

    fieldOutOfFocus(formName) {
        this.focusOnField = false;
        setTimeout(() => {
            if (!this.focusOnField) {
                this.focusObject = null;
            }
        }, 200);
    }

    saveLocalField() {
      if (this.alert_['textField'] && this.alert_['textField']['localStorageField']) {
          localStorage.setItem(this.alert_['textField']['localStorageField'], this.textFieldValue);
      }
    }

}
