import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {CommonService} from '../../../services/common.service';

@Component({
  selector: 'app-cropper',
  templateUrl: './cropper.component.html',
  styleUrls: ['./cropper.component.scss'],
})
export class CropperComponent implements OnInit {
    @Input() pickedPhoto: any;
    @Output() photoCropped = new EventEmitter();
    public worker: any;
    public photo = {
        base64: '',
        extension: ''
    };
    public cropper = {
        image: '',
        opened: false,
        have_image: false,
        file_type: ''
    };

  constructor(public comS: CommonService) { }

  ngOnInit() {
  }
  imageCropped(event: ImageCroppedEvent) {
      this.cropper.image = event.base64;
      this.cropper.file_type = event.file.type;
  }
  imageLoaded() {
      this.cropper.opened = true;
  }

  loadImageFailed() {
      this.cropper.opened = false;
      this.comS.showHint(this.comS.lang['error_failed_load_photo']);
  }

  closeCropper(save) {
      if (save) {
          this.photo.base64 = this.cropper.image;
          this.photo.extension = (this.cropper.file_type && this.cropper.file_type.length > 0) ? this.cropper.file_type.split('/')[1] : '';

      }
      this.photoCropped.emit(save ? this.photo : null);
      this.cropper.opened = false;
  }

}
