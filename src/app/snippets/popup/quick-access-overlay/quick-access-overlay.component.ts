import { Component, OnInit } from '@angular/core';
import {SharedService} from '../../../services/shared.service';
import {CommonService} from '../../../services/common.service';
import {ApiService} from '../../../services/api-service.service';

@Component({
  selector: 'app-quick-access-overlay',
  templateUrl: './quick-access-overlay.component.html',
  styleUrls: ['./quick-access-overlay.component.scss'],
})
export class QuickAccessOverlayComponent implements OnInit {
	public show = false;
	public show_rotate = false;
	public record: any;
	constructor(public shared: SharedService,
				public comS: CommonService,
				public apiS: ApiService) {
		this.shared.quickAccessOverlayShow.subscribe(
		    overlay => {
				this.show = typeof overlay['show'] !== 'undefined' ? overlay['show'] : false;
				this.addRemoveBlur();
				setTimeout(() => {
					this.show_rotate = typeof overlay['show'] !== 'undefined' ? overlay['show'] : false;
				}, 100);
				this.record = typeof overlay['record'] !== 'undefined' ? overlay['record']['record'] : null;
		    }
		);
		this.shared.alertCallbackMethodFired.subscribe(
			methodObj => {
			  if (methodObj['methodName']) {
			      if (typeof this[methodObj['methodName']] !== 'undefined') {
			          this[methodObj['methodName']]();
			      }
			  }
			}
		);
	}

	addRemoveBlur() {
        let blurArray = [document.querySelector('header'), document.querySelector('ion-content'), document.querySelector('footer')];
        for (let i = 0; i < blurArray.length; i++) {
        	if (blurArray[i]) {
                blurArray[i].classList[this.show ? 'add' : 'remove']('blurred');
			}
		}
	}

	ngOnInit() {}

	closeQuickAccess() {
		this.shared.quickAccessOverlay.next({});
	}

	goToRecordPage() {
        this.comS.pushPage('record-info/' + this.record['record_id']);
        this.closeQuickAccess();
	}

    openAlert() {
        let object = {
            title: this.comS.lang['label_warning'],
            text: {
                top: this.comS.lang['label_remove_record'],
                message: this.comS.lang['label_remove_record_warning']
            },
            buttons: {
                array: [
                    {
                        title: this.comS.lang['button_cancel'],
                        callback: { method: 'cancelRecordQuick' }
                    },
                    {
                        title: this.comS.lang['button_leave'],
                        callback: { method: 'hideAlert' }
                    }
                ],
                containerClassName: ''
            },
            show: true
        };

        console.log(object, 'on openAlert');

        this.comS.showAlert(object);
    }

    hideAlert() {
        this.comS.showAlert({show: false});
    }

    setRecordStatus(status: any) {
        this.hideAlert();
        let statusId = null;
        let keys = Object.keys(this.comS.userStatuses);

        for ( let i = 0; i < keys.length; i++) {
            if (status === this.comS.userStatuses[keys[i]]) {
                statusId = keys[i];
            }
        }

        if (statusId) {
            this.apiS.edit_record(this.record['record_id'], {'status_id': statusId}).subscribe(
                success => {
                    if (success['status']) {
						this.comS.showHint(this.comS.lang['hint_record_canceled']);
                        this.shared.newRecordAdded.next({added: true});
						this.closeQuickAccess();
                    }
                }, error => {
                    if (error.error) {
                        this.comS.showHint(error.error.text.errors);
                    }
                }
            );
        } else {
            this.comS.showHint(this.comS.lang['error_incorrect_record_status']);
        }
    }

    cancelRecordQuick() {
		this.setRecordStatus('records_cancelled');
	}

}
