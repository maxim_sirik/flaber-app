import { Component, OnInit } from '@angular/core';
import {SharedService} from '../../../services/shared.service';
import {CommonService} from '../../../services/common.service';

@Component({
  selector: 'app-popup-hint',
  templateUrl: './hint.component.html',
  styleUrls: ['./hint.component.scss'],
})
export class HintComponent implements OnInit {

  public show = false;
  public message: any = '';
  public continueRegister = false;

  constructor(public shared: SharedService,
              public comS: CommonService) {
      this.shared.hintStatusChanged.subscribe((data) => {
          this.show = data['show'] ? data['show'] : false;
          this.message = data['message'] ? data['message'] : '';
          this.continueRegister = data['continueRegister'] ? data['continueRegister'] : false;
      });
  }

  ngOnInit() {}

  toContinueRegister() {
      this.show = false;
      if (this.continueRegister['url'].length > 0) {
          this.comS.pushPage(this.continueRegister['url']);
      }
  }

}
