import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {CoastFormatPipe} from '../pipes/coast-format.pipe';
import {ServiceDurationFormatPipe} from '../pipes/service-duration-format.pipe';
import {PhoneFormatPipe} from '../pipes/phone-format.pipe';
import { NewRecordInlineComponent } from '../snippets/inline/new-record-inline/new-record-inline.component';
import { ServicesForSectionInlineComponent } from '../snippets/inline/services-for-section-inline/services-for-section-inline.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { CropperComponent } from '../snippets/popup/cropper/cropper.component';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
    imports: [
        CommonModule, FormsModule, ReactiveFormsModule, ImageCropperModule,
        LazyLoadImageModule,
        IonicModule
    ],
    declarations: [
        CoastFormatPipe, ServiceDurationFormatPipe, NewRecordInlineComponent,
        ServicesForSectionInlineComponent, PhoneFormatPipe, CropperComponent
    ],
    exports: [
        CoastFormatPipe, ServiceDurationFormatPipe, NewRecordInlineComponent,
        ServicesForSectionInlineComponent, FormsModule, ReactiveFormsModule,
        PhoneFormatPipe, ImageCropperModule, CropperComponent, LazyLoadImageModule,
        IonicModule
    ]
})

export class SharedModule { }