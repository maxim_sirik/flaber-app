import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WheelSelectorModule } from '@hyperblob/ngx-wheel-selector';
import { HeaderMenuComponent } from '../snippets/header-menu/header-menu.component';
import { MainFooterComponent } from '../snippets/inline/main-footer/main-footer.component';
import { AddRecordComponent } from '../snippets/popup/add-record/add-record.component';
import { AddWorkerComponent } from '../snippets/popup/add-record/add-worker/add-worker.component';
import { AddClientComponent } from '../snippets/popup/add-record/add-client/add-client.component';
import { AddNewClientComponent } from '../snippets/popup/add-record/add-client/add-new-client/add-new-client.component';
import { AddServiceComponent } from '../snippets/popup/add-record/add-service/add-service.component';
import { AddTimeComponent } from '../snippets/popup/add-record/add-time/add-time.component';
import { QuickAccessOverlayComponent } from '../snippets/popup/quick-access-overlay/quick-access-overlay.component';
import { CalendarComponent } from '../snippets/inline/header/calendar/calendar.component';
import { IonicSwipeAllModule } from 'ionic-swipe-all';
import { TapticEngine } from '@ionic-native/taptic-engine/ngx';

@NgModule({
    imports: [
        CommonModule, FormsModule, ReactiveFormsModule, WheelSelectorModule, IonicSwipeAllModule
    ],
    declarations: [
        HeaderMenuComponent, MainFooterComponent, AddRecordComponent, AddWorkerComponent,
        AddClientComponent, AddNewClientComponent, AddServiceComponent, AddTimeComponent,
        QuickAccessOverlayComponent, CalendarComponent
    ],
    exports: [
        HeaderMenuComponent, MainFooterComponent, AddRecordComponent, AddWorkerComponent,
        AddClientComponent, AddNewClientComponent, AddServiceComponent, AddTimeComponent,
        QuickAccessOverlayComponent, FormsModule, ReactiveFormsModule, WheelSelectorModule,
        CalendarComponent, IonicSwipeAllModule
    ],
    providers: [TapticEngine]
})

export class MainGroupSharedModule { }
