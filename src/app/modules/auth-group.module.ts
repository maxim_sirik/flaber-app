import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from '../pages/auth/components/login/login.component';
import { CodeComponent } from '../pages/auth/components/code/code.component';
import { PasswordComponent } from '../pages/auth/components/password/password.component';

@NgModule({
    imports: [
        CommonModule, FormsModule, ReactiveFormsModule
    ],
    declarations: [
        LoginComponent, CodeComponent, PasswordComponent
    ],
    exports: [
        LoginComponent, CodeComponent, PasswordComponent, FormsModule, ReactiveFormsModule
    ]
})

export class AuthGroupModule {}
