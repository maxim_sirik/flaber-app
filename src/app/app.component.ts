import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Globalization } from '@ionic-native/globalization/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Network } from '@ionic-native/network/ngx';
import {SharedService} from './services/shared.service';
import {NotificationsService} from './services/notifications.service';
import {SocketService} from './services/socket-service.service';
import {CommonService} from './services/common.service';
import {PushNotificationsService} from './services/push-notifications.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  public alertShow = false;
  public deviceClassName = '';

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private globalization: Globalization,
    private shared: SharedService,
    private notificationsService: NotificationsService,
    private socketS: SocketService,
    private backgroundMode: BackgroundMode,
    private comS: CommonService,
    private network: Network,
    private pushS: PushNotificationsService
  ) {
    this.initializeApp();

    this.shared.alertStatusChanged.subscribe((data) => {
        this.alertShow = data['show'];
    });

    this.shared.alertCallbackMethodFired.subscribe(
        methodObj => {
            if (methodObj['methodName']) {
                if (typeof this[methodObj['methodName']] !== 'undefined') {
                    this[methodObj['methodName']]();
                }
            }
        }
     );

    this.shared.onConnectToDialogue.subscribe(
        data => {
            if (data) {
                this.chat_connect_to_dialogue(data);
            }

        }
    );

    this.shared.onSendMessage.subscribe(
        data => {
            if (data) {
                this.send_message_to_chat(data);
            }
        }
    );

    this.shared.onCreateDialog.subscribe(
        data => {
            if (data) {
                this.create_dialog(data);
            }
        }
    );

    this.shared.onLogin.subscribe(
        data => {

            if (data === true) {
                console.log('on shared onLogin subscribe', this.pushS, data);
                // this.socketS.connectToSocket();
                this.pushS.checkPushPermission();
            }
        }
    );
  }

  initializeApp() {
    this.platform.ready().then(() => {
        this.statusBar.overlaysWebView(true);
        this.statusBar.styleDefault();
        this.splashScreen.hide();
        this.backgroundMode.enable();
        if (this.comS.isHasToken()) {
            this.socketS.connectToSocket();
        }
        // if (this.platform.is('iphone')) {
        // this.deviceClassName = 'iphone';
      // }
    });
  }

  receivePushNotifications() {
      // this.notificationsService.receiveMessage();
  }

  chat_connect_to_dialogue(data: any) {
      // let data = {
      //     token: this.comS.getToken(),
      //     dialog_id: dialog_id !== null ? dialog_id : this.currentDialog
      // };
      this.socketS.connectToRoomRequest(data);
  }

  send_message_to_chat(data: any) {
      this.socketS.sendMessageRequest(data);
  }

  create_dialog(data: any) {
      this.socketS.createDialogRequest(data);
  }

  //   initStatusBarMethod() {
  //       setTimeout(() => {
  //           console.log(JSON.stringify(this.statusBar), 'initStatusBar ___');
  //           this.statusBar.overlaysWebView(false);
  //           this.statusBar.backgroundColorByName('darkGray');
  //       }, 5000);
  // }
}
