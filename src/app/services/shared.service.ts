import { Injectable, EventEmitter } from '@angular/core';
import {Observable, BehaviorSubject} from 'rxjs';
@Injectable()
export class SharedService {
    activeTab = new BehaviorSubject('schedule');
    hintShowed = new BehaviorSubject({});
    headerMenuStatus = new BehaviorSubject({});
    sidebarShowed = new BehaviorSubject(false);
    searchShowed = new BehaviorSubject({});
    alertShowed = new BehaviorSubject({});
    newRecord = new BehaviorSubject({});
    serviceSection = new BehaviorSubject({});
    serviceObject = new BehaviorSubject({});
    searchFinished = new BehaviorSubject({});
    alertCallbackMethod = new BehaviorSubject({});
    serviceNameOnSearch = new BehaviorSubject({});
    addressOnSearch = new BehaviorSubject({});
    clientOnSearch = new BehaviorSubject({});
    // new record ------ START
    newRecordModalResetTabs = new BehaviorSubject({});
    newRecordModalResetTabsInit: Observable<any> = this.newRecordModalResetTabs.asObservable();
    newRecordUser = new BehaviorSubject({});
    newRecordUserSelected: Observable<any> = this.newRecordUser.asObservable();
    newRecordService = new BehaviorSubject({});
    newRecordServiceSelected: Observable<any> = this.newRecordService.asObservable();
    newRecordTime = new BehaviorSubject({});
    newRecordTimeSelected: Observable<any> = this.newRecordTime.asObservable();
    newRecordAdded = new BehaviorSubject({});
    newRecordAddedHandler: Observable<any> = this.newRecordAdded.asObservable();
    hideHeaderToShowNewRecord = new BehaviorSubject({});
    hideHeaderToShowNewRecordHandler: Observable<any> = this.hideHeaderToShowNewRecord.asObservable();
    recordMove = new BehaviorSubject({});
    recordMoveCallback: Observable<any> = this.recordMove.asObservable();
    // new record ------ END
    // quick access overlay ------ START
    quickAccessOverlay = new BehaviorSubject({});
    quickAccessOverlayShow: Observable<any> = this.quickAccessOverlay.asObservable();
    // quick access overlay ------ END
    // works ------ START
    workChanged = new BehaviorSubject({});
    workChangedHandler: Observable<any> = this.workChanged.asObservable();
    // works ------ END
    // calendar ------ START
    dateOnMonth = new BehaviorSubject({});
    dateOnMonthSelected: Observable<any> = this.dateOnMonth.asObservable();
    // calendar ------ END
    // chat ------ START
    connectToDialogue = new BehaviorSubject({});
    onConnectToDialogue: Observable<any> = this.connectToDialogue.asObservable();
    connectedToDialogue = new BehaviorSubject({});
    onConnectedToDialogue: Observable<any> = this.connectedToDialogue.asObservable();
    sendMessage = new BehaviorSubject({});
    onSendMessage: Observable<any> = this.sendMessage.asObservable();
    getMessage = new BehaviorSubject({});
    onGetMessage: Observable<any> = this.getMessage.asObservable();
    createDialog = new BehaviorSubject({});
    onCreateDialog: Observable<any> = this.createDialog.asObservable();
    // chat ------ END
    mainTabChanged: Observable<any> = this.activeTab.asObservable();
    hintStatusChanged: Observable<any> = this.hintShowed.asObservable();
    sidebarStatusChanged: Observable<any> = this.sidebarShowed.asObservable();
    searchStatusChanged: Observable<any> = this.searchShowed.asObservable();
    alertStatusChanged: Observable<any> = this.alertShowed.asObservable();
    showNewRecord: Observable<any> = this.newRecord.asObservable();
    headerMenuStatusChanged: Observable<any> = this.headerMenuStatus.asObservable();
    serviceSectionSelected: Observable<any> = this.serviceSection.asObservable();
    serviceObjectAdded: Observable<any> = this.serviceObject.asObservable();
    alertCallbackMethodFired: Observable<any> = this.alertCallbackMethod.asObservable();
    searchFinishedObserver: Observable<any> = this.searchFinished.asObservable();
    serviceNameOnSearchSelected: Observable<any> = this.serviceNameOnSearch.asObservable();
    addressOnSearchSelected: Observable<any> = this.addressOnSearch.asObservable();
    clientOnSearchSelected: Observable<any> = this.clientOnSearch.asObservable();

    // user login ------ START
    login = new BehaviorSubject({});
    onLogin: Observable<any> = this.login.asObservable();
    // user login ------ END

    constructor() { }

}
