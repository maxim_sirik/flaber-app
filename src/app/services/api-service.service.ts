import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import {CommonService} from './common.service';

@Injectable()

export class ApiService {

  constructor(private httpClient: HttpClient,
              public comS: CommonService) { }
    // для карт
    // геокодинг - https://nominatim.org/release-docs/develop/api
    // карты - https://openlayers.org/en/latest/apidoc/

  public static apiBase = 'http://flaber.dev-3.com/api/v1/';
  public static geocodeBase = 'https://nominatim.openstreetmap.org/';

  static get authHeaders(): HttpHeaders {
    const token = localStorage.getItem('token');
    const locale = localStorage.getItem('locale');
    return new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
        // 'Authorization': 'Bearer ' + token,
        'X-Localization': locale ? locale.replace(/"/g, '') : 'ru'
    });
  }

  static handleError(error: any) {
      return throwError(error);
  }

  checkStatusCodeOnError(error, apiS) {
      if (error.status === 401) {
          apiS.comS.logoutUser();
      } else if (error.status === 403) {
          apiS.comS.pushBack();
      }
      return ApiService.handleError(error);
  }

  updateUserStatuses() {
      this.get_user_statuses_revert().subscribe(
          success => {
              this.comS.userStatuses = success['text']['statuses'];
          }
      );
  }

  /////////// USER

  login_request(object: any) {
      return this.httpClient.post(ApiService.apiBase + 'user/login',
          object);
  }
  confirm_sms(object: any) { // on register/password restore
      return this.httpClient.post(ApiService.apiBase + 'user/sms_code_confirm', object);
  }
  send_sms(object: any) { // on register/password restore
      return this.httpClient.post(ApiService.apiBase + 'user/sms_code', object);
  }
  register(object: any) {
      return this.httpClient.post(ApiService.apiBase + 'user/register', object);
  }
  password_restore(object: any) {
      return this.httpClient.post(ApiService.apiBase + 'user/password_restore', object);
  }
  logout() {
      return this.httpClient.get(ApiService.apiBase + 'user/logout', {headers: ApiService.authHeaders});
  }
  user_info() {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'user/details', {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  update_user(object: any) {
      const __this = this;
      return this.httpClient.post(ApiService.apiBase + 'user/update', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }

  /////////// WORKER

  register_worker(object: any) {
      const __this = this;
      return this.httpClient.post(ApiService.apiBase + 'workers/add', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_work_days() {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'schedule/days', {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  update_work_days(time_id: any, object: any) {
      const __this = this;
      return this.httpClient.put(ApiService.apiBase + 'schedule/work-times/' + time_id, object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  delete_work_days(time_id: any) {
      const __this = this;
      return this.httpClient.delete(ApiService.apiBase + 'schedule/work-times/' + time_id, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  add_work_days(object: any) {
      const __this = this;
      return this.httpClient.post(ApiService.apiBase + 'workers/work-times/add', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  update_worker_profile(object: any) {
      const __this = this;
      return this.httpClient.put(ApiService.apiBase + 'workers/my-profile/update', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_worker_schedule() {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'workers/schedule', {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_my_profile() {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'workers/my', {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_worker_reviews(workerId: any, pageId: any = 1, perPage: any = 3) {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'workers/' + workerId + '/reviews?page=' + pageId + '&per_page=' + perPage, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }

  /////////// WORKS

  get_worker_works(workerId: any) {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'workers/' + workerId + '/works', {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_worker_customer_works(workerId: any) {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'workers/' + workerId + '/customer-works', {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_company_works(companyId: any) {
      // http://flaber.dev-3.com/api/v1/companies/:company_id/works
  }
  get_company_customer_works(companyId: any) {
      // http://flaber.dev-3.com/api/v1/companies/:company_id/customer-works
  }
  show_work(workId: any) {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'works/' + workId, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  add_master_work(workerId: any, object: any) {
      const __this = this;
      return this.httpClient.post(ApiService.apiBase + 'workers/' + workerId + '/works/add', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  add_company_work(companyId: any) {
      // http://flaber.dev-3.com/api/v1/companies/:company_id/works/add
  }
  update_work(workId: any, object) {
      // http://flaber.dev-3.com/api/v1/works/:work_id
      const __this = this;
      return this.httpClient.put(ApiService.apiBase + 'works/' + workId, object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  change_work_order(workId: any, object: any) {
      const __this = this;
      return this.httpClient.post(ApiService.apiBase + 'works/' + workId + '/sort', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  bulk_change_work_order(object: any) {
      const __this = this;
      return this.httpClient.put(ApiService.apiBase + 'works/sort/change', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  delete_work(workId: any) {
      const __this = this;
      return this.httpClient.delete(ApiService.apiBase + 'works/' + workId, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_worker_services_new_work(workerId: any) {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'workers/' + workerId + '/providers', {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }


  /////////// SERVICES

  get_services_sections() {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'services/sections', {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  add_new_service(object: any) {
      const __this = this;
      return this.httpClient.post(ApiService.apiBase + 'services/add', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_services(workerId: any) {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'workers/' + workerId + '/services', {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_all_available_services() {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'services/all', {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  add_provider(object: any) {
      const __this = this;
      return this.httpClient.post(ApiService.apiBase + 'services/providers/add', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  update_provider(id: any, object: any) {
      const __this = this;
      return this.httpClient.put(ApiService.apiBase + 'services/providers/' + id, object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_services_by_category_id(id: any) {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'services/by-section/' + id, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  delete_provider(id: any) {
      const __this = this;
      return this.httpClient.delete(ApiService.apiBase + 'services/providers/' + id, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  add_custom_category(object: any) {
      const __this = this;
      return this.httpClient.post(ApiService.apiBase + 'services/sections/add', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }

  /////////// COMMON

  get_address_data(address) {
      return this.httpClient.get(ApiService.geocodeBase + 'search?q=' + address + '&addressdetails=1&format=json');
  }
  getPlaceData(osmId: any) {
      // https://nominatim.openstreetmap.org/lookup?osm_ids=W181582482&format=json
      return this.httpClient.get(ApiService.geocodeBase + 'lookup?osm_ids=' + osmId + '&format=json');
  }
  update_user_info() {
    this.user_info().subscribe(
        success => {
            if (success['status']) {
                localStorage.setItem('user', JSON.stringify(success['text']['user']));
                this.comS.setLang(success['text']['user']['language']['locale']);
                this.comS.userInfo = success['text']['user'];
            }
        }, error => {
            console.log(error);
        });
                              
  }

  /////////// SPECIAL
  get_user_statuses() {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'special/show-statuses', {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_user_statuses_revert() {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'special/show-statuses2', {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_langs() {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'special/show-langs', {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }

  /////////// CUSTOMERS
  add_customer(object: any) { //  можно использовать для добавления контакта
    const __this = this;
    return this.httpClient.post(ApiService.apiBase + 'customers/add', object, {headers: ApiService.authHeaders})
        .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
    );
  }
  add_multiple_customers(object: any) {
      const __this = this;
      return this.httpClient.post(ApiService.apiBase + 'customers/add-multiple', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_customers(page: any = null) {
    const __this = this;
    return this.httpClient.get(ApiService.apiBase + 'customers/my' + (page !== null ? '?page=' + page : ''), {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_customer_by_id(id: any) {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'customers/' + id, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  update_customer(id: any, object: any) {
      const __this = this;
      return this.httpClient.put(ApiService.apiBase + 'customers/' + id, object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  delete_customer(id: any) {
      const __this = this;
      return this.httpClient.delete(ApiService.apiBase + 'customers/' + id, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }

  /////////// RECORDS
  get_records_by_date(dateString: any, workerId: any) {

      // http://flaber.dev-3.com/api/v1/records/8/worker-date?start_date=2019-07-15&end_date=2019-07-15
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'records/' + workerId + '/worker-date?' + dateString, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_record_by_id(recordId: any) {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'records/' + recordId, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  add_new_record(object: any) {
      const __this = this;
      return this.httpClient.post(ApiService.apiBase + 'records/add', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_archived_records(workerId: any, pageId: any = 1, perPage: any = 3) {
      const __this = this;
      return this.httpClient.get(ApiService.apiBase + 'workers/' + workerId + '/records-history?page=' + pageId + '&per_page=' + perPage, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  edit_record(recordId: any, object: any) {
      const __this = this;
      return this.httpClient.put(ApiService.apiBase + 'records/' + recordId, object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  change_record_settings(recordId: any, object: any) {
      const __this = this;
      return this.httpClient.put(ApiService.apiBase + 'records/' + recordId + '/settings', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
          );
  }
  delete_record(recordId: any) {
      // DELETE http://flaber.dev-3.com/api/v1/records/{record_id}/
      const __this = this;
      return this.httpClient.delete(ApiService.apiBase + 'records/' + recordId, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }
  get_busy_days(date: any) {
    const __this = this;
    return this.httpClient.get(ApiService.apiBase + 'records/my/by-month?date=' + date, {headers: ApiService.authHeaders})
        .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
    );
  }

  /////////// SETTINGS
  update_basic_settings(object: any) {
      const __this = this;
      return this.httpClient.put(ApiService.apiBase + 'settings/basic', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }

  update_settings_password(object: any) {
      // {
      //     "old_password": "admin",
      //     "new_password": "1111",
      //     "new_password1": "1111"
      // }
      const __this = this;
      return this.httpClient.put(ApiService.apiBase + 'settings/password', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }

  addFBToken(object: any) {
      const __this = this;
      return this.httpClient.post(ApiService.apiBase + 'user/add-firebase-token', object, {headers: ApiService.authHeaders})
          .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
      );
  }

    setFBUID(object: any) {
        const __this = this;
        return this.httpClient.put(ApiService.apiBase + 'user/set-firebase-uid', object, {headers: ApiService.authHeaders})
            .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
            );
    }


    ////////////// DIALOGS
    get_dialog_list(page: any = null) {
        const __this = this;
        return this.httpClient.get(ApiService.apiBase + 'chat/dialogs' + (page !== null ? '?page=' + page : ''), {headers: ApiService.authHeaders})
            .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
        );
    }

    get_dialog_info(id: any) {
        const __this = this;
        return this.httpClient.get(ApiService.apiBase + 'chat/dialogs/' + id, {headers: ApiService.authHeaders})
            .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
            );
    }

    get_dialog_messages(id: any, last_msg_id: any = null) {
        const __this = this;
        return this.httpClient.get(ApiService.apiBase + 'chat/dialogs/' + id + '/messages' + (last_msg_id !== null ? '?last_msg_id=' + last_msg_id : ''), {headers: ApiService.authHeaders})
            .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
            );
    }

    set_messages_read(object: any) {
        const __this = this;
        return this.httpClient.put(ApiService.apiBase + 'chat/messages/read', object, {headers: ApiService.authHeaders})
            .pipe(catchError(error => this.checkStatusCodeOnError(error, __this)));
    }

    ////////////// NOTIFICATIONS
    get_notifications_list(page) {
        const __this = this;
        return this.httpClient.get(ApiService.apiBase + 'notifications/list?page=' + page, {headers: ApiService.authHeaders})
            .pipe(catchError(error => this.checkStatusCodeOnError(error, __this))
            );
        // http://flaber.dev-3.com/api/v1/notifications/list
    }
}
