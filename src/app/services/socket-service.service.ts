import io from 'socket.io-client/dist/socket.io.slim.js';
import {CommonService} from './common.service';
import {Injectable} from '@angular/core';
import {SharedService} from './shared.service';
import {ApiService} from './api-service.service';
import {LocalNotificationService} from './local-notification.service';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';

@Injectable()

export class SocketService {

  public socket: any;
  public onGoToDialog = false;
  public joined: any = [];
  public toJoin: any = [];
  public usersStatus: any = {};
  private wasDisconnected: boolean = false;

  constructor(public comS: CommonService,
              private shared: SharedService,
              private apiS: ApiService,
              private localN: LocalNotificationService,
              private backgroundMode: BackgroundMode) {
  }

  reconnectToRooms() {
      for (let join of this.joined) {
          this.connectToRoomRequest({
              dialog_id: join,
              token: this.comS.getToken()
          });
      }
  }

    connectToSocket(callback: any = null, callbackParams: any = null) {
        if (this.comS.updateUser()) {
            // this.socket = io('http://78.46.194.182:2783/?user_id=' + this.comS.userInfo['id']);
            this.socket.on('connect', () => {
              console.log('connected to server');
              this.getRoomsToConnect();
              if (this.wasDisconnected) {
                  this.wasDisconnected = false;
                  this.reconnectToRooms();
              }
            });

            this.socket.on('disconnect', (reason) => {
                this.wasDisconnected = true;
                if (reason === 'io server disconnect') {
                    this.connectToSocket();
                }
            });

            this.socket.on('send dialog request', (data) => {
                if (this.comS.updateUser()) {
                    if ((data.id_to === this.comS.userInfo['id']) || this.onGoToDialog) {
                        if (this.joined.indexOf(data.dialog_id) === -1 && this.toJoin.indexOf(data.dialog_id) !== -1) {
                            this.connectToRoomRequest({
                                dialog_id: data.dialog_id,
                                token: this.comS.getToken(),
                                onGoToDialog: this.onGoToDialog
                            });
                        }
                    }
                }
            });

            this.socket.on('error creating dialog', (data) => {
                if (data.exists) {
                    this.comS.pushPage('main/messages/dialogue/' + data.dialog_id);
                } else {
                    this.comS.showHint(this.comS.lang['error_dialog_creation']);
                }
            });

            this.socket.on('message added', (data) => {
                this.shared.getMessage.next(data);
                if (window.location.pathname.indexOf('messages') === -1 || this.backgroundMode.isActive()) {
                    this.localN.showNotification(data);
                }
            });

            this.socket.on('connected to room', (data) => {
                console.log('connected to room', data.dialog_id);
            });

            this.socket.on('new_client', (data) => {
                this.socket.emit('add_client', localStorage.getItem('token'));
            });

            this.socket.on('online users list', (data) => {
                if (this.comS.userInfo['id'] === data['worker']) {
                    for (let user of Object.keys(data['users'])) {
                        this.usersStatus[user] = data['users'][user];
                    }
                }

                this.comS.setUserStatuses(this.usersStatus);
            });

            this.socket.on('user connected', (data) => {
                if (typeof this.usersStatus[data['user_id']] !== 'undefined') {
                    this.usersStatus[data['user_id']] = true;
                }
                this.comS.setUserStatuses(this.usersStatus);
            });

            this.socket.on('user disconnected', (data) => {
                if (typeof this.usersStatus[data['user_id']] !== 'undefined') {
                    this.usersStatus[data['user_id']] = false;
                }
                this.comS.setUserStatuses(this.usersStatus);
            });
        }
    }

    public connectToRoomRequest(data: any = null) {
        if (this.socket) {
          if (this.socket.connected) {
              if (this.comS.updateUser()) {
                  this.onGoToDialog = data['onGoToDialog'] !== null ? data['onGoToDialog'] : false;
                  this.socket.emit('join to room', data);
              } else {
                  this.comS.logoutUser(); // нет данных о пользователе, его нужно разлогинить
              }
          } else {
              if (data['requested']) {
                  this.comS.showHint(this.comS.lang['error_no_socket_connection']); // дисконект с сокетом
              }
          }
        } else {
            // if (!data['requested']) {
            //     data['requested'] = true;
            //     this.connectToSocket('connectToRoomRequest', data);
            // }
        }
    }

    public createDialogRequest(data: any) {
        if (this.socket) {
            if (this.socket.connected) {
                if (this.comS.updateUser()) {
                    this.onGoToDialog = data['onGoToDialog'] !== null ? data['onGoToDialog'] : false;
                    this.socket.emit('private dialog request', data);
                } else {
                    this.comS.logoutUser(); // нет данных о пользователе, его нужно разлогинить
                }
            } else {
                if (data['requested']) {
                    this.comS.showHint(this.comS.lang['error_no_socket_connection']); // дисконект с сокетом
                }
            }
        } else {
            // if (!data['requested']) {
            //     data['requested'] = true;
            //     this.connectToSocket('createDialogRequest', data);
            // }
        }
    }

    public sendMessageRequest(data: any) {
        if (this.socket) {
            if (this.socket.connected) {
                if (this.comS.updateUser()) {
                    this.socket.emit('new message', data);
                } else {
                    this.comS.logoutUser(); // нет данных о пользователе, его нужно разлогинить
                }
            } else {
                if (data['requested']) {
                    this.comS.showHint(this.comS.lang['error_no_socket_connection']); // дисконект с сокетом
                }
            }
        } else {
            // if (!data['requested']) {
            //     data['requested'] = true;
            //     this.connectToSocket('sendMessageRequest', data);
            // }
        }
    }

    getRoomsToConnect(page: any = 1) {
        this.apiS.get_dialog_list(page).subscribe(
            success => {
                if (success['status']) {
                    for (let dialog of success['text']['dialogs']['data']) {
                        if (this.toJoin.indexOf(dialog['id']) === -1) {
                            this.toJoin.push(dialog['id']);
                        }
                    }
                    this.checkUsersStatusRequest(success['text']['dialogs']['data']);
                    if (success['text']['dialogs']['current_page'] < success['text']['dialogs']['last_page']) {
                        this.getRoomsToConnect(success['text']['dialogs']['current_page'] + 1);
                    } else {
                        for (let join of this.toJoin) {
                            if (this.joined.indexOf(join) === -1) {
                                this.joined.push(join);
                                this.connectToRoomRequest({
                                    dialog_id: join,
                                    token: this.comS.getToken(),
                                    onGoToDialog: this.onGoToDialog
                                });
                            }
                        }
                    }
                }
            }, error => {
                console.log(error, 'getRoomsToConnect error');
            }
        );
    }

    checkUsersStatusRequest(dialogsData: any) {
        let users = [];
        for (let dialog of dialogsData) {
            if (dialog['dialog_users'].length > 0) {
                if (users.indexOf(dialog['dialog_users'][0]['user_id']) === -1) {
                    users.push(dialog['dialog_users'][0]['user_id']);
                }
            }
        }

        this.socket.emit('get online users', {users, token: this.comS.getToken()});
    }
}
