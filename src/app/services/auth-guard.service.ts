import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import {CommonService} from './common.service';
import {ApiService} from './api-service.service';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(public comS: CommonService,
                public apiS: ApiService) { }

    canActivate(): boolean {
        let activated = this.comS.isHasToken();
        if (activated) {
            this.isUserActivated();
        }
        return activated;
    }

    isUserActivated() {
        let activated = true;
        this.apiS.get_user_statuses_revert().subscribe(
            success => {
                if (success['status']) {
                    this.comS.userStatuses = success['text']['statuses'];
                    if (!this.comS.userInfo['status_id']) {
                        if (localStorage.getItem('user')) {
                            this.comS.userInfo = JSON.parse(localStorage.getItem('user'));
                        }
                    }
                    if (this.comS.userInfo['status_id']) {
                        if (success['text']['statuses'][this.comS.userInfo['status_id']] === 'users_not_completed') {
                            this.comS.showHint(this.comS.lang['hint_trial_version'], {text: this.comS.lang['hint_continue_register'], url: '/personal-information'}, 0);
                        }
                    }
                }

            }, error => {
                console.log(error);
            }
        );
    }

}
