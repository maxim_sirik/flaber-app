import { Injectable } from '@angular/core';
import {NavController} from '@ionic/angular';
import {SharedService} from './shared.service';
import * as moment from 'moment';
import io from 'socket.io-client/dist/socket.io.slim.js';
import {StatusBar} from '@ionic-native/status-bar/ngx';

@Injectable()

export class CommonService {
  public appLogo = require('../../assets/images/logo.svg');
  public defaultPhoto = require('../../assets/images/defaultUserImage.jpg');
  public appLang = 'ru';
  public lang = {};
  public userStatuses: any;
  public onClickOutsideTimeout: any = false;
  public userInfo = {};
  public workerInfo: any;
  public workerForProfile: any;
  public socket: any;
  public phoneMask = '+ 380 (99) 999 99 99';
  public usersStatuses: any = null;
  public alphabet = [
      {
          words: 'абвгдеёжзиклмнопрстуфхцчшщьъыэюя'.split('')
      },
      {
          words: 'абвгґдеєжзиіїйклмнопрстуфхцчшщьюя'.split('')
      },
      {
          words: 'abcdefghijklmnopqrstuvwxyz'.split('')
      },
      {
          words: '0123456789'.split('')
      }
  ];
  constructor(public navCtrl: NavController,
              public shared: SharedService,
              private statusBar: StatusBar) {
      this.setLang();
      // document.addEventListener('click', (ev) => {
      //     console.log('global click event');
      //     if (this.onClickOutsideTimeout) {
      //         console.log('click on onClickOutsideTimeout');
      //         ev.stopPropagation();
      //     }
      // });
      this.clickOutSide('.tab-container', 'closeNewRecordModal', ['icon-plus', 'icon-pluse', 'fill-in__tab', 'contact-row', 'icon-check', 'popup-hint', 'popup-modal__overlay', 'form-button__fill']);
      this.clickOutSide('.circle-menu-item', 'closeQuickAccess', ['icon-tascs', 'icon-pluse', 'icon-burger-thin', 'circle-menu-item', 'popup-hint', 'popup-modal__overlay', 'ellipsis-container']);
      this.clickOutSide('.popup-modal', 'hideAlert', ['btn-link', 'form-button__fill_dark', 'form-button__fill', 'active-service', 'circle-menu-item']);
      this.addKeyboardListeners();
      // this.setStatusBarStyle('#fff');


      window.addEventListener('keyboardDidShow', (event) => {
          // Describe your logic which will be run each time when keyboard is about to be shown.
          // console.log(event.keyboardHeight);
          // alert('keyboardDidShow');
      });
  }

  public static formatDuration(duration: any, format: any) {
      return moment(duration).format(format);
  }

  public static showPicker(selector) {
      const picker = document.querySelector(selector) as HTMLElement;
      if (picker) {
          picker.click();
      }
  }

  public static formatCoast(value: any) {
      if (! (parseFloat(value) > parseInt(value, null))) {
          value = parseFloat(value).toFixed(0);
      } else {
          value = parseFloat(value).toFixed(2);
      }
      return value;
  }

  public setStatusBarStyle(color: any, lightTheme: boolean = true) {
      // if (lightTheme) {
      //     this.statusBar.backgroundColorByHexString(color);
      // }
  }

  public setUserStatuses(statuses: any) {
      this.usersStatuses = statuses;
  }

  public getType(variable: any) {
      return typeof variable;
  }

  public getIntValue(value: any) {
      return parseInt(value, null);
  }

  public clickOutSide(selector, callback, exception) {
      document.addEventListener('click', (ev) => {
        let target = document.querySelector(selector);
        if (target) {
          let evTarget = ev.target as Element;
          if (!this.containsMany(evTarget, exception)) {
              if (!this.findAncestor(evTarget, selector)) {
                  ev.preventDefault();
                  ev.stopPropagation();
                  this.shared.alertCallbackMethod.next({ methodName: callback});
              }
          }
        }
      });
  }

  public getElementCompleteHeight(selector, withoutPadding = false) {
    let elmHeight, elmMargin, elmMPadding, elm = document.querySelector(selector) as HTMLElement;
    if (elm) {
        if (document['all']) { // IE
            elmHeight = parseInt(elm['currentStyle'].height, null);
            elmMargin = parseInt(elm['currentStyle'].marginTop, 10) + parseInt(elm['currentStyle'].marginBottom, 10);
        } else { // Mozilla
            elmHeight = parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('height'), null);
            elmMargin = parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('margin-top'), null) + parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('margin-bottom'), null);
            elmMPadding = parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('padding-top'), null) + parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('padding-bottom'), null);
        }
        return (elmHeight + elmMargin + withoutPadding ? 0 : elmMPadding);
    } else {
        return 0;
    }
  }

  public calendarOnSwipe(delta: any, blockWidth: any) {
      let listsContainer = document.querySelector('.lists-container') as HTMLElement;
      let calendarFooter = document.querySelector('.calendar-footer') as HTMLElement;
      let listContainerTransform = parseInt(window.getComputedStyle(listsContainer, null).getPropertyValue('transform').split(',')[4]);
      let calendarFooterTransform = parseInt(window.getComputedStyle(calendarFooter, null).getPropertyValue('transform').split(',')[4]);

      listsContainer.style.transform = 'translateX(' + ((listContainerTransform ? (listContainerTransform * 100 / blockWidth) : 0) + delta)  + 'vw)';
      listsContainer.style.transition = 'none';
      calendarFooter.style.transform = 'translateX(' + ((calendarFooterTransform ? (calendarFooterTransform * 100 / blockWidth) : 0) + delta * 0.75) + 'vw)';
      calendarFooter.style.transition = 'none';
  }

  public calendarOnSwipeEnd(delta: any) {
      let listsContainer = document.querySelector('.lists-container') as HTMLElement;
      let calendarFooter = document.querySelector('.calendar-footer') as HTMLElement;
      let listContainerTransform = parseInt(window.getComputedStyle(listsContainer, null).getPropertyValue('transform').split(',')[4]);
      let calendarFooterTransform = parseInt(window.getComputedStyle(calendarFooter, null).getPropertyValue('transform').split(',')[4]);
      let blockWidth = document.body.offsetWidth;

      listsContainer.style.transform = 'translateX(' + Math[delta < 0 ? 'floor' : 'ceil'](listContainerTransform / blockWidth) * 100 + 'vw)';
      listsContainer.style.transition = 'all .5s';
      calendarFooter.style.transform = 'translateX(' + Math[delta < 0 ? 'floor' : 'ceil']((calendarFooterTransform / 0.73) / blockWidth) * 73 + 'vw)';
      calendarFooter.style.transition = 'all .5s';
  }

  public resetSwipeState() {
      let listsContainer = document.querySelector('.lists-container') as HTMLElement;
      let calendarFooter = document.querySelector('.calendar-footer') as HTMLElement;

      listsContainer.style.transform = 'translateX(-100vw)';
      listsContainer.style.transition = 'none';
      calendarFooter.style.transform = 'translateX(-146vw)';
      calendarFooter.style.transition = 'none';
  }

  public containsMany(el: any, classes: any) {
      let contains = false;
      classes.forEach((className) => {
          if (el.classList.contains(className) || this.findAncestor(el, '.' + className)) {
              contains = true;
          }
      });
      return contains;
  }

  public findAncestor(el, sel) {
      while ((el = el.parentElement) && !((el.matches || el.matchesSelector).call(el, sel)));
      return el;
  }

  public showAlert(object) {
      this.shared.alertShowed.next(object);
  }

  public removeFromArray(array: any, element: any) {
      array.forEach((item) => {
          if (item === element) {
              array.splice(array.indexOf(element), 1);
          }
      });
      return array;
  }

  public pushPage(route: any) {
      this.navCtrl.navigateForward(route);
  }

  public pushBack() {
      this.navCtrl.pop();
      // console.log(this.navCtrl, 'pushBack');
      // let lastNavId = this.navCtrl['lastNavId'];
      // // console.log(this.navCtrl.lastNavId, 'this.navCtrl before');
      // let response = this.navCtrl.pop().then((resp) => {
      //     console.log(lastNavId, this.navCtrl['lastNavId'], 'response');
      //     if ((lastNavId === this.navCtrl['lastNavId']) && (lastNavId === 1)) {
      //         console.log('equals');
      //         this.pushPage('/main/records');
      //     } else {
      //         console.log('not equals');
      //     }
      //
      //     // console.log(resp, 'pushBack');
      //     // console.log(this.navCtrl.lastNavId, 'this.navCtrl after');
      // });
      // console.log(response, 'pushBack');
  }

  public showHint(message: any, continueRegister: any = false, timer = 5000) {
      this.shared.hintShowed.next({ show: true, message, continueRegister});

      if (timer !== 0) {
          setTimeout(() => {
              this.hideHint();
          }, timer);
      }
  }

  public showQuickAcess(obj: any) {
      console.log('showQuickAcess', obj);
    this.shared.quickAccessOverlay.next(obj);
  }

  showSearch(object: any) {
      this.shared.searchShowed.next(object);
  }

  isHasToken() {
      let token = this.getToken();
      if (!token) {
          this.navCtrl.navigateRoot(['/login']);
          return false;
      }
      return true;
  }

  public getToken() {
      return localStorage.getItem('token');
  }

  public hideHint() {
      this.shared.hintShowed.next({show: false});
  }

  public toggleSidebar(show) {
      this.shared.sidebarShowed.next(show);
  }

  public setLang(lang = null) {
      if (lang) {
          localStorage.setItem('locale', lang);
      }
      this.appLang = localStorage.getItem('locale') ? localStorage.getItem('locale').replace(/"/g, '') : 'ru';
      this.lang = require('../../assets/json/lang/' + this.appLang + '.json');
  }

  public updateWorker() {
      if (localStorage.getItem('worker_obj')) {
          if (typeof this.workerInfo === 'undefined' || typeof this.workerInfo['id'] === 'undefined') {
              this.workerInfo = JSON.parse(localStorage.getItem('worker_obj'));
          }
          return true;
      } else {
          return false;
      }
  }

  public updateProfileWorker() {
      if (localStorage.getItem('worker')) {
          // if (typeof this.workerForProfile === 'undefined' || typeof this.workerForProfile['id'] === 'undefined') {
          this.workerForProfile = JSON.parse(localStorage.getItem('worker'));
          // }
          return true;
      } else {
          return false;
      }
  }

  public getNearestByCode(code: any) {
      let response = [this.lang['label_yesterday'], this.lang['label_today'], this.lang['label_tomorrow']];
      return typeof response[code] !== 'undefined' ? response[code] : null;
  }

  public updateUser() {
      if (localStorage.getItem('user')) {
          if (typeof this.userInfo === 'undefined' || this.userInfo && typeof this.userInfo['id'] === 'undefined') {
              this.userInfo = JSON.parse(localStorage.getItem('user'));
          }
          return this.userInfo && this.userInfo['id'] !== null;
      } else {
          return false;
      }
  }

  public updateAppLocale() {
    if (localStorage.getItem('locale')) {
        if (typeof this.workerInfo === 'undefined' || typeof this.workerInfo['id'] === 'undefined') {
            this.workerInfo = JSON.parse(localStorage.getItem('worker_obj'));
        }
        return true;
    } else {
        return false;
    }
  }

  public getCalendar(dateTime: any = new Date(), locale = this.appLang) {
      let calendar = [];
      const startDay = moment(dateTime).locale(locale).startOf('month').startOf('week');
      const endDay = moment(dateTime).locale(locale).endOf('month').endOf('week');

      let date = startDay.subtract(1, 'day');

      while (date.isBefore(endDay, 'day')) {
          calendar.push({
              days: Array(7).fill(0).map(() => date.add(1, 'day').clone())
          });
      }
      return calendar;
  }

  public dynamicSort(property) {
      let sortOrder = 1;
      if (property[0] === '-') {
          sortOrder = -1;
          property = property.substr(1);
      }
      return (a, b) => {
          let result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
          return result * sortOrder;
      };
  }

  public getSelectedWeekDays(dateTime: any = new Date(), locale = this.appLang) {
      let startOfWeek = moment(dateTime).locale(locale).startOf('isoWeek');
      let endOfWeek = moment(dateTime).locale(locale).endOf('isoWeek');

      let days = [];
      let day = startOfWeek;

      while (day <= endOfWeek) {
          days.push({date: day.format('DD'), weekDay: day.format('dd'), fullDate: day.format('YYYY-MM-DD')});
          day = day.clone().add(1, 'd');
      }

      return days;
  }

  public getDaysDiff(day1, day2) {
      return moment.duration(day1.diff(day2)).asDays();
  }

  public getMomentObject(dateTime: any = new Date(), locale = this.appLang) {
      return moment(dateTime).locale(locale);
  }

  public timeDifference(start: any, end: any, locale = this.appLang) {
      console.log(moment(end).locale(locale).from(moment(start), true), 'timeDifference');
      return moment.utc(moment(end).diff(moment(start))).format('HH:mm');
  }

  public isCurrentDay(dateTime: any) {
      return dateTime.format('YYYY-MM-DD') === moment().format('YYYY-MM-DD');
  }

  public isDayOfSelectedMonth(day: any, month: any = new Date(), locale = this.appLang): boolean {
      let formatedDay = day.locale(locale).format('YYYY-MM');
      let formatedMonth = moment(month).locale(locale).format('YYYY-MM');
      return formatedDay === formatedMonth;
  }

  public getTimeTo(timestamp: any, to = 300, locale = this.appLang) {
      return moment(new Date((timestamp + to) * 1000)).locale(locale).fromNow();
  }

  public getAge(date: any, locale = this.appLang) {
      if (date) {
          if (moment(date, 'YYYY-MM-DD').isBefore(moment())) {
              let diff = moment(date, 'YYYY-MM-DD').locale(locale).fromNow().split(' ');
              return diff[0] + ' ' + diff[1];
          } else {
              return '';
          }
      } else {
          return '';
      }
  }

  public mailTo(customer: any = null) {
      if (customer && customer['email']) {
          if (customer['email'].replace(/\s/g, '').length > 0) {
              window.location.href = 'mailto:' + customer['email'];
          } else {
              this.showHint(this.lang['hint_add_email_to_client']);
          }
      } else {
          this.showHint(this.lang['hint_add_email_to_client']);
      }
  }

  public oneHoureLess(time) {
      let response = time;
      if (time) {
          if (time.split(':').length > 0) {
              let number = parseInt(time.split(':')[0], null);
              if (number && number > 0) {
                  let less = number - 1;
                  response = (less < 10) ? '0' + less + ':00' : less + ':00';
              }
          }
      }
      return response;
  }

  public roundMinHours(time) {
      let response = time;
      if (time) {
          if (time.split(':').length > 0) {
              let number = parseInt(time.split(':')[0], null);
              let less = number + 1;
              response = (less < 10) ? '0' + less + ':00' : less + ':00';
          }
      }
      return response;
  }

  public getBase64Value(base64: any) {
      let response = base64.split('base64,');
      return response.length > 1 ? response[1] : response[0];
  }

  public formatDateTime(date: any, format: any, locale = this.appLang) {
      return moment(date).locale(locale).format(format);
  }

  public formatTime(time: any, inputFormat: any, outputFormat: any) {
      return moment(time, inputFormat).format(outputFormat);
  }

  public getWeekDay(date: any, locale = this.appLang) {
      return moment(date).locale(locale).format('dddd');
  }

  public getDayId(day: any = new Date()) {
      return moment(day).day();
  }

  public getDaysBeforeAfter(daysBefore: any,) {
    return moment().add(daysBefore, 'days');
  }

  addKeyboardListeners() {
      window.addEventListener('keyboardWillShow', (event) => {
        this.hideShowAuthButtons(true);
      });
      window.addEventListener('keyboardWillHide', (event) => {
        this.hideShowAuthButtons(false);
      });
      window.addEventListener('keyboardDidHide', (event) => {
          // this.hideShowAuthButtons(false);
          console.log('keyboard hidden !!!!');
      });
  }

  hideShowAuthButtons(show) {
      let app_root = document.querySelector('ion-app');
      app_root.classList[show ? 'add' : 'remove']('auth-keyboard-opened');
  }

  public logoutUser() {
      // for (let i = 0; i < localStorage.length; i++) {
      //     localStorage.removeItem(localStorage.key(i));
      // }
      // localStorage.removeItem('token');
      localStorage.clear();
      this.workerForProfile = {};
      this.workerInfo = {};
      this.userInfo = {};
      this.pushPage('/login');
  }

  public resizeTextArea(ev: any = null) {
      let textbox: any;
      if (!ev) {
          textbox = document.querySelector('.form-item textarea');
      } else {
          textbox = ev.target;
      }

      if (textbox) {
          let maxrows = 5;
          let textAreaValue = document.querySelector('.textAreaValue') as HTMLElement;

          let rows = (textAreaValue.offsetWidth === 0) ? 1 : (Math.ceil(textAreaValue.offsetWidth / (textbox.offsetWidth - 5)));
          if (rows > maxrows) {
              textbox.rows = maxrows;
          } else {
              textbox.rows = rows;
          }
      }
  }

  public updateUserStatuses() {
      // this.apiS.get_user_statuses_revert().subscribe();
  }
}
