import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import { Injectable } from '@angular/core';
import {ApiService} from './api-service.service';

@Injectable()
export class PushNotificationsService {

    constructor(
        private push: Push,
        private apiS: ApiService
    ) {}

    public checkPushPermission() {
        console.log('on Push checkPermission');
        // this.push.hasPermission()
        //     .then((res: any) => {
        //         if (res.isEnabled) {
        this.receivePush();
            //         console.log('We have permission to send push notifications ((push))');
            //     } else {
            //         console.log('We do not have permission to send push notifications ((push))');
            //     }
            // });
    }

    public receivePush() {
        // console.log('on receivePush');
        const options: PushOptions = {
            android: {},
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };
        //
        const pushObject: PushObject = this.push.init(options);
        //
        console.log('on receivePush', pushObject);
        //
        //
        pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification ((push))', notification));

        pushObject.on('registration').subscribe((registration: any) => {this.updateFBToken(registration['registrationId']); console.error('on registration ((push))', registration)});

        pushObject.on('error').subscribe(error => console.error('Error with Push plugin ((push))', error));
    }

    updateFBToken(token) {
        this.apiS.addFBToken({
            token
        }).subscribe((res3) => {
            console.log(res3);
            // if (res3.status === true) {
            //   _this.cookie.set('FLABER_FIREBASE_TOKEN', JSON.stringify(res3.text.token)); // TODO time !!!
            // }
            //  TODO res handling !!!
        });
    }
}
