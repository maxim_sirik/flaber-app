import {CommonService} from './common.service';
import {Injectable} from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@Injectable({
    providedIn: 'root'
})

export class LocalNotificationService {

    public socket: any;
    public onGoToDialog = false;
    public joined: any = [];
    public toJoin: any = [];

    constructor(public comS: CommonService,
                private localNotifications: LocalNotifications) {
        // if (!this.socket) {
        //     this.connectToSocket();
        // }
        this.localNotifications.on('click').subscribe(
            data => {
                console.log(data, 'on localNotifications click success');
            }, error => {
                console.log(error, 'on localNotifications click error');
            }

        );
    }

    showNotification(data: any) {
        this.localNotifications.schedule({
            text: data['message']['text']
            // actions: [{id: 'dialog_id' + }]
        });
    }
}
