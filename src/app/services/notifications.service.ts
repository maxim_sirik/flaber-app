import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFireMessaging} from '@angular/fire/messaging';
import {take} from 'rxjs/internal/operators';
// import {CookieService} from 'ngx-cookie-service';
import {ApiService} from './api-service.service';
// import { Firebase } from '@ionic-native/firebase/ngx';

@Injectable({
    providedIn: 'root'
})
export class NotificationsService {

  private currentUser: any;
  // private pushObject: PushObject;

  currentMessage = new BehaviorSubject(null);

  constructor(
    private angularFireDB: AngularFireDatabase,
    private angularFireAuth: AngularFireAuth,
    private angularFireMessaging: AngularFireMessaging,
    private apiS: ApiService,
    // private push: Push
    // private cookie: CookieService
  ) {

    this.currentUser = JSON.parse(localStorage.getItem('FLABER_CURRENT_USER'));
    this.angularFireMessaging.messaging.subscribe(
      (_messaging) => {
          // console.log(_messaging, '_messaging');
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
    );
  }


  /**
   * update token in firebase database
   *
   * @param userId userId as a key
   * @param token2 token2 as a value
   */
  updateToken(userId, token2) {
    const _this = this;

    // we can change this function to request our backend service
    _this.angularFireAuth.authState.pipe(take(1)).subscribe(
      () => {
        _this.apiS.addFBToken({
          token: token2
        }).subscribe((res3) => {
          console.log(res3);
          // if (res3.status === true) {
          //   _this.cookie.set('FLABER_FIREBASE_TOKEN', JSON.stringify(res3.text.token)); // TODO time !!!
          // }
          //  TODO res handling !!!
        });
      });
  }

  /**
   * request permission for notification from firebase cloud messaging
   *
   * @param userId userId
   */
  requestPermission(userId) {
    // if (userId) {
    //   this.angularFireMessaging.requestToken.subscribe(
    //     (token) => {
    //       console.log(token);
    //       this.updateToken(userId, token);
    //     },
    //     (err) => {
    //       console.error('Unable to get permission to notify.', err);
    //     }
    //   );
    // }
  }

  /**
   * hook method when new notification received in foreground
   */
  receiveMessage() {
    // this.angularFireMessaging.messages.subscribe(
    //   (payload) => {
    //     console.log('new message received. ', payload);
    //     this.currentMessage.next(payload);
    //   });

      // const options: PushOptions = {
      //     android: {},
      //     ios: {
      //         alert: 'true',
      //         badge: true,
      //         sound: 'false'
      //     },
      //     windows: {},
      //     browser: {
      //         pushServiceURL: ''
      //         // pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      //     }
      // };
      //
      // this.pushObject = this.push.init(options);
      //
      // this.pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));
      //
      // this.pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));
      //
      // this.pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }

  addUserToFirebase(object: any) {
      // this.angularFireAuth.auth.createUserWithEmailAndPassword(object.email, object.password)
      //     .then((res) => {
      //         if (res && res['user'] && res['user']['uid']) {
      //             this.apiS.setFBUID({firebase_uid: res['user']['uid']}).subscribe(
      //                 success => {
      //                     if (success['status']) {
      //                         localStorage.setItem('f_uid', success['text']['uid']);
      //                     }
      //                 }, error => {
      //                     console.log('addUserToFirebase error', error);
      //                 }
      //             );
      //         }
      //
      //         // this.procRes(res.user);
      //     })
      //     .catch((e) => console.log('createUserWithEmailAndPassword error', e ));
  }



}
