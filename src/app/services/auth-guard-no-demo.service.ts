import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import {CommonService} from './common.service';

@Injectable()
export class AuthGuardNoDemoService implements CanActivate {

    constructor(public comS: CommonService) { }

    canActivate(): boolean {
        return this.comS.isHasToken();
    }

}
