import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProfilePersonalPage } from './profile-personal.page';
import { SharedModule } from '../../../modules/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ProfilePersonalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProfilePersonalPage]
})
export class ProfilePersonalPageModule {}
