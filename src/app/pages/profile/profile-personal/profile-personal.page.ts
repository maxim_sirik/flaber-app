import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../../services/api-service.service';
import {SharedService} from '../../../services/shared.service';

@Component({
  selector: 'app-profile-personal',
  templateUrl: './profile-personal.page.html',
  styleUrls: ['./profile-personal.page.scss'],
})
export class ProfilePersonalPage implements OnInit {

    public personalForm: FormGroup;
    public focusObject = null;
    public focusOnField = false;
    public addressOpened = false;
    public searchData: any;
    public selectedAddress: any;
    public selectedBirthDate: any;
    public datePickerTouched = false;
    public changedUserFields = {};
    public submitedInvalid = false;
    public imageChangedEvent: any = '';
    public photoBase64Raw: any;
    public validateClicked = false;
    public worker: any;
    public photo = {
        base64: '',
        extension: ''
    };
    public personalArray = [
        {
            formName: 'first_name',
            className: 'name-input',
            labelClassName: 'icon-user name',
            type: 'text',
            placeholder: this.comS.lang['label_first_name'],
            hasRestriction: false,
            restriction: 10000
        },
        {
            formName: 'last_name',
            className: 'surname-input',
            labelClassName: 'icon-user surname',
            type: 'text',
            placeholder: this.comS.lang['label_last_name'],
            hasRestriction: false,
            restriction: 10000
        },
        {
            formName: 'patronymic',
            className: 'patronymic-input',
            labelClassName: 'icon-user patronymic',
            type: 'text',
            placeholder: this.comS.lang['label_patronymic'],
            hasRestriction: false,
            restriction: 300
        },
        {
            formName: 'address',
            className: 'adress-input',
            labelClassName: 'icon-location adress',
            type: 'text',
            placeholder: this.comS.lang['label_work_address'],
            hasRestriction: false,
            restriction: 10000
        },
        {
            formName: 'birth_date',
            className: 'date-input',
            labelClassName: 'icon-calendar dob date',
            type: 'date',
            placeholder: this.comS.lang['label_birth_date'],
            hasRestriction: false,
            restriction: 10000
        }
    ];

  constructor(public comS: CommonService,
              public fb: FormBuilder,
              public apiS: ApiService,
              public shared: SharedService) {
      this.personalForm = fb.group({
          'first_name': ['', Validators.compose([Validators.required])],
          'last_name': ['', Validators.compose([Validators.required])],
          'patronymic': ['', Validators.compose([Validators.required])],
          'address': ['', Validators.compose([Validators.required])],
          'birth_date': ['', Validators.compose([Validators.required])],
      });

      this.shared.addressOnSearchSelected.subscribe(
          data => {
              if (data['address']) {
                  for (let field of Object.keys(data)) {
                      this.changedUserFields[field] = data[field];
                  }
                  this.personalForm.controls['address'].setValue(this.changedUserFields['address']);
              }
          }

      );
  }

  ngOnInit() {}

    ionViewWillEnter() {
      if (this.comS.updateProfileWorker()) {
          if (typeof this.comS.userInfo === 'undefined' || this.comS.userInfo) {
              this.comS.userInfo = JSON.parse(localStorage.getItem('user'));
          }

          if (this.comS.userInfo['first_name']) {
              this.personalForm.controls['first_name'].setValue(this.comS.userInfo['first_name']);
          }
          if (this.comS.userInfo['last_name']) {
              this.personalForm.controls['last_name'].setValue(this.comS.userInfo['last_name']);
          }
          if (this.comS.userInfo['second_name']) {
              this.personalForm.controls['patronymic'].setValue(this.comS.userInfo['second_name']);
          }

          this.personalForm.controls['address'].setValue(this.comS.workerForProfile ? this.comS.workerForProfile['location']['address'] : (this.comS.userInfo['address'] ? this.comS.userInfo['address'] : ''));

          if (this.comS.userInfo['birth_date']) {
              this.personalForm.controls['birth_date'].setValue(this.comS.userInfo['birth_date']);
              this.selectedBirthDate = this.comS.userInfo['birth_date'];
          }

          this.worker = localStorage.getItem('worker');

          if (this.worker) {
              this.worker = JSON.parse(this.worker);
          }
      }
    }

  updateData() {
    this.validateClicked = true;
    this.submitedInvalid = !this.personalValid();
    if (this.personalValid(true)) {
      this.apiS.update_worker_profile(this.changedUserFields).subscribe(
      // this.apiS.update_user(this.changedUserFields).subscribe(
          success => {
            if (success['status']) {
              this.comS.pushBack();
            } else {
                this.comS.showHint(this.comS.lang['error_update_profile']);
            }
          }, error => {
              if (error.error) {
                  this.comS.showHint(error.error.text.errors);
              }
          });
    } else {
      this.comS.showHint(this.comS.lang['error_fill_fields']);
    }

  }

    clearFieldValue(field) {
        field.setValue('');
    }

    showSearchModal() {
        this.comS.showSearch({
            text: '',
            show: true,
            searchComponent: 'address',
            category: null
        });
    }

    dateChanged(ev: any) {
        this.selectedBirthDate = this.comS.formatDateTime(this.selectedBirthDate, 'YYYY-MM-DD');
        this.personalForm.controls['birth_date'].setValue(this.selectedBirthDate);
    }

    fieldOnFocus(field: any, ev: any) {
        this.focusObject = field;
        this.focusOnField = true;
        if (field.formName === 'address') {
            this.showSearchModal();
        }
    }

    fieldOutOfFocus(formName) {
        this.focusOnField = false;
        setTimeout(() => {
            if (!this.focusOnField) {
                this.focusObject = null;
            }
        }, 200);

        // if (formName === 'address' && this.personalForm.controls[formName].value) {
        //     this.addressOpened = true;
        //     CommonService.showPicker('.address_select');
        // }
    }

    showComponentPicker(selector: any, formName = null) {
        if (!this.addressOpened) {
            CommonService.showPicker(selector);
        }
        if (formName === 'birth_date') {
            setTimeout(() => {
                this.datePickerTouched = true;
            }, 1000);
        }
    }

    personalValid(fromNext = false) {
        let valid = this.personalForm.valid;
        if (fromNext) {
            if (valid) {
                for (let i = 0; i < Object.keys(this.personalForm.value).length; i++) {
                    let key = Object.keys(this.personalForm.value)[i];
                    this.changedUserFields[(key !== 'patronymic') ? key : 'second_name'] = this.personalForm.controls[key].value;
                }
            }
        }
        return valid;
    }
    photoPicked(ev: any) {
        this.imageChangedEvent = ev;
    }
    cropped(ev: any) {
      if (ev) {
          this.photoBase64Raw = ev.base64;
          this.photo.base64 = this.comS.getBase64Value(ev.base64);
          this.photo.extension = ev.extension;
          this.changedUserFields['photo'] = this.photo;
      } else {
          this.photoBase64Raw = null;
      }
    }

}
