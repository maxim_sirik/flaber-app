import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordsArchivePage } from './records-archive.page';

describe('RecordsArchivePage', () => {
  let component: RecordsArchivePage;
  let fixture: ComponentFixture<RecordsArchivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecordsArchivePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordsArchivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
