import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {ApiService} from '../../../services/api-service.service';

@Component({
  selector: 'app-records-archive',
  templateUrl: './records-archive.page.html',
  styleUrls: ['./records-archive.page.scss'],
})
export class RecordsArchivePage implements OnInit {

  public archivedRecords: any = [];
  public pageId = 1;
  public perPage = 10;
  public recordsFiltered = [];
  public searchRecords: any;
  public isOnLoadRecords: any = false;
  public isLastPage: any = false;
  public currentPage: any = 0;
  constructor(public comS: CommonService,
              private apiS: ApiService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
      if (this.comS.updateWorker()) {
          this.getArchivedRecords(this.currentPage + 1);
          this.addArchiveScrollListener();
      } else {
          this.comS.pushPage('/main');
      }
  }

  getArchivedRecords(pageId: any) {
      this.isOnLoadRecords = true;
      this.apiS.get_archived_records(this.comS.workerInfo['id'], pageId, this.perPage).subscribe(
          success => {
              if (success['status']) {
                  // this.archivedRecords = success['text']['records'];
                  if (success['text']['records']['data']) {
                      this.isLastPage = success['text']['records']['current_page'] === success['text']['records']['last_page'];
                      this.currentPage = success['text']['records']['current_page'];
                      for (let record of success['text']['records']['data']) {
                          this.archivedRecords.push(record);
                      }
                  }
                  this.filterRecords();
              }
              this.isOnLoadRecords = false;
          }, error => {
              this.isOnLoadRecords = false;
              if (error.error) {
                  this.comS.showHint(error.error.text.errors);
              }
          }
      );
  }

  addArchiveScrollListener() {
      let section = document.querySelector('app-records-archive .section-archive');

      if (section) {
          section.addEventListener('scroll', (ev) => {
              if (ev.target['scrollTop'] === section.scrollHeight - window.innerHeight) {
                  console.log('on bottom of page');
                  if (!this.isOnLoadRecords && !this.isLastPage) {
                      this.getArchivedRecords(this.currentPage + 1);
                  }
              }
           // console.log(ev.target['scrollTop'], section.scrollHeight - window.innerHeight, 'on scroll');
          });
      } else {
          setTimeout(() => {
              this.addArchiveScrollListener();
          }, 200);
      }
  }

  filterRecords() {
      this.recordsFiltered = [];
      if (this.searchRecords && this.searchRecords.length > 0) {
          for (let i = 0; i < this.archivedRecords.length; i++) {
              if (this.archivedRecords[i]['customer']['first_name'] && this.archivedRecords[i]['customer']['first_name'].toLowerCase().indexOf(this.searchRecords.toLowerCase()) !== -1) {
                  this.recordsFiltered.push(this.archivedRecords[i]);
              } else if (this.archivedRecords[i]['customer']['last_name'] && this.archivedRecords[i]['customer']['last_name'].toLowerCase().indexOf(this.searchRecords.toLowerCase()) !== -1) {
                  this.recordsFiltered.push(this.archivedRecords[i]);
              }
          }
      } else {
          this.recordsFiltered = this.archivedRecords;
      }
  }

}
