import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProfileMainPage } from './profile-main.page';
import { SharedModule } from '../../../modules/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ProfileMainPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProfileMainPage]
})
export class ProfileMainPageModule {}
