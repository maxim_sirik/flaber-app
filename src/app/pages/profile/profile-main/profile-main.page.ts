import { Component, OnInit } from '@angular/core';
import {SharedService} from '../../../services/shared.service';
import {CommonService} from '../../../services/common.service';
import {ApiService} from '../../../services/api-service.service';
import Inputmask from 'inputmask';

@Component({
  selector: 'app-profile-main',
  templateUrl: './profile-main.page.html',
  styleUrls: ['./profile-main.page.scss'],
})
export class ProfileMainPage implements OnInit {

  public reviews: any;
  public archivedRecords: any;
  public worker: any;
  public Object = Object;
    public im: any;

  constructor(public shared: SharedService,
              public comS: CommonService,
              public apiS: ApiService) {}

  ngOnInit() {
  }

  ionViewWillEnter() {
      this.im = new Inputmask(this.comS.phoneMask);
      if (this.comS.updateWorker()) {
          this.getUsersData();
          this.getWorkerInfo();
          this.getArchivedRecords();
          this.getReviews();
      } else {
          this.comS.pushPage('/main');
      }
  }

  initPhoneMask(value: any = null) {
      // personal-phone
      let selector = document.querySelector('.personal-phone input');
      if (selector) {
          this.im.mask(selector);
          if (value) {
              selector['inputmask'].setValue(value.replace('380', ''));
          }
      }
  }

    getUsersData() {
        this.apiS.user_info().subscribe(
            success => {
                if (success['status']) {
                    localStorage.setItem('user', JSON.stringify(success['text']['user']));
                    this.comS.userInfo = success['text']['user'];
                    this.initPhoneMask(this.comS.userInfo['phone'] ? this.comS.userInfo['phone'] : null);
                }
            }, error => {
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }
    getWorkerInfo() {
        this.apiS.get_my_profile().subscribe(
            success => {
                if (success['status']) {
                    this.comS.workerInfo = success['text']['info'];
                    localStorage.setItem('worker', JSON.stringify(success['text']['info']));

                    this.comS.updateProfileWorker();
                }
            }, error => {
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            });
    }
    getArchivedRecords() {
      this.apiS.get_archived_records(this.comS.workerInfo['id']).subscribe(
          success => {
              if (success['status']) {
                  this.archivedRecords = success['text']['records'];
              }
          }, error => {
              if (error.error) {
                  this.comS.showHint(error.error.text.errors);
              }
          }
      );
    }

    getReviews() {
      this.apiS.get_worker_reviews(this.comS.workerInfo['id']).subscribe(
          success => {
              if (success['status']) {
                  this.reviews = success['text']['reviews'];
              }
          }, error => {
              if (error.error) {
                  this.comS.showHint(error.error.text.errors);
              }
          }
      );
    }

  changeUsersData() {
      this.comS.pushPage('/profile-personal');
  }

  changeUsersDataCallback() {

  }

}
