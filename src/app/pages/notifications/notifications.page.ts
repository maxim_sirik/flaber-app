import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../services/common.service';
import {ApiService} from '../../services/api-service.service';
import SimpleBar from 'simplebar';
import 'simplebar/dist/simplebar.css';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  public classEntity = this;
  public headerConfig: any;
  public notifications: any = [];
  public bar: any;
  public currentPage: any = 1;
  public isLastPage: any = false;
  public isOnRequest: any = false;

  constructor(public comS: CommonService,
              private apiS: ApiService) {
      this.headerConfig = {
          title: this.comS.lang['nav_title_notifications'],
          showBack: false,
          showNotif: false
      };
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
      this.initSimpleBar();
      this.getNotificationsList(1);
      this.scrollLoadListener();

  }

  initSimpleBar() {
      // let elem = document.querySelector('.section-notifications');
      // if (elem) {
      //     this.bar = new SimpleBar(elem);
      //     this.scrollLoadListener();
      // }
  }

  getNotificationsList(page: any) {
      this.isOnRequest = true;
      this.apiS.get_notifications_list(page).subscribe(
          success => {
              if (success['status']) {
                  let tempData = success['text']['notifications']['data'];
                  for (let i = 0; i < tempData.length; i++) {
                      this.notifications.push(tempData[i]);
                  }
                  this.currentPage = success['text']['notifications']['current_page'];
                  this.isLastPage = success['text']['notifications']['last_page'] === success['text']['notifications']['current_page'];
              }
              setTimeout(() => {
                  this.isOnRequest = false;
              }, 300);
          }, error => {
              setTimeout(() => {
                  this.isOnRequest = false;
              }, 300);
              if (error.error) {
                  this.comS.showHint(error.error.text.errors);
              }
          }
      );
  }

    scrollLoadListener() {
      let scrollElem = document.querySelector('app-notifications .section-support');

      if (scrollElem) {
          scrollElem.addEventListener('scroll', (ev) => {
              let scrollElemHeight = parseInt(window.getComputedStyle(scrollElem, null).getPropertyValue('height'), null);
              if (ev.target['scrollTop'] === scrollElem.scrollHeight - scrollElemHeight) {
                  console.log('on scroll bottom');
              }
          });
      } else {
          setTimeout(() => {
              this.scrollLoadListener();
          }, 200);
      }
        // if (this.bar.getScrollElement()) {
        //     this.bar.getScrollElement().addEventListener('scroll', (ev) => {
        //         let scroll = this.bar.getScrollElement().scrollHeight - ev.target['scrollTop'];
        //         if (scroll === window.screen.height - 117) {
        //             if (!this.isLastPage && !this.isOnRequest) {
        //                 this.getNotificationsList(this.currentPage + 1);
        //             }
        //         }
        //     });
        // }
    }

  acceptCallback() {
      console.log('on acceptCallback');
  }

  declineCallback() {
      console.log('on declineCallback');
  }

}
