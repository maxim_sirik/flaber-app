import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {ApiService} from '../../../services/api-service.service';
import {ActivatedRoute, ParamMap} from '@angular/router';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-view-work',
  templateUrl: './view-work.page.html',
  styleUrls: ['./view-work.page.scss'],
})
export class ViewWorkPage implements OnInit {

  public swiperConfig: any;
  public workId: any;
  public work: any;

  constructor(public comS: CommonService,
              public apiS: ApiService,
              private router: ActivatedRoute) { }

  ionViewWillEnter() {
      this.router.params.subscribe(params => {
          if (params['id']) {
              this.workId = params['id'];
              this.initPage();
          } else {
              this.comS.pushBack();
          }
      });

  }

  initPage() {
    this.swiperConfig = {
        speed: 300,
        autoHeight: false,
        slidesPerView: 1,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
        },
    };

    this.getWorkInfo();
  }

  editThisWork() {
      if (this.workId) {
          this.comS.pushPage('/new-work?work=' + this.workId);
      }
  }

  getWorkInfo() {
      this.apiS.show_work(this.workId).subscribe(
          success => {
              if (success['status']) {
                  this.work = success['text']['work'];
              }
          }, error => {
              if (error.error) {
                  this.comS.showHint(error.error.text.errors);
              }
          }
      );
  }

  ngOnInit() {
  }

}
