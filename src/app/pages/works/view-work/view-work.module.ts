import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ViewWorkPage } from './view-work.page';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

const routes: Routes = [
  {
    path: '',
    component: ViewWorkPage
  }
];

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 'auto'
};

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SwiperModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ViewWorkPage],
  providers: [
      {
          provide: SWIPER_CONFIG,
          useValue: DEFAULT_SWIPER_CONFIG
      }
  ]
})
export class ViewWorkPageModule {}
