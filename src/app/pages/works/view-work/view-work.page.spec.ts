import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewWorkPage } from './view-work.page';

describe('ViewWorkPage', () => {
  let component: ViewWorkPage;
  let fixture: ComponentFixture<ViewWorkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewWorkPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewWorkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
