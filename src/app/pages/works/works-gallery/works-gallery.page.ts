import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {ApiService} from '../../../services/api-service.service';
import {SharedService} from '../../../services/shared.service';

@Component({
  selector: 'app-works-gallery',
  templateUrl: './works-gallery.page.html',
  styleUrls: ['./works-gallery.page.scss'],
})
export class WorksGalleryPage implements OnInit {

  public works: any;
  public isWorksChanged = false;

  public isGridView = false;

  constructor(public comS: CommonService,
              public apiS: ApiService,
              public shared: SharedService) {
      this.shared.workChangedHandler.subscribe(
          (data) => {
              if (data['changed']) {
                  this.isWorksChanged = true;
                  this.selectedWorkerInfo();
              }
          }
      );
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.isWorksChanged = false;
    this.initPage();
  }

  initPage() {
    this.selectedWorkerInfo();
  }

  selectedWorkerInfo() {
      if (this.comS.updateWorker()) {
          this.getWorkerWorks();
      } else {
          this.comS.pushPage('/main');
      }
  }

  getWorkerWorks() {
    this.apiS.get_worker_works(this.comS.workerInfo['id']).subscribe(
        success => {
            if (success['status']) {
                if (!this.works || this.isWorksChanged) {
                    this.works = success['text']['works'];
                }
            }
        }, error => {
            if (error.error) {
                this.comS.showHint(error.error.text.errors);
            }
        }
    );
  }

}
