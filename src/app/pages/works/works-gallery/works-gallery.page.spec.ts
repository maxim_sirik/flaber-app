import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorksGalleryPage } from './works-gallery.page';

describe('WorksGalleryPage', () => {
  let component: WorksGalleryPage;
  let fixture: ComponentFixture<WorksGalleryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorksGalleryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorksGalleryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
