import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WorksGalleryPage } from './works-gallery.page';
import { SharedModule } from '../../../modules/shared.module';

const routes: Routes = [
  {
    path: '',
    component: WorksGalleryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WorksGalleryPage]
})
export class WorksGalleryPageModule {}
