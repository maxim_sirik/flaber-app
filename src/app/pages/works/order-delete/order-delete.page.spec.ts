import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDeletePage } from './order-delete.page';

describe('OrderDeletePage', () => {
  let component: OrderDeletePage;
  let fixture: ComponentFixture<OrderDeletePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderDeletePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDeletePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
