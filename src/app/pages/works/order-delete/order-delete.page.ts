import {Component, NgZone, OnInit, ViewChild} from '@angular/core';
import {CommonService} from '../../../services/common.service';
import { IonReorderGroup } from '@ionic/angular';
import {ApiService} from '../../../services/api-service.service';

@Component({
  selector: 'app-order-delete',
  templateUrl: './order-delete.page.html',
  styleUrls: ['./order-delete.page.scss'],
})
export class OrderDeletePage implements OnInit {
  @ViewChild(IonReorderGroup) reorderGroup: IonReorderGroup;

  public albums = {
      worker: [],
      workerWorksFormated: [],
      customer: []
  };

  public changedOrderArray = [];
  public currentDragged = 0;
  public isDragging = false;
  public updateAllowed = true;
  constructor(public comS: CommonService,
              public apiS: ApiService,
              private zone: NgZone) { }

  ngOnInit() {}

  ionViewWillEnter() {
      this.initPage();
  }

  initPage() {
      this.selectedWorkerInfo();
  }

  selectedWorkerInfo() {
      if (this.comS.updateWorker()) {
          this.getWorkerWorks();
      } else {
          this.comS.pushPage('/main');
      }
  }

  getWorkerWorks() {
      this.apiS.get_worker_works(this.comS.workerInfo['id']).subscribe(
          success => {
              if (success['status']) {
                  this.albums.worker = success['text']['works']['data'];
                  if (this.albums.worker && this.albums.worker.length > 0) {
                      for (let i = 0; i < this.albums.worker.length; i++) {
                          if (this.albums.worker[i]['sort'] === 500) {
                              this.initWorkOrder(this.albums.worker[i], i + 1);
                              this.changedOrderArray.push(i);
                          }
                      }
                  }
              }
          }, error => {
              if (error.error) {
                  this.comS.showHint(error.error.text.errors);
              }
          }
      );
  }

  initWorkOrder(work: any, index: any) {
      work['sort'] = index;
  }

  onDrag(ev: any, index: any) {
    this.isDragging = true;
    this.currentDragged = index;
  }

  touchend(ev: any, work: any) {
      if (ev.target.offsetParent.parentNode.getBoundingClientRect().top < 40) {
          let elemId = this.albums.worker.indexOf(work);
          if (elemId !== -1) {
              this.deleteWork(work);
          }
      }
      this.isDragging = false;
  }

  doReorder(ev: any) {
      ev.detail.complete();
      this.reorderWorks();
  }

  reorderWorks() {
      let wrappers = document.querySelectorAll('.list-wrapper');

      for (let i = 0; i < wrappers.length; i++) {
          let work_id = parseInt(wrappers[i].getAttribute('data-work-id'), null);
          for (let j = 0; j < this.albums.worker.length; j++) {
              if (this.albums.worker[j]['id'] === work_id) {
                  if (this.albums.worker[j]['sort'] !== i + 1) {
                      this.initWorkOrder(this.albums.worker[j], i + 1);
                      if (!this.changedOrderArray.includes(j)) {
                          this.changedOrderArray.push(j);
                      }
                  }
              }
          }
      }
  }

  delayUpdate() {
      setTimeout(() => {
          this.updateAllowed = true;
      }, 5000);
  }

  getChangedSort() {
    let works = [];
    for (let i = 0; i < this.changedOrderArray.length; i++) {
        works.push({
           'work_id': this.albums.worker[this.changedOrderArray[i]]['id'],
           'sort': this.albums.worker[this.changedOrderArray[i]]['sort']
        });
    }
    return works;
  }

  completeReorder() {
      if (this.updateAllowed) {
          let updatedWorks = this.getChangedSort();

          if (updatedWorks.length > 0) {
              let obj = {
                  works: updatedWorks
              };

              this.apiS.bulk_change_work_order(obj).subscribe(
                  success => {
                      if (success['status']) {
                          this.comS.showHint(this.comS.lang['hint_updated_successfully']);
                          this.comS.pushBack();
                      }
                      this.delayUpdate();
                  }, error => {
                      if (error.error) {
                          this.comS.showHint(error.error.text.errors);
                      }
                      this.delayUpdate();
                  }
              );
          } else {
              this.comS.showHint(this.comS.lang['error_nothing_to_update']);
              this.comS.pushBack();
          }
      } else {
          this.comS.showHint( this.comS.lang['error_update_delay']);
      }
  }

  deleteWork(work: any) {
      if (work) {
          this.apiS.delete_work(work['id']).subscribe(
              success => {
                  this.comS.showHint(this.comS.lang[success['status'] ? 'hint_work_deleted' : 'error_unable_delete_work']);
                  this.getWorkerWorks();
              }, error => {
                  if (error.error) {
                      this.comS.showHint(error.error.text.errors);
                      this.getWorkerWorks();
                  }
              }
          );
      }
  }

  toggleReorderGroup() {
      this.reorderGroup.disabled = !this.reorderGroup.disabled;
  }
}
