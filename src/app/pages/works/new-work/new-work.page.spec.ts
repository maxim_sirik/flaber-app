import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewWorkPage } from './new-work.page';

describe('NewWorkPage', () => {
  let component: NewWorkPage;
  let fixture: ComponentFixture<NewWorkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewWorkPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewWorkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
