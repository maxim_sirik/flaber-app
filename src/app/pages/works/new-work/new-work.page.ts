import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {CommonService} from '../../../services/common.service';
import {ApiService} from '../../../services/api-service.service';
import {SharedService} from '../../../services/shared.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-new-work',
  templateUrl: './new-work.page.html',
  styleUrls: ['./new-work.page.scss'],
})
export class NewWorkPage implements OnInit {

    public imageChangedEvent: any;
    public selectedService: any;
    public selectedCustomer: any;
    public focusObject = null;
    public focusOnField = false;
    public workName: any;
    public work: any;
    public editWork = false;
    public workId: any;
    public description: any;
    public nameRestriction = 50;
    public descriptionRestriction = 300;
    public validateClicked = false;
    public thumbs = [];

    constructor(public comS: CommonService,
                public apiS: ApiService,
                public shared: SharedService,
                private location: Location,
                private router: ActivatedRoute) {

        this.shared.serviceNameOnSearchSelected.subscribe(
            data => {
                if (data['name']) {
                    this.selectedService = data;
                } else {
                    this.selectedService = null;
                }
            }
        );

        this.shared.clientOnSearchSelected.subscribe(
            data => {
                if (data['id']) {
                    this.selectedCustomer = data;
                }
            }
        );


    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.router.queryParams.subscribe(params => {
            this.editWork = typeof params['work'] !== 'undefined';
            if (this.editWork) {
                this.workId = params['work'];
                this.getWorkInfo();
            }
        });
    }

    getWorkInfo() {
        this.apiS.show_work(this.workId).subscribe(
            success => {
                if (success['status']) {
                    this.work = success['text']['work'];
                    this.fillWorkData(this.work);
                }
            }, error => {
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }

    addWork() {
        this.validateClicked = true;
        if (this.selectedService && this.workName && this.description) {
            if (this.editWork) {
                this.editWorkHandler();
            } else {
                let work = {
                    'service_id': this.selectedService ? this.selectedService['id'] : null,
                    'title': this.workName,
                    'description': this.description,
                    'photos': this.thumbs,
                    'customer_id': this.selectedCustomer ? this.selectedCustomer['id'] : null
                };

                this.apiS.add_master_work(this.comS.workerInfo['id'], work).subscribe(
                    success => {
                        if (success['status']) {
                            this.comS.pushBack();
                            this.shared.workChanged.next({changed: true});
                        }
                    }, error => {
                        if (error.error) {
                            this.comS.showHint(error.error.text.errors);
                        }
                    }
                );
            }
        }
    }

    editWorkHandler() {
        this.apiS.update_work(this.workId, {
            'service_id': this.selectedService ? this.selectedService['id'] : null,
            'title': this.workName,
            'description': this.description,
            'photos': this.thumbs,
            'customer_id': this.selectedCustomer ? this.selectedCustomer['id'] : null
        }).subscribe(
            success => {
                if (success['status']) {
                    setTimeout(() => {
                        this.comS.pushBack();
                        this.shared.workChanged.next({changed: true});
                    }, 400);
                }
            }, error => {
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }

    removePhoto(photo: any) {
        if (this.editWork) {
            if (photo['flag'] === 'existing') {
                photo['flag'] = 'delete';
            } else {
                this.thumbs = this.comS.removeFromArray(this.thumbs, photo);
            }
        } else {
            this.thumbs = this.comS.removeFromArray(this.thumbs, photo);
        }
    }

    showComponentPicker(selector: any) {
        CommonService.showPicker(selector);
    }

    showSearch(component: any) {
        this.comS.showSearch({
            text: null,
            show: true,
            searchComponent: component,
            category: null,
            isMasters: true
        });
    }

    fillWorkData(work: any) {
        this.thumbs = [];
        work['photos'].forEach((photo) => {
            this.thumbs.push({
                base64: null,
                id: photo['id'],
                extension: 'png',
                unformatedBase64: photo['path'],
                flag: 'existing'
            });
        });

        this.workName = work['title'];
        this.description = work['description'];
        this.selectedService = work['service'];
        this.selectedCustomer = work['customer'];

        if (this.description) {
            setTimeout(() => {
                this.comS.resizeTextArea();
            }, 300);
        }
    }

    photoPicked(ev: any) {
        this.imageChangedEvent = ev;
    }

    pickCropped(ev: any) {
        let tmp = ev;
        if (this.editWork) {
            this.thumbs.push({
                base64: this.comS.getBase64Value(tmp['base64']),
                id: null,
                extension: tmp['extension'],
                unformatedBase64: tmp['base64'],
                flag: 'new'
            });
        } else {
            this.thumbs.push({base64: this.comS.getBase64Value(tmp['base64']), extension: tmp['extension'], unformatedBase64: tmp['base64']});
        }

    }

    fieldOnFocus(field: any, ev: any) {
        this.focusObject = field;
        this.focusOnField = true;
    }

    fieldOutOfFocus() {
        this.focusOnField = false;
        setTimeout(() => {
            if (!this.focusOnField) {
                this.focusObject = null;
            }
        }, 200);
    }

    clearFieldValue(field: any) {
        this[field] = '';
    }

}
