import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NewWorkPage } from './new-work.page';
import {SharedModule} from '../../../modules/shared.module';

const routes: Routes = [
  {
    path: '',
    component: NewWorkPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NewWorkPage]
})
export class NewWorkPageModule {}
