import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {ApiService} from '../../../services/api-service.service';
import {SharedService} from '../../../services/shared.service';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-my-services-main',
  templateUrl: './my-services-main.page.html',
  styleUrls: ['./my-services-main.page.scss'],
})
export class MyServicesMainPage implements OnInit {

  public servicesSections: any = [];
  public currentShownNewService: any = null;
  public shownServiceList: any = null;
  public serviceDuration: any;
  public editedProvider: any;

  constructor(public comS: CommonService,
              private apiS: ApiService,
              private shared: SharedService,
              public alertController: AlertController) {
      let temp_d = new Date();
      temp_d.setHours(0, 5, 0, 0);
      this.serviceDuration = temp_d.toISOString();
      this.serviceAddedFromPage();

      this.shared.alertCallbackMethodFired.subscribe(
          methodObj => {
              if (methodObj['methodName']) {
                  if (typeof this[methodObj['methodName']] !== 'undefined') {
                      this[methodObj['methodName']]();
                  }
              }
          }
      );
  }

  ionViewWillEnter() {
    this.initPage();
      // this.deleteProvider();
      // this.addProvider();
  }

  initPage() {
      if (this.comS.updateWorker()) {
          this.getServices();
      } else {
          this.comS.pushPage('/main');
      }

  }

  getServices() {
      this.apiS.get_services(this.comS.workerInfo['id']).subscribe(
          success => {
            if (success['status']) {
              this.servicesSections = success['text']['sections'];
              this.filterServices();
            }
          }, error => {
              if (error['error']) {
                  this.comS.showHint(error['error']['text']['errors']);
              }
          }
      );
  }

  filterServices() {
      for (let section of this.servicesSections) {
          let formatted = {
              approved: [],
              onModeration: []
          };
          section['onModeration'] = (this.comS.userStatuses[section['status_id']] === 'service_sections_on_moderation');
          for (let service of section['services']) {
              if (this.comS.userStatuses[service['status_id']] === 'services_on_moderation') {
                  formatted.onModeration.push(service);
              } else {
                  formatted.approved.push(service);
              }
          }
          formatted.approved.sort(this.comS.dynamicSort('name'));
          formatted.onModeration.sort(this.comS.dynamicSort('name'));
          section['formatted'] = formatted;
      }
  }

  openItem(slideItem: any, open: boolean) {
      slideItem[open ? 'open' : 'close']();
  }

  showNewService(section: any, service: any = null, ev: any = null) {
      if (ev && ev.target && ev.target.classList.contains('icon-minus')) {
          this.currentShownNewService = null;
          this.shownServiceList = null;
      } else {
          this.currentShownNewService = section;
          this.shownServiceList = null;
          this.editedProvider = service;
          if (service) {
              let time = service['time'].split(':');
              if (time.length > 1) {
                  this.setPickerTime(time[0], time[1]);
              }
          } else {
              this.setPickerTime();
          }
      }
  }

  serviceAdded(ev: any) {
    if (!ev) {
        this.currentShownNewService = ev;
    } else {
        if (this.currentShownNewService) {
            if (this.servicesSections.length > 0) {
                if (ev['isUpdated']) {
                    this.updateServiceData(ev['service']);
                } else {
                    this.currentShownNewService['services'].push(ev['service']);
                    if (ev['isCustom']) {
                        this.showNewServiceAlert();
                    }
                }
            } else {
                this.getServices();
            }
            this.currentShownNewService = null;
        }
    }
  }

  updateServiceData(service: any) {
      for (let i = 0; i < this.currentShownNewService['services'].length; i++) {
          // let item = this.currentShownNewService['services'][i];
          if (this.currentShownNewService['services'][i]['id'] === service['id']) {
              this.currentShownNewService['services'][i] = service;
          }
      }
      // this.currentShownNewService['services'].forEach((item) => {
      //     if (item['id'] === service['id']) {
      //         item = service;
      //     }
      // });
  }

  showServices(section: any) {
      this.shownServiceList = this.shownServiceList === section ? null : section;
      this.currentShownNewService = null;
  }

  serviceAddedFromPage() {
      this.shared.serviceObjectAdded.subscribe(
          service => {
              if (service['name']) {
                  if (service['service_id']) {
                      this.addProvider(service);
                  } else {
                      this.addCustomService(service);
                  }
              }
          });
    }

    addProvider(service: any) {
        let provider = {
            'section_id': service['section_id'],
            'service_id': service['service_id'],
            'worker_id': this.comS.workerInfo['id'],
            'time': service['time'],
            'price':  service['price'],
            'description':  service['description'],
            'can_record_online': service['can_record_online'] ? 1 : 0
        };

        this.apiS.add_provider(provider).subscribe(
            success => {
                if (success['status']) {
                    this.serviceAdded({service: success['text']['service_provider'], isCustom: false});
                }
            }, error => {
                if (error['error']) {
                    this.comS.showHint(error['error']['text']['errors']);
                }
            }
        );
    }

    addCustomService(service: any) {
        let provider = {
            'section_id': service['section_id'],
            'name': service['name'],
            'worker_id': this.comS.workerInfo['id'],
            'time': service['time'],
            'price':  service['price'],
            'description':  service['description']
        };

        this.apiS.add_new_service(provider).subscribe(
            success => {
                if (success['status']) {
                    this.serviceAdded({service: success['text']['service'], isCustom: true});
                }
            }, error => {
                if (error['error']) {
                    this.comS.showHint(error['error']['text']['errors']);
                }
            }
        );
    }

    hideAlert() {
        this.comS.showAlert({show: false});
    }

    showNewServiceAlert() {
        let object = {
            title: 'Добавления услуги',
            text: {
                message: 'Вы добавили новую услугу, но она должна сперва пройти модерацию. Как только она будет проверена вы получите уведомление. Также, услуга будет отображена в списке услуг, но не будет активна'
            },
            image: 'popup-modal__subheading icon-checklist',
            buttons: {
                array: [
                    {
                        title: 'Ок',
                        callback: { method: 'hideAlert' }
                    }
                ],
                containerClassName: ''
            },
            show: true
        };
        this.comS.showAlert(object);
    }

    editService(service: any, section: any, ev: any) {
      ev.preventDefault();
      ev.stopPropagation();
        let time = service['time'].split(':');
        if (time.length > 1) {
            this.setPickerTime(time[0], time[1]);
        }
        this.showNewService(section, service);
    }

    setPickerTime(hour: any = null, minute: any = null) {
        let temp_d = new Date();
        temp_d.setHours(hour ? hour : 0, minute ? minute : 5, 0, 0);
        this.serviceDuration = temp_d.toISOString();
    }

    deleteService(service: any, section: any, ev: any) {
        ev.preventDefault();
        ev.stopPropagation();
        this.apiS.delete_provider(service['provider_id']).subscribe(
            success => {
                if (success['status']) {
                    section.services = this.comS.removeFromArray(section.services, service);
                    if (section.services.length === 0) {
                        this.servicesSections = this.comS.removeFromArray(this.servicesSections, section);
                    }
                }
            }, error => {
                if (error['error']) {
                    this.comS.showHint(error['error']['text']['errors']);
                }
            }
        );
    }

    serviceDurationChanged(ev: any) {
        let temp = new Date(this.serviceDuration);
        this.serviceDuration = ((temp.getMinutes() === 0 && temp.getHours() === 0) ? null : this.serviceDuration);
    }

    initPicker(ev: any) {
        console.log(ev, 'initPicker');
        this.serviceDuration = ev;
    }

  ngOnInit() {
  }

    async presentAlert(name) {
        const alert = await this.alertController.create({
            // header: name,
            // subHeader: name,
            message: name,
            buttons: [this.comS.lang['button_close']]
        });

        await alert.present();
    }

}
