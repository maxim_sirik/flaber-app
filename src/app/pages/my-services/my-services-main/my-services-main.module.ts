import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import {SharedModule} from '../../../modules/shared.module';

import { IonicModule } from '@ionic/angular';

import { MyServicesMainPage } from './my-services-main.page';

const routes: Routes = [
  {
    path: '',
    component: MyServicesMainPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MyServicesMainPage]
})
export class MyServicesMainPageModule {}
