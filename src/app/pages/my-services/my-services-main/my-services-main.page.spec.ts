import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyServicesMainPage } from './my-services-main.page';

describe('MyServicesMainPage', () => {
  let component: MyServicesMainPage;
  let fixture: ComponentFixture<MyServicesMainPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyServicesMainPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyServicesMainPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
