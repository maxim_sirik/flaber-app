import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {ApiService} from '../../../services/api-service.service';

@Component({
  selector: 'app-settings-main',
  templateUrl: './settings-main.page.html',
  styleUrls: ['./settings-main.page.scss'],
})
export class SettingsMainPage implements OnInit {

  public locationField = false;
  public pushNotifications = false;

  constructor(public comS: CommonService,
              private apiS: ApiService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.locationField = this.comS.userInfo['detect_location'] !== null ? this.comS.userInfo['detect_location'] : false;
    this.pushNotifications = this.comS.userInfo['notify'] !== null ? this.comS.userInfo['notify'] : false;
  }

  locationStateChange() {
    // нужно проверять разрешение на использование местоположения
    let object = {
        detect_location: this.locationField ? 1 : 0
    };
    this.updateSettings(object, this.locationField);
  }

  pushStateChange() {
      // нужно проверять разрешение на отправку нотификаций
      let object = {
          notify: this.pushNotifications ? 1 : 0
      };
      this.updateSettings(object, this.pushNotifications);
  }

  changeFailedCallback(field: any) {
      field = !field;
  }

  updateSettings(object, field) {
      this.apiS.update_basic_settings(object).subscribe(
          success => {
              if (!success['status']) {
                  this.changeFailedCallback(field);
              } else {
                  localStorage.setItem('user', JSON.stringify(success['text']['user']));
                  this.comS.userInfo = success['text']['user'];
              }
          }, error => {
              console.log(error);
          }
      );
  }

}
