import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsMainPage } from './settings-main.page';

describe('SettingsMainPage', () => {
  let component: SettingsMainPage;
  let fixture: ComponentFixture<SettingsMainPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsMainPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsMainPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
