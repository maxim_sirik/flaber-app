import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../../services/api-service.service';

@Component({
  selector: 'app-settings-password-change',
  templateUrl: './settings-password-change.page.html',
  styleUrls: ['./settings-password-change.page.scss'],
})
export class SettingsPasswordChangePage implements OnInit {

  public passwordForm: FormGroup;
  public focusObject = null;
  public focusOnField = false;
  public passFields = [
      {
          formName: 'current_password',
          placeholder: this.comS.lang['label_current_password'],
          type: 'password'
      },
      {
          formName: 'password',
          placeholder: this.comS.lang['label_new_password'],
          type: 'password'
      },
      {
          formName: 'password_confirmation',
          placeholder: this.comS.lang['label_password_confirmation'],
          type: 'password'
      }
  ];

  constructor(public comS: CommonService,
              public fb: FormBuilder,
              public apiS: ApiService) {
      this.passwordForm = fb.group({
          'current_password': ['', Validators.required],
          'password': ['', Validators.compose([Validators.required, Validators.minLength(6)])],
          'password_confirmation': ['', Validators.required],
      }, {validator: this.checkPasswords});
  }

  ngOnInit() {
  }

  checkPasswords(group: FormGroup) {
      const pass = group.controls.password.value;
      const confirmPass = group.controls.password_confirmation.value;
      return pass === confirmPass ? null : { notSame: true };
  }

  changePassword() {
      if (this.passwordForm.valid) {
          let object = {
              'old_password': this.passwordForm.controls['current_password'].value,
              'new_password': this.passwordForm.controls['password'].value,
              'new_password1': this.passwordForm.controls['password_confirmation'].value
          };

          this.apiS.update_settings_password(object).subscribe(
              success => {
                  if (success['status']) {
                      this.comS.pushBack();
                  }
              }, error => {
                  if (error.error && error.error.text) {
                      this.comS.showHint(error.error.text.errors);
                  }
              }
          );
      } else {
          this.comS.showHint(this.comS.lang['error_fill_fields']);
      }
  }
  clearFieldValue(field: any) {
      field.setValue('');
  }

    fieldOnFocus(field: any, ev: any) {
        this.focusObject = field;
        this.focusOnField = true;
    }

    fieldOutOfFocus(formName) {
        this.focusOnField = false;
        setTimeout(() => {
            if (!this.focusOnField) {
                this.focusObject = null;
            }
        }, 200);
    }
}
