import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SettingsPasswordChangePage } from './settings-password-change.page';

const routes: Routes = [
  {
    path: '',
    component: SettingsPasswordChangePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SettingsPasswordChangePage]
})
export class SettingsPasswordChangePageModule {}
