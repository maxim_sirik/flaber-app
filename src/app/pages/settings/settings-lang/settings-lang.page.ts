import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {ApiService} from '../../../services/api-service.service';

@Component({
  selector: 'app-settings-lang',
  templateUrl: './settings-lang.page.html',
  styleUrls: ['./settings-lang.page.scss'],
})
export class SettingsLangPage implements OnInit {

  public languages: any;

  constructor(public comS: CommonService,
              private apiS: ApiService) { }

  ionViewWillEnter() {
    this.getLanguages();
  }

  ngOnInit() {
  }

  updateLanguage() {
      let element = document.querySelector('input[name="selectedLanguage"]:checked') as HTMLElement;
      if (element) {
        this.apiS.update_basic_settings({lang_id: parseInt(element['value'], null)}).subscribe(
            success => {
                if (!success['status']) {
                    // показать ошибку смены языка
                } else {
                    localStorage.setItem('user', JSON.stringify(success['text']['user']));
                    this.comS.setLang(success['text']['user']['language']['locale']);
                    this.comS.userInfo = success['text']['user'];
                }
                this.comS.pushBack();
            }, error => {
                console.log(error);
            }
        );
      } else {
          this.comS.showHint(this.comS.lang['error_empty_language']);
      }
  }

  getLanguages() {
      this.apiS.get_langs().subscribe(
          success => {
              if (success['status']) {
                  this.languages = success['text']['langs'];
              }
          }, error => {
              if (error.error && error.error.text) {
                  this.comS.showHint(error.error.text.errors);
              }
          }
      );
  }

}
