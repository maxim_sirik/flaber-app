import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ApiService} from '../../services/api-service.service';
import {CommonService} from '../../services/common.service';
import {SharedService} from '../../services/shared.service';
import Inputmask from 'inputmask';

@Component({
  selector: 'app-record-info',
  templateUrl: './record-info.page.html',
  styleUrls: ['./record-info.page.scss'],
})
export class RecordInfoPage implements OnInit {

    public im: any;
    public record: any;
    public recordId: any;
    public dayNotify: any;
    public hourNotify: any;
    public note: any;
    public focusObject: any;
    public focusOnField = false;
    public isNewRecord = false;
    public Object = Object;
    public freeTime: any;
    public isActiveRecord = false;
    public serviceProviders: any = [];
    public selectedProviders: any = [];
    public masterServices: any;
    public addNewServicesOpen = false;
    public headerBlur = false;

    constructor(private router: ActivatedRoute,
                private apiS: ApiService,
                public comS: CommonService,
                private shared: SharedService) {
        this.im = new Inputmask(this.comS.phoneMask);
        this.shared.alertCallbackMethodFired.subscribe(
            methodObj => {
                if (methodObj['methodName']) {
                    if (typeof this[methodObj['methodName']] !== 'undefined') {
                        this[methodObj['methodName']]();
                    }
                }
            }
        );

        this.shared.showNewRecord.subscribe(
            data => {
                this.headerBlur = typeof data['show'] !== 'undefined' ? data['show'] : false;
            }
        );

        this.shared.recordMoveCallback.subscribe(
            data => {
                if (data && data.time) {
                    this.updateRecordTime(data);
                }
            }
        );
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.router.params.subscribe(params => {
          if (params['id'] && params['id'] !== 'undefined') {
              this.recordId = params['id'];
              this.getRecordInfo(this.recordId);
              this.getMasterServices();
          } else {
            this.comS.pushPage('/main/schedule');
          }
        });
    }

    ionViewDidLeave() {
        this.shared.alertCallbackMethod.next({});
    }

    moveRecord() {
        this.shared.newRecord.next(
            {
                show: true,
                moveRecord: true,
                time: this.record['start_time'],
                finish_time: this.record['finish_time'],
                finish_date: this.record['finish_date'],
                date: this.record['date']
            });
    }

    initPhoneMask() {
        let phone = document.querySelector('app-record-info [data-phone]');
        if (phone) {
            this.im.mask(phone);
            phone['inputmask'].setValue(phone.getAttribute('data-phone').replace('380', ''));
        }

    }

    updateRecordTime(data: any) {
        if (this.recordId && typeof this.recordId !== 'undefined') {
            this.apiS.edit_record(this.recordId, data).subscribe(
                success => {
                    if (success['status']) {
                        this.getRecordInfo(this.recordId);
                    }
                }, error => {
                    if (error.error) {
                        if (error.error.text.errors['existing_record_id']) {
                            this.comS.showHint(error.error.text.errors['message'], {text: 'Нажмите, чтобы просмотреть информацию о записи', url: '/record-info/' + error.error.text.errors['existing_record_id']});
                        } else {
                            this.comS.showHint(error.error['text']['errors']);
                        }
                    }
                }
            );
        }
    }

    getRecordInfo(id: any) {
      this.apiS.get_record_by_id(id).subscribe(
          success => {
            if (success['status']) {
              this.record = success['text']['record'];
              this.dayNotify = this.record['day_notify'];
              this.hourNotify = this.record['hour_notify'];

              this.getFreeTime();

              this.serviceProviders = [];
              this.addNewServicesOpen = false;
              if (this.record && this.record.services) {
                  for (let service of this.record.services) {
                      if (this.serviceProviders.indexOf(service.id) === -1) {
                          this.serviceProviders.push(service.id);
                      }
                  }
              }

              if (!this.comS.userStatuses) {
                  this.apiS.updateUserStatuses();
              }

              setTimeout(() => {
                  this.initPhoneMask();
              }, 200);

              if (this.comS.userStatuses[this.record['status_id']] === 'records_new') {
                  this.isNewRecord = true;
              }
              this.isActiveRecord = this.comS.userStatuses[this.record['status_id']] === 'records_new' || this.comS.userStatuses[this.record['status_id']] === 'records_approved';
            }
          }, error => {
            if (error.error) {
              this.comS.showHint(error.error.text.errors);
            }
          }
      );
    }

    changeNotify(param: any) {
        let object = {};
        object[param + '_notify'] = this[param + 'Notify'] ? 1 : 0;
        this.updateSettings(object);
    }

    addNote() {
        if (this.note) {
            let object = {
                notes: this.note
            };
            this.updateSettings(object);
        } else {
            this.comS.showHint(this.comS.lang['error_empty_note']);
        }
    }

    updateSettings(object: any) {
        this.apiS.change_record_settings(this.recordId, object).subscribe(
            success => {
                if (success['status']) {
                    this.record = success['text']['record'];
                    if (this.note) {
                        this.resetTextAreaRows();
                        this.note = '';
                    }
                }
            }, error => {
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }

    fieldOnFocus(field: any) {
        this.focusObject = field;
        this.focusOnField = true;
    }

    fieldOutOfFocus() {
        this.focusOnField = false;
        setTimeout(() => {
            if (!this.focusOnField) {
                this.focusObject = null;
            }
        }, 200);
    }

    clearFieldValue(field: any) {
        this.note = '';
        this.resetTextAreaRows();
    }

    resetTextAreaRows() {
        let textbox = document.querySelector('.form-item textarea') as HTMLElement;
        if (textbox) {
            textbox['rows'] = 1;
        }
    }

    setRecordStatus(status: any) {
        this.hideAlert();
        let statusId = null;
        let keys = Object.keys(this.comS.userStatuses);

        for ( let i = 0; i < keys.length; i++) {
            if (status === this.comS.userStatuses[keys[i]]) {
                statusId = keys[i];
            }
        }

        if (statusId) {
            this.apiS.edit_record(this.recordId, {'status_id': statusId}).subscribe(
                success => {
                    if (success['status']) {
                        if (status === 'records_cancelled') {
                            this.comS.showHint(this.comS.lang['hint_record_canceled']);
                            this.comS.pushBack();
                        } else if (status === 'records_approved') {
                            this.comS.showHint(this.comS.lang['hint_record_approved']);
                            this.record = success['text']['record'];
                            this.dayNotify = this.record['day_notify'];
                            this.hourNotify = this.record['hour_notify'];

                            if (!this.comS.userStatuses) {
                                this.apiS.updateUserStatuses();
                            }

                            if (this.comS.userStatuses[this.record['status_id']] === 'records_new') {
                                this.isNewRecord = true;
                            }
                            this.isActiveRecord = this.comS.userStatuses[this.record['status_id']] === 'records_new' || this.comS.userStatuses[this.record['status_id']] === 'records_approved';
                            //
                            // if (this.comS.userStatuses && this.record) {
                            //     if (this.comS.userStatuses[this.record['status_id']] === 'records_new') {
                            //         this.isNewRecord = true;
                            //     }
                            // }
                        }
                    }
                }, error => {
                    if (error.error) {
                        this.comS.showHint(error.error.text.errors);
                    }
                }
            );
        } else {
            this.comS.showHint(this.comS.lang['error_incorrect_record_status']);
        }
    }

    openAlert() {
        let object = {
            title: this.comS.lang['label_warning'],
            text: {
                top: this.comS.lang['label_remove_record'],
                message: this.comS.lang['label_remove_record_warning']
            },
            buttons: {
                array: [
                    {
                        title: this.comS.lang['button_cancel'],
                        callback: { method: 'cancelRecord' }
                    },
                    {
                        title: this.comS.lang['button_leave'],
                        callback: { method: 'hideAlert' }
                    }
                ],
                containerClassName: ''
            },
            show: true
        };

        this.comS.showAlert(object);
    }

    cancelRecord() {
        this.setRecordStatus('records_cancelled');
    }

    approveRecord() {
        this.setRecordStatus('records_approved');
    }

    hideAlert() {
        this.comS.showAlert({show: false});
    }

    getFreeTime() {
        this.freeTime = null;
        let next_record_time_array = ['24', '00'];
        if (this.record.next_record_time) {
            next_record_time_array = this.record.next_record_time.split(':');
        }
        let finish_time_array = this.record.finish_time.split(':');
        if (next_record_time_array.length > 0 && finish_time_array.length > 0) {

            let next_timestamp_temp = new Date();
            next_timestamp_temp.setHours(parseInt(next_record_time_array[0], null), parseInt(next_record_time_array[1], null), 0 , 0);

            let finish_timestamp_temp = new Date();
            finish_timestamp_temp.setHours(parseInt(finish_time_array[0], null), parseInt(finish_time_array[1], null), 0 , 0);
            let diff = this.comS.timeDifference(finish_timestamp_temp.toISOString(), next_timestamp_temp.toISOString());

            if (diff) {
                let diff_array = diff.split(':');
                this.freeTime = {
                    hours: parseInt(diff_array[0], null),
                    minutes: parseInt(diff_array[1], null)
                };
            }

        }
    }

    getMasterServices() {
        this.masterServices = [];
        if (this.comS.updateWorker()) {
            this.apiS.get_services(this.comS.workerInfo['id']).subscribe(
                success => {
                    if (success['status']) {
                        this.masterServices = success['text']['sections'];
                    }
                }, error => {
                    console.log(error);
                }
            );
        }
    }

    showAddServiceTimeModal() {
        this.getSelectedServices();
        let object = {
            title: 'Добавления услуги',
            text: {
                message: 'Хотите ли вы, чтобы длительность записи изменилась в соответствии с длительностью добавленных Вами услуг?'
            },
            image: 'popup-modal__subheading icon-checklist',
            buttons: {
                array: [
                    {
                        title: 'Изменить',
                        callback: { method: 'changeRecordTime' }
                    },
                    {
                        title: 'Не изменять',
                        callback: { method: 'updateRecordServices' }
                    }
                ],
                containerClassName: ''
            },
            show: true
        };
        this.comS.showAlert(object);
    }

    getSelectedServices() {
        let elements = document.querySelectorAll('input[name="selectedService"]:checked');
        if (elements && elements.length > 0) {
            this.selectedProviders = [];
            for (let i = 0; i < elements.length; i++) {
                let element = elements[i] as HTMLElement;
                this.selectedProviders.push(parseInt(element['value'], null));
            }
        } else {
            this.comS.showHint(this.comS.lang['error_empty_service']);
        }
    }

    changeRecordTime() {
        this.updateRecordServices(true);
    }

    updateRecordServices(changeRecordTimeFlag: any = false) {
        this.comS.showAlert({show: false});
        let data = {
            'service_providers': this.selectedProviders
        };
        if (!changeRecordTimeFlag) {
            data['finish_time'] = this.record['finish_time'];
        }
        this.apiS.edit_record(this.recordId, data).subscribe(
            success => {
                if (success['status']) {
                    this.getRecordInfo(this.recordId);
                }
            }, error => {
                if (error.error.text.errors['existing_record_id']) {
                    this.comS.showHint(error.error.text.errors['message'], {text: 'Нажмите, чтобы просмотреть информацию о записи', url: '/record-info/' + error.error.text.errors['existing_record_id']});
                } else {
                    this.comS.showHint(error.error['text']['errors']);
                }
            }
        );
    }

    createDialog(client: any) {
        if (this.comS.updateUser() && client) {
            let data = {
                token: this.comS.getToken(),
                id_from: this.comS.userInfo['id'],
                id_to: client['user_id'],
                company_id: null,
                onGoToDialog: true
            };
            this.shared.createDialog.next(data);
        }
    }
}
