import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RecordInfoPage } from './record-info.page';
import { SharedModule } from '../../modules/shared.module';
import { MainGroupSharedModule } from '../../modules/main-group-shared.module';

const routes: Routes = [
  {
    path: '',
    component: RecordInfoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    MainGroupSharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RecordInfoPage]
})
export class RecordInfoPageModule {}
