import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SharedService} from '../../../services/shared.service';
import {ApiService} from '../../../services/api-service.service';

@Component({
  selector: 'app-services-page',
  templateUrl: './services-page.page.html',
  styleUrls: ['./services-page.page.scss'],
})
export class ServicesPagePage implements OnInit {

  public serviceForm: FormGroup;
  public duration: any;
  public focusObject = null;
  public focusOnField = false;
  public onlineRegister = 1;
  public descriptionRestriction = 300;
  public serviceCategoryId: any;
  public serviceId: any;
  public subscriptions = [];
  public behaviors = [];
  private isCustomService = false;
  public textAreaValue: any;
  public validateClicked = false;
  public services = [
      {
          formName: 'category',
          labelClassName: 'category',
          type: 'text',
          placeholder: 'Категория'
      },
      {
          formName: 'service_name',
          labelClassName: 'service_name',
          type: 'text',
          placeholder: 'Название услуги'
      },
      {
          formName: 'time',
          labelClassName: 'time',
          type: 'text',
          placeholder: 'Продолжительность'
      },
      {
          formName: 'cost',
          labelClassName: 'cost',
          type: 'number',
          placeholder: 'Стоимость'
      },
      {
          formName: 'describe',
          labelClassName: 'describe',
          type: 'text',
          placeholder: 'Описание',
          hasRestriction: true,
          restriction: 300
      }
  ];

  constructor(public comS: CommonService,
              public fb: FormBuilder,
              public shared: SharedService,
              public apiS: ApiService) {
      this.serviceForm = fb.group({
          'category': ['', Validators.required],
          'service_name': ['', Validators.required],
          'time': ['', Validators.required],
          'cost': ['', Validators.required],
          'describe': ['']
      });

      this.onChanges();

      let temp_d = new Date();
      temp_d.setHours(0, 5, 0, 0);
      this.duration = temp_d.toISOString();

      this.shared.serviceSectionSelected.subscribe(
          data => {
              this.serviceForm.controls['category'].setValue(data['name']);


              if (this.serviceCategoryId && data['id'] && (this.serviceCategoryId !== data['id'])) {
                  if (this.serviceId) {
                      this.notInSection(data['id']);
                  }
              }

              this.serviceCategoryId = data['id'];

              if (this.isCustomService) {
                  this.addNewServiceName();
              }
          });
      this.shared.serviceNameOnSearchSelected.subscribe(
          data => {
              this.serviceForm.controls['service_name'].setValue(data['name']);
              this.serviceId = data['id'];
              this.isCustomService = (data['id'] === null);

              if (data['section']) {
                  this.serviceForm.controls['category'].setValue(data['section']['name']);
                  this.serviceCategoryId = data['section']['id'];
              }

              if (this.serviceCategoryId && this.isCustomService) {
                  this.addNewServiceName();
              }
          }
      );
  }

    onChanges(): void {
        this.serviceForm.get('describe').valueChanges.subscribe(val => {
            setTimeout(() => {
                this.comS.resizeTextArea();
            }, 300);
        });
    }

  ionViewWillEnter() {
      // this.subscriptions.push(

      // );

      // this.behaviors.push(this.shared.serviceSection);
      // this.behaviors.push(this.shared.serviceNameOnSearch);
      // this.behaviors.push(this.shared.searchFinished);
  }

  ionViewDidLeave() {
      this.shared.serviceSection.next({});
      this.shared.serviceNameOnSearch.next({});
  }

  ngOnInit() {
  }

  notInSection(id: any) {
      this.apiS.get_services_by_category_id(id).subscribe(
          success => {
              if (success['status']) {
                  const servicesObj = success['text']['sections'];
                  let inSection = false;

                  if (servicesObj.length === 0) {
                      inSection = false;
                  } else {
                      for (let i = 0; i < servicesObj.length; i++) {
                          if (this.serviceId === servicesObj[i].id) {
                              inSection = true;
                          }
                      }
                  }
                  if (!inSection) {
                      this.serviceForm.controls['service_name'].setValue('');
                      this.serviceId = null;
                      this.isCustomService = false;
                  }
              }
          }, error => {
              console.log(error);
          }
      );
  }

  addNewServiceName() {
      let object =  {
              'section_id': this.serviceCategoryId,
              'name': this.serviceForm.controls['service_name'].value
          };
      // this.apiS.add_new_service(object).subscribe(
      //     success => {
      //         console.log(success);
      //     }, error => {
      //         console.log(error);
      //     }
      // );
  }

  saveService() {
      this.validateClicked = true;
      if (this.serviceForm.valid) {
          // if (parseFloat(this.serviceForm.controls['cost'].value) > 0) {
          //
          // } else {
          //
          // }
          let obj = {
              section_id: this.serviceCategoryId,
              service_id: this.serviceId,
              name: this.serviceForm.controls['service_name'].value,
              time: this.serviceForm.controls['time'].value,
              price: parseFloat(this.serviceForm.controls['cost'].value).toFixed(2),
              description: this.serviceForm.controls['describe'].value,
              can_record_online: this.onlineRegister
          };

          console.log(obj, 'obj');

          if (parseFloat(obj.price) > 0) {
              this.shared.serviceObject.next(obj);
              this.serviceForm.controls['category'].setValue('');
              this.serviceCategoryId = null;
              if (this.isCustomService) {
                  this.showNewServiceAlert();
              }
              this.comS.pushBack();
              this.clearPageData();
          } else {
              this.comS.showHint(this.comS.lang['error_zero_price']);
          }
      }
  }

  clearPageData() {
      this.serviceCategoryId = null;
      this.serviceId = null;
      this.isCustomService = false;
      this.focusObject = null;
      // this.serviceForm.reset();
      let keys = Object.keys(this.serviceForm.controls);
      for (let i = 0; i < keys.length; i++) {
          this.serviceForm.controls[keys[i]].setValue('');
      }
      for (let j = 0; j < this.subscriptions.length; j++) {
          this.subscriptions[j].unsubscribe();
          this.behaviors[j].next({});
      }
  }

  showNewServiceAlert() {
    let object = {
      title: 'Добавления услуги',
      text: {
          message: 'Вы добавили новую услугу, но она должна сперва пройти модерацию. Как только она будет проверена вы получите уведомление. Также, услуга будет отображена в списке услуг, но не будет активна'
      },
      image: 'popup-modal__subheading icon-checklist',
      buttons: {
          array: [
              {
                  title: 'Ок',
                  callback: { method: 'hideAlert' }
              }
          ],
          containerClassName: ''
      },
      show: true
    };
    this.comS.showAlert(object);
  }

  fieldOnFocus(service: any, ev: any) {
      if (service['formName'] === 'category') {
          this.comS.pushPage('/select-category');
      } else {
          this.focusObject = service;
          this.focusOnField = true;
      }
  }

  fieldOutOfFocus(formName) {
      this.focusOnField = false;
      setTimeout(() => {
          if (!this.focusOnField) {
              this.focusObject = null;
          }
      }, 200);
  }

  showSearch() {
      this.comS.showSearch({
          text: this.serviceForm.controls['service_name'].value,
          show: true,
          searchComponent: 'service',
          category: this.serviceCategoryId
      });
  }

  durationChanged(ev: any) {
      let temp = new Date(this.duration);
      this.serviceForm.controls['time'].setValue((temp.getMinutes() === 0 && temp.getHours() === 0) ? '' : this.comS.formatDateTime(this.duration, 'H:mm'));
  }

  openPicker(selector) {
    CommonService.showPicker(selector);
  }

  clearFieldValue(field, formName: any = null) {
      field.setValue('');
      if (formName === 'category') {
          this.serviceCategoryId = null;
      }
  }

}
