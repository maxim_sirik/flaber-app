import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommonService} from '../../services/common.service';
import {ApiService} from '../../services/api-service.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import {SharedService} from '../../services/shared.service';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.page.html',
  styleUrls: ['./personal-information.page.scss'],
})
export class PersonalInformationPage implements OnInit {
  public personalForm: FormGroup;
  public focusObject = null;
  public focusOnField = false;
  public addressOpened = false;
  public searchData: any;
  public selectedAddress: any;
  public selectedBirthDate: any;
  public serviceSections: any;
  public datePickerTouched = false;
  public photo = {
    base64: '',
    extension: ''
  };
  public photoBase64Raw = null;
  public changedUserFields = {};
  public imageChangedEvent: any = '';
  public validateClicked = false;
  public cropper = {
      image: '',
      opened: false,
      have_image: false,
      file_type: ''
  };
  public personal = {
      step: 0,
      navTitles: [
          this.comS.lang['nav_title_personal_info'],
          this.comS.lang['nav_title_personal_schedule'],
          this.comS.lang['nav_title_personal_services']
      ],
      containerClass: [
          'registration personal-information container-narrow',
          'personal-schedule container-menu',
          'registration section-services container-wide'
      ]
  };
  public workflowTableTimeFormat = 'HH:mm';
  public workflowTable = [];

  public personalArray = [
      {
          formName: 'first_name',
          className: 'name-input',
          labelClassName: 'icon-user name',
          type: 'text',
          placeholder: this.comS.lang['label_first_name']
      },
      {
          formName: 'last_name',
          className: 'surname-input',
          labelClassName: 'icon-user surname',
          type: 'text',
          placeholder: this.comS.lang['label_last_name']
      },
      {
          formName: 'patronymic',
          className: 'patronymic-input',
          labelClassName: 'icon-user patronymic',
          type: 'text',
          placeholder: this.comS.lang['label_patronymic']
      },
      {
          formName: 'address',
          className: 'adress-input',
          labelClassName: 'icon-location adress',
          type: 'text',
          placeholder: this.comS.lang['label_work_address']
      },
      {
          formName: 'birth_date',
          className: 'date-input',
          labelClassName: 'icon-calendar dob date',
          type: 'date',
          placeholder: this.comS.lang['label_birth_date']
      }
  ];

  public services = {};
  public servicesPure = [];
  public addedSections = [];

  constructor(public fb: FormBuilder,
              public comS: CommonService,
              public apiS: ApiService,
              public shared: SharedService) {
      this.personalForm = fb.group({
          'first_name': ['', Validators.compose([Validators.required])],
          'last_name': ['', Validators.compose([Validators.required])],
          'patronymic': ['', Validators.compose([Validators.required])],
          'address': ['', Validators.compose([Validators.required])],
          'birth_date': ['', Validators.compose([Validators.required])],
      });

      this.getWorkDays();
      this.serviceAdded();
      this.getServiceSections();

      this.shared.alertCallbackMethodFired.subscribe(
          methodObj => {
              if (methodObj['methodName']) {
                  if (typeof this[methodObj['methodName']] !== 'undefined') {
                      this[methodObj['methodName']]();
                  }
              }
          }
      );

      this.shared.addressOnSearchSelected.subscribe(
          data => {
              if (data['address']) {
                  for (let field of Object.keys(data)) {
                      this.changedUserFields[field] = data[field];
                  }
                  this.personalForm.controls['address'].setValue(this.changedUserFields['address']);
              }
          }

      );
  }



  ngOnInit() {}

  prevStep() {
      if (this.personal.step > 0) {
          this.personal.step--;
      }
  }

  nextStep() {
    let valid = false;
    if (this.personal.step === 0) {
        this.validateClicked = true;
        valid = this.personalValid(true);
    } else if (this.personal.step === 1) {
        valid = true;
    } else if (this.personal.step === 2) {
        this.registerWorker();
    }
    if (valid) {
        if (this.personal.step !== 2) {
            this.openAlert();
            this.personal.step++;
        }

    }
  }

  personalValid(fromNext = false) {
    let valid = this.personalForm.valid;
    if (fromNext) {
        if (valid) {
            for (let i = 0; i < Object.keys(this.personalForm.value).length; i++) {
                let key = Object.keys(this.personalForm.value)[i];
                this.changedUserFields[(key !== 'patronymic') ? key : 'second_name'] = this.personalForm.controls[key].value;
            }
        }
    }
    return valid;
  }

  // создание мастера на последнем шаге
  registerWorker() {

      let schedule = this.getSchedule();

      if (schedule.length > 0) {
          this.changedUserFields['schedule'] = schedule;
      }
      if (this.servicesPure.length > 0) {
          this.changedUserFields['services'] = this.servicesPure;
      }
      this.changedUserFields['description'] = null;

      this.apiS.register_worker(this.changedUserFields).subscribe(
          success => {
              if (success['status']) {
                  localStorage.setItem('worker_id', success['text']['worker']['id']);
                  if (success['text']['worker']) {
                      localStorage.setItem('worker_obj', JSON.stringify(success['text']['worker']));
                      this.comS.workerInfo = success['text']['worker'];
                  }
                  this.apiS.update_user_info();
                  setTimeout(() => {
                    this.comS.pushPage('/main/schedule');
                  }, 400);
                  // показать экраны 1.5.3 - 1.5.5 и перенаправить пользователя на главный экран
                  
              } else {
                  // show error hint
              }
          }, error => {
              if (error.error) {
                this.comS.showHint(error.error['text']['errors']);
              }
          }
      );
  }

  fieldOnFocus(field: any, ev: any) {
    this.focusObject = field;
    this.focusOnField = true;
    if (field['formName'] === 'address') {
        this.showSearchModal();
    }
  }

  fieldOutOfFocus(formName) {
      this.focusOnField = false;
      setTimeout(() => {
        if (!this.focusOnField) {
            this.focusObject = null;
        }
      }, 200);

      // if (formName === 'address' && this.personalForm.controls[formName].value) {
      //     this.addressOpened = true;
      //     CommonService.showPicker('.address_select');
      // }
  }

  showComponentPicker(selector: any, formName: any = null, formField: any = null) {
      if (!this.addressOpened) {
          CommonService.showPicker(selector);
      }
      if (formName === 'birth_date') {
          this.fieldOnFocus(formField, null);
          setTimeout(() => {
              this.datePickerTouched = true;
          }, 1000);
      }
  }

  clearFieldValue(field) {
      field.setValue('');
  }

  // searchAddress(fieldName: any) {
  //     if (fieldName === 'address') {
  //         this.apiS.get_address_data(this.personalForm.controls[fieldName].value).subscribe(
  //             success => {
  //                 this.searchData = success;
  //             }, error => {
  //                 this.searchData = [];
  //             }
  //         );
  //     }
  // }

  addressChanged() {
      this.personalForm.controls['address'].setValue(this.selectedAddress['display_name']);
      this.changedUserFields['lat'] = this.selectedAddress['lat'];
      this.changedUserFields['lng'] = this.selectedAddress['lon'];
      this.addressOpened = false;
  }

  showSearchModal() {
      this.comS.showSearch({
          text: '',
          show: true,
          searchComponent: 'address',
          category: null
      });
  }

  dateChanged(ev: any) {
      this.selectedBirthDate = this.comS.formatDateTime(this.selectedBirthDate, 'YYYY-MM-DD');
      this.personalForm.controls['birth_date'].setValue(this.selectedBirthDate);
  }

  photoPicked(ev: any) {
      this.imageChangedEvent = ev;
  }
  imageCropped(event: ImageCroppedEvent) {
      this.cropper.image = event.base64;
      this.cropper.file_type = event.file.type;
  }
  imageLoaded() {
      this.cropper.opened = true;
  }

  loadImageFailed() {
      this.cropper.opened = false;
      this.comS.showHint(this.comS.lang['error_failed_load_photo']);
  }

  closeCropper(save) {
      if (save) {
          this.photo.base64 = this.comS.getBase64Value(this.cropper.image);
          this.photoBase64Raw = this.cropper.image;
          this.photo.extension = (this.cropper.file_type && this.cropper.file_type.length > 0) ? this.cropper.file_type.split('/')[1] : '';
          this.changedUserFields['photo'] = this.photo;
      }
      this.cropper.have_image = this.photo !== null;
      this.cropper.opened = false;
  }

  openAlert() {

      let object = {};

      if (this.personal.step === 0) {
          object = {
              title: 'График работы',
              text: {
                  message: 'Заполните ваш график \n' +
                  'работы на неделю, это позволит клиентам \n' +
                  'осуществлять онлайн запись в те дни и \n' +
                  'время которые вы указали как рабочие. \n' +
                  'После завершения  регистрации можно будет изменить   '
              },
              image: 'popup-modal__subheading icon-calendar-big',
              buttons: {
                  array: [
                      {
                          title: 'Продолжить',
                          callback: { method: 'hideAlert' }
                      }
                  ],
                  containerClassName: ''
              },
              show: true
          };
      } else if (this.personal.step === 1) {
          object = {
              title: 'Мои услуги',
              text: {
                  message: 'Вам необходимо добавить предоставляемые вами услуги, после добавления услуг у клиента при поиске будут отображается ваши услуги  '
              },
              image: 'popup-modal__subheading icon-checklist',
              buttons: {
                  array: [
                      {
                          title: 'Продолжить',
                          callback: { method: 'hideAlert' }
                      }
                  ],
                  containerClassName: ''
              },
              show: true
          };
      }

      this.comS.showAlert(object);
  }

  hideAlert() {
      this.comS.showAlert({show: false});
  }

  getSchedule() {
      let schedule = [];
      for (let i = 0; i < this.workflowTable.length; i++) {
          if (this.workflowTable[i].isChecked) {
              schedule.push({
                  day_id: this.workflowTable[i].day_id,
                  start: this.workflowTable[i].timeStart,
                  end: this.workflowTable[i].timeEnd
              });
          }
      }
      return schedule;
  }
  
  serviceAdded() {
      this.shared.serviceObjectAdded.subscribe(
          service => {
              if (service['section_id']) {
                  this.servicesPure.push(service);
                  if (!this.addedSections.includes(service['section_id'])) {
                      this.addedSections.push(service['section_id']);
                  }

                  if (this.services['sections']) {
                      if (this.services['sections'][service['section_id']]) {
                          if (this.services['sections'][service['section_id']]['services']) {
                              this.services['sections'][service['section_id']]['services'].push(service);
                          } else {
                              this.services['sections'][service['section_id']]['services'] = [service];
                          }
                      } else {
                          this.services['sections'][service['section_id']] = {
                              name: this.getServiceSectionById(service['section_id'])['name'],
                              services: [service]
                          };
                      }
                  } else {
                      this.services['sections'] = {};
                      this.services['sections'][service['section_id']] = {
                          name: this.getServiceSectionById(service['section_id'])['name'],
                          services: [service]
                      };
                  }
              }
          }
      );
  }

  getServiceSections() {
      this.apiS.get_services_sections().subscribe(
          success => {
              if (success['status']) {
                  this.serviceSections = success['text']['sections'];
              }
          }, error => {
              console.log(error);
          }
      );
  }

  getServiceSectionById(id: any) {
      let response = {};
      if (this.serviceSections) {
          for (let i = 0; i < this.serviceSections.length; i++) {
              if (this.serviceSections[i].id === id) {
                  response = {
                     id: this.serviceSections[i].id,
                     name: this.serviceSections[i].name
                  };
                  break;
              }
          }
      }
      return response;
  }

  getWorkDays() {
      this.apiS.get_work_days().subscribe(
          success => {
              if (success['status']) {
                  let days = success['text']['days'];
                  let keys = Object.keys(days);
                  this.workflowTable = [];
                  for (let i = 0; i < keys.length; i++) {
                      this.workflowTable.push({
                          day_id: days[keys[i]].id,
                          dayShort: this.comS.lang['label_day_' + days[keys[i]].id + '_short'],
                          isChecked: 1,
                          timeStart: '09:00',
                          timeEnd: '20:00'
                      });
                  }
              }
          }, error => {
              console.log(error);
          }
      );
  }
}
