import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ImageCropperModule } from 'ngx-image-cropper';
import { IonicModule } from '@ionic/angular';
import { PersonalInformationPage } from './personal-information.page';
import { SharedModule } from '../../modules/shared.module';

const routes: Routes = [
  {
    path: '',
    component: PersonalInformationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ImageCropperModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PersonalInformationPage]
})
export class PersonalInformationPageModule {}
