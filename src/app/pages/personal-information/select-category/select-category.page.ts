import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {ApiService} from '../../../services/api-service.service';
import {SharedService} from '../../../services/shared.service';
import SimpleBar from 'simplebar';
import 'simplebar/dist/simplebar.css';

@Component({
  selector: 'app-select-category',
  templateUrl: './select-category.page.html',
  styleUrls: ['./select-category.page.scss'],
})
export class SelectCategoryPage implements OnInit {

  public sections: any;
  public simple: any;

  constructor(public comS: CommonService,
              public apiS: ApiService,
              public shared: SharedService) {
      this.getServicesSections();
      this.shared.alertCallbackMethodFired.subscribe(
          methodObj => {
              if (methodObj['methodName']) {
                  if (typeof this[methodObj['methodName']] !== 'undefined') {
                      this[methodObj['methodName']]();
                  }
              }
          }
      );
  }

  ngOnInit() {
      this.simple = new SimpleBar(document.querySelector('app-select-category .services-list'));
  }

  saveCategory() {
      let element = document.querySelector('input[name="selectedSection"]:checked') as HTMLElement;
      if (element) {
          this.shared.serviceSection.next({name: this.sections[element['value']]['name'], id: this.sections[element['value']]['id']});
          this.comS.pushBack();
      } else {
        this.comS.showHint(this.comS.lang['error_empty_service_section']);
      }
  }

    saveCustomCategory() {
      let categoryName = localStorage.getItem('customCategoryName');
      if (categoryName && categoryName !== 'null') {
          this.apiS.add_custom_category({
              name: categoryName
          }).subscribe(
              success => {
                  this.shared.serviceSection.next({name: categoryName, id: success['text']['section']['id']});
                  this.comS.pushBack();
              }, error => {
                  if (error.error) {
                      this.comS.showHint(error.error['text']['errors']);
                  }
              }
          );
      } else {
          this.comS.showHint(this.comS.lang['error_empty_category_name']);
      }
      this.hideAlert();

    }

    openAlert() {
        let object = {
            title: this.comS.lang['label_new_category_title'],
            text: {
                top: this.comS.lang['hint_new_category_top'],
                message: this.comS.lang['hint_new_category_message']
            },
            textField: {
                placeholder: this.comS.lang['label_add_category'],
                type: 'text',
                icon: 'icon-options',
                localStorageField: 'customCategoryName'
            },
            buttons: {
                array: [
                    {
                        title: this.comS.lang['button_cancel'],
                        callback: { method: 'hideAlert' }
                    },
                    {
                        title: this.comS.lang['button_save'],
                        callback: { method: 'saveCustomCategory' }
                    }
                ],
                containerClassName: ''
            },
            show: true
        };

        this.comS.showAlert(object);
    }

    hideAlert() {
        this.comS.showAlert({show: false});
    }

  getServicesSections() {
      this.apiS.get_services_sections().subscribe(
          success => {
              if (success['status']) {
                this.sections = success['text']['sections'];
              }
          }, error => {
              if (error.error) {
                  this.comS.showHint(error.error['text']['errors']);
              }
          }
      );
  }

}
