import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ContactEditPage } from './contact-edit.page';
import { MainGroupSharedModule } from '../../../../modules/main-group-shared.module';
import { SharedModule } from '../../../../modules/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ContactEditPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MainGroupSharedModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ContactEditPage]
})
export class ContactEditPageModule {}
