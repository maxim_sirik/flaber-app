import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../../services/api-service.service';
import {CommonService} from '../../../../services/common.service';
import {SharedService} from '../../../../services/shared.service';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import Inputmask from 'inputmask';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.page.html',
  styleUrls: ['./contact-edit.page.scss'],
})
export class ContactEditPage implements OnInit {

    public customerForm: FormGroup;
    public headerConfig: any;
    public imageChangedEvent: any;
    public photoBase64Raw: any;
    public customerId: any;
    public customer: any;
    public submitedInvalid = false;
    public selectedBirthDate: any;
    public focusObject: any;
    public focusOnField = false;
    public isNewCustomer = false;
    public changedUserFields = {};
    public validateClicked = false;
    public im: any;
    public photo = {
        base64: '',
        extension: ''
    };
    public subEvent: Subscription;

    public personalArray = [
        {
            formName: 'first_name',
            className: 'name-input',
            labelClassName: 'icon-user name',
            type: 'text',
            placeholder: this.comS.lang['label_first_name'],
            hasRestriction: false,
            restriction: 10000
        },
        {
            formName: 'last_name',
            className: 'surname-input',
            labelClassName: 'icon-user surname last-of-type',
            type: 'text',
            placeholder: this.comS.lang['label_last_name'],
            hasRestriction: false,
            restriction: 10000
        },
        {
            formName: 'phone',
            className: 'description-input',
            labelClassName: 'icon-options',
            type: 'text',
            placeholder: this.comS.lang['label_phone'],
            hasRestriction: false,
            restriction: 10000
        },
        {
            formName: 'email',
            className: 'description-input',
            labelClassName: 'icon-options last-of-type',
            type: 'text',
            placeholder: this.comS.lang['label_email'],
            hasRestriction: false,
            restriction: 10000
        },
        {
            formName: 'notes',
            className: 'description-input',
            labelClassName: 'icon-options',
            type: 'text',
            placeholder: this.comS.lang['label_notes'],
            hasRestriction: false,
            restriction: 10000
        },
        {
            formName: 'birth_date',
            className: 'date-input',
            labelClassName: 'icon-calendar dob date',
            type: 'date',
            placeholder: this.comS.lang['label_birth_date'],
            hasRestriction: false,
            restriction: 10000
        }
    ];

    constructor(public comS: CommonService,
                public apiS: ApiService,
                private router: ActivatedRoute,
                private shared: SharedService,
                public fb: FormBuilder) {

        this.customerForm = fb.group({
            'first_name': [''],
            'last_name': [''],
            'phone': ['', Validators.compose([Validators.required])],
            'notes': [''],
            'email': [''],
            'birth_date': [''],
        });

        this.headerConfig = {
            title: this.comS.lang['nav_title_client'],
            showBack: true,
            showNotif: false,
            showBtn: true,
            btn: {
                title: 'Готово',
                callback: 'customerAction'
            }
        };
    }

    ngOnInit() {

    }

    ionViewWillEnter() {
        this.im = new Inputmask(this.comS.phoneMask);
        this.subEvent = this.shared.alertCallbackMethodFired.subscribe(
            methodObj => {
                if (methodObj['methodName']) {
                    if (typeof this[methodObj['methodName']] !== 'undefined') {
                        this[methodObj['methodName']]();
                    }
                }
            }
        );
        this.router.params.subscribe(params => {
            this.isNewCustomer = false;
            if (params['id']) {
                this.customerId = params['id'];
                if (this.customerId !== 'new') {
                    this.getClientInfo(this.customerId);
                } else {
                    this.isNewCustomer = true;
                    this.initPhoneMaskField();
                }
            } else {
                this.comS.pushBack();
            }
        });

    }

    initPhoneMaskField(value: any = null) {
        let selector = document.querySelector('[data-form-name="phone"]');
        if (selector) {
            this.im.mask(selector);
            if (value) {
                selector['inputmask'].setValue(value.replace('380', ''));
            }
        }
    }

    ionViewDidLeave() {
        this.subEvent.unsubscribe();
    }

    getClientInfo(id: any) {
        this.apiS.get_customer_by_id(id).subscribe(
            success => {
                if (success['status']) {
                    this.customer = success['text']['customer'];
                    this.fillUserInfo();
                }
            }, error => {
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }

    fillUserInfo() {
        for (let i = 0; i < Object.keys(this.customerForm.value).length; i++) {
            let key = Object.keys(this.customerForm.value)[i];
            if (this.customer.user[key]) {
                this.customerForm.controls[key].setValue(this.customer.user[key]);
                if (key === 'phone') {
                    this.initPhoneMaskField(this.customer.user[key]);
                }
            }
        }
        if (this.customer.user['birth_date']) {
            this.selectedBirthDate = this.customer.user['birth_date'];
        }
    }

    dateChanged(ev: any) {
        this.selectedBirthDate = this.comS.formatDateTime(this.selectedBirthDate, 'YYYY-MM-DD');
        this.customerForm.controls['birth_date'].setValue(this.selectedBirthDate);
    }

    fieldOnFocus(field: any, ev: any) {
        this.focusObject = field;
        this.focusOnField = true;
    }

    fieldOutOfFocus(formName) {
        this.focusOnField = false;
        setTimeout(() => {
            if (!this.focusOnField) {
                this.focusObject = null;
            }
        }, 200);

        if (formName === 'address' && this.customerForm.controls[formName].value) {
            CommonService.showPicker('.address_select');
        }
    }

    photoPicked(ev: any) {
        this.imageChangedEvent = ev;
    }

    cropped(ev: any) {
        if (ev) {
            this.photoBase64Raw = ev.base64;
            this.photo.base64 = this.comS.getBase64Value(ev.base64);
            this.photo.extension = ev.extension;
            this.changedUserFields['photo'] = this.photo;
        } else {
            this.photoBase64Raw = null;
        }
    }

    clearFieldValue(field) {
        field.setValue('');
    }

    showComponentPicker(selector: any) {
        CommonService.showPicker(selector);
    }

    showDeleteAlert() {
        let object = {
            title: 'Предупреждение ',
            text: {
                top: 'Вы точно хотите удалить контакт?',
                message: 'В случае удаления контакта будет удалена вся информация и история.',
            },
            buttons: {
                array: [
                    {
                        title: 'Удалить',
                        callback: { method: 'deleteCustomer' }
                    },
                    {
                        title: 'Оставить',
                        callback: { method: 'hideAlert' }
                    }
                ],
                containerClassName: ''
            },
            show: true
        };

        this.comS.showAlert(object);
    }

    formValid() {
        let valid = this.customerForm.valid;
        if (valid) {
            for (let i = 0; i < Object.keys(this.customerForm.value).length; i++) {
                let key = Object.keys(this.customerForm.value)[i];
                if (this.customerForm.controls[key].value.length) {
                    this.changedUserFields[key] = this.customerForm.controls[key].value;
                } else {
                    if (key !== 'birth_date') {
                        this.changedUserFields[key] = '';
                    }
                }
            }
        }
        return valid;
    }

    customerAction() {
        this.validateClicked = true;
        this.submitedInvalid = !this.formValid();
        if (!this.submitedInvalid) {
            if (!this.isNewCustomer) {
                this.updateCustomer();
            } else {
                this.createCustomer();
            }

        }
    }

    updateCustomer() {
        if (typeof this.changedUserFields['email'] !== 'undefined') {
            let temp = this.changedUserFields['email'].replace(/\s/g, '');
            this.changedUserFields['email'] = (temp.length > 0) ? this.changedUserFields['email'] : null;
        }
        if (typeof this.changedUserFields['phone'] !== 'undefined') {
            let selector = document.querySelector('[data-form-name="phone"]');
            if (selector) {
                this.changedUserFields['phone'] = '380' + selector['inputmask'].unmaskedvalue();

            }
        }
        this.apiS.update_customer(this.customerId, this.changedUserFields).subscribe(
            success => {
                if (success['status']) {
                    this.comS.pushBack();
                }
            }, error => {
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }

    createCustomer() {
        if (typeof this.changedUserFields['email'] !== 'undefined') {
            let temp = this.changedUserFields['email'].replace(/\s/g, '');
            this.changedUserFields['email'] = (temp.length > 0) ? this.changedUserFields['email'] : null;
        }
        if (typeof this.changedUserFields['phone'] !== 'undefined') {
            let selector = document.querySelector('[data-form-name="phone"]');
            if (selector) {
                console.log(selector['inputmask'], selector['inputmask'].unmaskedvalue());
                this.changedUserFields['phone'] = '380' + selector['inputmask'].unmaskedvalue();

            }
        }
        this.apiS.add_customer(this.changedUserFields).subscribe(
            success => {
                if (success['status']) {
                    this.comS.pushPage('/main/contacts');
                }
            }, error => {
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }

    hideAlert() {
        this.comS.showAlert({show: false});
    }

    deleteCustomer() {
        this.apiS.delete_customer(this.customerId).subscribe(
            success => {
                if (success['status']) {
                    this.shared.alertCallbackMethod.next({});
                    this.comS.pushBack();
                    this.hideAlert();
                }
            }, error => {
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }



}
