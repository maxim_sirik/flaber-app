import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../../services/api-service.service';
import {CommonService} from '../../../../services/common.service';
import {ActivatedRoute} from '@angular/router';
import {SharedService} from '../../../../services/shared.service';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.page.html',
  styleUrls: ['./contact-detail.page.scss'],
})
export class ContactDetailPage implements OnInit {

  public headerConfig: any;
  public customer: any;
  public customerId: any;


  constructor(public comS: CommonService,
              public apiS: ApiService,
              private router: ActivatedRoute,
              private shared: SharedService) {
      this.headerConfig = {
          title: this.comS.lang['nav_title_client'],
          showBack: true,
          showNotif: false,
          showBtn: true,
          btn: {
              title: 'Править',
              callback: 'editContact'
          }
      };

      this.shared.alertCallbackMethodFired.subscribe(
          methodObj => {
              if (methodObj['methodName']) {
                  if (typeof this[methodObj['methodName']] !== 'undefined') {
                      this[methodObj['methodName']]();
                  }
              }
          }
      );
  }

  ngOnInit() {


  }

  ionViewWillEnter() {
      this.router.params.subscribe(params => {
          if (params['id']) {
              this.customerId = params['id'];
              this.getClientInfo(this.customerId);
          } else {
              this.comS.pushPage('/main/contacts');
          }
      });

  }

  getClientInfo(id: any) {
      this.apiS.get_customer_by_id(id).subscribe(
          success => {
              if (success['status']) {
                  this.customer = success['text']['customer'];
              }
              if (!this.customer || this.customer.user.status_id !== 69) {
                  this.comS.pushPage('/main/contacts');
              }
          }, error => {
              if (error.error) {
                  this.comS.showHint(error.error.text.errors);
              }
          }
      );
  }

  editContact() {
    this.comS.pushPage('main/contacts/' + this.customerId + '/edit');
  }

}
