import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { Contacts } from '@ionic-native/contacts/ngx';

import { IonicModule } from '@ionic/angular';

import { ContactsPage } from './contacts.page';
import { MainGroupSharedModule } from '../../../modules/main-group-shared.module';
import { SharedModule } from '../../../modules/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ContactsPage
  },
  { path: 'import', loadChildren: './/contacts-import/contacts-import.module#ContactsImportPageModule' },
  { path: ':id', loadChildren: './contact-detail/contact-detail.module#ContactDetailPageModule' },
  { path: ':id/edit', loadChildren: './contact-edit/contact-edit.module#ContactEditPageModule' },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MainGroupSharedModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ContactsPage],
  providers: [Contacts]
})
export class ContactsPageModule {}
