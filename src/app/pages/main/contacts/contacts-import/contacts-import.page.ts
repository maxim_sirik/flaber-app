import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Contacts } from '@ionic-native/contacts/ngx';
import {CommonService} from '../../../../services/common.service';
import {SharedService} from '../../../../services/shared.service';
import {ApiService} from '../../../../services/api-service.service';
import {Subscription} from 'rxjs';
import Inputmask from 'inputmask';


@Component({
  selector: 'app-contacts-import',
  templateUrl: './contacts-import.page.html',
  styleUrls: ['./contacts-import.page.scss'],
})
export class ContactsImportPage implements OnInit {

  public gettedClients: any = [];
  public selector: any;
  public customersToShow: any = [];
  public wordsFounded: any = [];
  public othersFound: any = [];
  public allFounded: any = [];
  public headerConfig: any;
  public contactsToImport: any = [];
  public im: any;
  public sharedSubscription: Subscription;

  constructor(public comS: CommonService,
              private cc: Contacts,
              private shared: SharedService,
              private sanitizer: DomSanitizer,
              private apiS: ApiService) {
      this.headerConfig = {
          title: this.comS.lang['nav_title_contacts'],
          showBack: true,
          showNotif: false,
          showBtn: true,
          btn: {
              title: this.comS.lang['button_done'],
              callback: 'importContacts'
          },
          withoutShadow: true
      };

      this.sharedSubscription = this.shared.alertCallbackMethodFired.subscribe(
          methodObj => {
              if (methodObj['methodName']) {
                  if (typeof this[methodObj['methodName']] !== 'undefined') {
                      this[methodObj['methodName']]();
                  }
              }
          }
      );
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
      this.im = new Inputmask(this.comS.phoneMask);
      this.selector = document.querySelector('.contacts-mask-input');
      if (this.selector) {
          this.im.mask(this.selector);
      }
      if (this.cc) {
          let findResponse = this.cc.find(['displayName', 'name', 'phoneNumbers', 'emails'], {filter: '', multiple: true});
          if (findResponse) {
              findResponse.then(data => {
                  this.gettedClients = data;
                  this.filterCustomers(this.gettedClients);

                  // console.log()
              });
          }
      }

      let response = {
          "_objectInstance":{
              "id":71,
              "rawId":null,
              "displayName":null,
              "name":{
                  "givenName":"",
                  "honorificSuffix":"",
                  "formatted":"",
                  "middleName":"",
                  "familyName":"",
                  "honorificPrefix":""
              },
              "nickname":"",
              "phoneNumbers":[
                  {"value":"+79286276352","pref":false,"id":0,"type":"mobile"}
              ],
              "emails":null,
              "addresses":null,
              "ims":null,
              "organizations":[
                  {
                      "pref":"false",
                      "title":"",
                      "name":"",
                      "department":"",
                      "type":null
                  }
              ],
              "birthday":null,
              "note":"",
              "photos":null,
              "categories":null,
              "urls":null
          },
          "rawId":null
      };
  }

    filterCustomers(customers: any) {
        this.customersToShow = {};
        this.wordsFounded = [];
        this.othersFound = [];
        this.allFounded = [];

        if (customers && customers.length > 0) {
            customers.forEach((customer) => {
                let letterFound = false;
                let word = '';
                this.comS.alphabet.forEach((alphabet) => {

                    if (!letterFound) {
                        if (customer['_objectInstance']['name']['familyName']) {
                            word = customer['_objectInstance']['name']['familyName'][0].toLowerCase();
                            if (alphabet.words.includes(word) && word.length > 0) {
                                letterFound = true;
                            }
                        } else if (customer['_objectInstance']['name']['givenName']) {
                            word = customer['_objectInstance']['name']['givenName'][0].toLowerCase();
                            if (alphabet.words.includes(word) && word.length > 0) {
                                letterFound = true;
                            }
                        }
                    }
                });

                let phoneRaw = customer['_objectInstance']['phoneNumbers'] ? customer['_objectInstance']['phoneNumbers'][0]['value'].replace(/ /g, '').replace(/\+|\(|\)|\-/g, '') : null;
                this.selector['inputmask'].setValue(phoneRaw ? phoneRaw.replace('380', '') : null);

                let formatedClient = {
                    first_name: customer['_objectInstance']['name']['formatted'] ? customer['_objectInstance']['name']['formatted'].replace(customer['_objectInstance']['name']['familyName'], '') : '',
                    last_name: customer['_objectInstance']['name']['familyName'],
                    second_name: customer['_objectInstance']['name']['middleName'],
                    formatted: customer['_objectInstance']['name']['formatted'],
                    phone: this.selector['inputmask'].isValid() ? '380' + this.selector['inputmask'].unmaskedvalue() : phoneRaw,
                    email: customer['_objectInstance']['emails'] ? customer['_objectInstance']['emails'][0]['value'] : null,
                    birth_date: customer['_objectInstance']['birthday'] ? this.comS.formatDateTime(customer['_objectInstance']['birthday'], 'YYYY-MM-DD') : null,
                    notes: customer['_objectInstance']['note']
                };

                if (letterFound) {
                    if (!this.wordsFounded.includes(word)) {
                        this.wordsFounded.push(word);
                    }
                    if (!this.customersToShow[word]) {
                        this.customersToShow[word] = [];
                    }
                    this.customersToShow[word].push(formatedClient);
                } else {
                    this.othersFound.push(formatedClient);
                }
                this.allFounded.push(formatedClient);
            });
            this.wordsFounded = this.wordsFounded.sort();
        }
    }

    markToImport(contact: any) {
      if (this.contactsToImport.indexOf(contact) === -1) {
          this.contactsToImport.push(contact);
      } else {
          this.contactsToImport.splice(this.contactsToImport.indexOf(contact), 1);
      }
    }

    importAll() {
        this.contactsToImport = this.allFounded;
        this.importContacts();
    }

    importContacts() {
      // console.log('imprt!');
        this.apiS.add_multiple_customers(this.contactsToImport).subscribe(
            success => {
                if (success['status']) {
                    let existing = success['text']['existing_customers'];
                    let invalid = success['text']['invalid_customers'];
                    let saved = success['text']['customers'];
                    let hintArray = [];
                    if (existing.length > 0) {
                        hintArray.push(existing.length + this.comS.lang['hint_contacts_exists']);
                    }
                    if (invalid.length > 0) {
                        hintArray.push(invalid.length + this.comS.lang['hint_contacts_invalid']);
                    }
                    hintArray.push(saved.length + this.comS.lang['hint_contacts_imported']);
                    this.comS.showHint(hintArray);
                    this.comS.pushBack();
                }
            }, error => {
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }

    ionViewWillLeave() {
        this.sharedSubscription.unsubscribe();
    }

}
