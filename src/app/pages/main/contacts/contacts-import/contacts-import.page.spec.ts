import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactsImportPage } from './contacts-import.page';

describe('ContactsImportPage', () => {
  let component: ContactsImportPage;
  let fixture: ComponentFixture<ContactsImportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactsImportPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactsImportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
