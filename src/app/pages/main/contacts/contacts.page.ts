import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {ApiService} from '../../../services/api-service.service';
import {SharedService} from '../../../services/shared.service';
import {Subscription} from 'rxjs';
import Inputmask from 'inputmask';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.scss']
})
export class ContactsPage implements OnInit {

    public headerConfig: any;
    public customers: any = [];
    public searchClients: any;
    public customersToShow: any = [];
    public customersFiltered: any = [];
    public wordsFounded: any = [];
    public othersFound: any = [];
    public subEvent: Subscription;
    public isOnloadMoreCustomers = false;
    public isLastPage = false;
    public currentPage = 1;
    public im: any;

    constructor(public comS: CommonService,
                public apiS: ApiService,
                private shared: SharedService) {
        this.headerConfig = {
          title: this.comS.lang['nav_title_contacts'],
          showBack: false,
          showNotif: false,
          showBtn: true,
          withoutShadow: true,
          btn: {
              title: 'Добавить',
              callback: 'showAddCustomer'
          }
        };
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.im = new Inputmask(this.comS.phoneMask);
        this.getCustomers();
        this.subEvent = this.shared.alertCallbackMethodFired.subscribe(
            methodObj => {
                if (methodObj['methodName']) {
                    if (typeof this[methodObj['methodName']] !== 'undefined') {
                        this[methodObj['methodName']]();
                    }
                }
            }
        );
        this.initScrollLoadListener();
    }

    initScrollLoadListener() {
        let containerElement = document.querySelector('app-contacts main');

        if (containerElement) {
            containerElement.addEventListener('scroll', (ev) => { // пагинация контактов по скроллу
                let target = ev.target as HTMLElement;
                if (target) {
                    if (target.scrollHeight - target.offsetHeight === target.scrollTop) {
                        if (!this.isOnloadMoreCustomers && !this.isLastPage) {
                            this.getCustomers(this.currentPage + 1);
                        }

                    }
                }
            });
        } else {
            setTimeout(() => {
                this.initScrollLoadListener();
            }, 100);
        }
        //
    }

    ionViewDidLeave() {
        this.shared.alertCallbackMethod.next({});
        this.customers = [];
        this.subEvent.unsubscribe();
    }

    getCustomers(page: any = null) {
        this.isOnloadMoreCustomers = true;
        this.apiS.get_customers(page).subscribe(
          success => {
              if (success['status']) {
                  // this.customers = success['text']['customers'];
                  if (success['text']['customers'] && success['text']['customers']['data']) {
                      for (let customer of success['text']['customers']['data']) {
                          this.customers.push(customer);
                      }
                  }
                  if (success['text']['customers'] && success['text']['customers']['current_page']) {
                      this.isLastPage = success['text']['customers']['current_page'] === success['text']['customers']['last_page'];
                      this.currentPage = success['text']['customers']['current_page'];
                  }
                  this.filterSearchResults();
              }
              this.isOnloadMoreCustomers = false;
          }, error => {
              this.isOnloadMoreCustomers = false;
              if (error.error) {
                  this.comS.showHint(error.error.text.errors);
              }
          }
        );
    }

    filterCustomers(customers: any) {
        this.customersToShow = {};
        this.wordsFounded = [];
        this.othersFound = [];

        if (customers && customers.length > 0) {
            customers.forEach((customer) => {
              let letterFound = false;
              let word = '';
              this.comS.alphabet.forEach((alphabet) => {

                if (!letterFound) {
                  if (customer['last_name']) {
                      word = customer['last_name'][0].toLowerCase();
                      if (alphabet.words.includes(word) && word.length > 0) {
                          letterFound = true;
                      }
                  } else if (customer['first_name']) {
                      word = customer['first_name'][0].toLowerCase();
                      if (alphabet.words.includes(word) && word.length > 0) {
                          letterFound = true;
                      }
                  }
                }
              });
              if (letterFound) {
                  if (!this.wordsFounded.includes(word)) {
                      this.wordsFounded.push(word);
                  }
                  if (!this.customersToShow[word]) {
                      this.customersToShow[word] = [];
                  }
                  this.customersToShow[word].push(customer);
              } else {
                  this.othersFound.push(customer);
              }
            });
            this.wordsFounded = this.wordsFounded.sort();
        }
    }

    filterSearchResults() {
        this.customersFiltered = [];
        if (this.searchClients && this.searchClients.length > 0) {
            for (let i = 0; i < this.customers.length; i++) {
                if (this.customers[i]['first_name'] && this.customers[i]['first_name'].toLowerCase().indexOf(this.searchClients.toLowerCase()) !== -1) {
                    this.customersFiltered.push(this.customers[i]);
                } else if (this.customers[i]['last_name'] && this.customers[i]['last_name'].toLowerCase().indexOf(this.searchClients.toLowerCase()) !== -1) {
                    this.customersFiltered.push(this.customers[i]);
                } else if (this.customers[i]['phone'] && this.customers[i]['phone'].toLowerCase().indexOf(this.searchClients.toLowerCase()) !== -1) {
                    this.customersFiltered.push(this.customers[i]);
                }
            }
        } else {
            this.customersFiltered = this.customers;
            this.maskPhones();
        }
        this.filterCustomers(this.customersFiltered);
    }

    maskPhones() {
        setTimeout(() => {
            for (let item of this.customersFiltered) {
                // phone
                let selector = document.querySelector('[data-custid="' + item.id + '"] .phone input');
                if (selector) {
                    this.im.mask(selector);
                    selector['inputmask'].setValue(item['phone'].replace('380', ''));
                }
            }
        }, 300);
    }

    showAddCustomer() {
        let object = {
            title: 'Добавить контакт',
            text: {
                top: 'Вы можете добавить',
                message: 'Из контактов вашего телефона или создать новый контакт',
            },
            buttons: {
                array: [
                    {
                        title: 'Создать новый контакт',
                        callback: { method: 'addCustomer' }
                    },
                    {
                        title: 'Импортировать из контактов',
                        callback: { method: 'importFromDevice' }
                    }
                ],
                containerClassName: 'vertical'
            },
            show: true
        };

        this.comS.showAlert(object);

    }

    hideAlert() {
        this.comS.showAlert({show: false});
    }

    importFromDevice() {
        this.comS.pushPage('/main/contacts/import');
        this.hideAlert();
    }

    addCustomer() {
      this.comS.pushPage('/main/contacts/new/edit');
      this.hideAlert();
    }

    goToContact(id: any) {
        this.comS.pushPage('main/contacts/' + id);
    }

    createDialog(client: any) {
        if (this.comS.updateUser()) {
            let data = {
                token: this.comS.getToken(),
                id_from: this.comS.userInfo['id'],
                id_to: client['user_id'],
                company_id: null,
                onGoToDialog: true
            };
            this.shared.createDialog.next(data);
        }
    }

}
