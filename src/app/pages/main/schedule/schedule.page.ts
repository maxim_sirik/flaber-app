import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {ApiService} from '../../../services/api-service.service';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.page.html',
  styleUrls: ['./schedule.page.scss']
})
export class SchedulePage implements OnInit {

    public headerConfig: any;
    public workflowTableTimeFormat = 'H:mm';
    public workflowTable = [];

    constructor(public comS: CommonService,
                private apiS: ApiService) {

        this.headerConfig = {
            title: this.comS.lang['nav_title_schedule'],
            showBack: true,
            showNotif: true
        };

        this.apiS.get_worker_schedule().subscribe(
            success => {
                if (success['status']) {
                    let worktimes = success['text']['worktimes'];
                    this.workflowTable = [];
                    for (let i = 1; i <= 7; i++) {
                        let splittedStartTime = (worktimes[i] ? worktimes[i]['start'] : '9:00').split(':');
                        let splittedEndTime = (worktimes[i] ? worktimes[i]['end'] : '20:00').split(':');
                        this.workflowTable.push({
                            day_id: i,
                            id: worktimes[i] ? worktimes[i]['id'] : null,
                            dayShort: this.comS.lang['label_day_' + i + '_short'],
                            isChecked: worktimes[i] ? 1 : 0,
                            timeStart: worktimes[i] ? (splittedStartTime.length > 2 ? splittedStartTime[0] + ':' + splittedStartTime[1] : worktimes[i]['start']) : '9:00',
                            timeEnd: worktimes[i] ? (splittedEndTime.length > 2 ? splittedEndTime[0] + ':' + splittedEndTime[1] : worktimes[i]['end']) : '20:00',
                        });
                    }
                }
            }, error => {
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }

    ngOnInit() {}

    showComponentPicker(selector: any, formName: any = null, formField: any = null) {
        CommonService.showPicker(selector);
    }

    getSchedule() {
        let schedule = [];
        for (let i = 0; i < this.workflowTable.length; i++) {
            if (this.workflowTable[i].isChecked) {
                schedule.push({
                    day_id: this.workflowTable[i].day_id,
                    start: this.workflowTable[i].timeStart,
                    end: this.workflowTable[i].timeEnd
                });
            }
        }
        return schedule;
    }

    timeChanged(row: any, start: boolean) {
        if (row['isChecked']) {
            this.updateDays(row);
        }
    }

    updateDayState(row: any) {
        console.log(row, 'row');
        if (row['isChecked']) {
            this.addDays(row);
        } else {
            this.deleteDays(row);
        }
    }

    updateDays(row: any) {
        let object = {
            'start': this.comS.formatTime(row['timeStart'], 'HH:mm', 'H:mm'),
            'end': this.comS.formatTime(row['timeEnd'], 'HH:mm', 'H:mm')
        };
        this.apiS.update_work_days(row['id'], object).subscribe(
            success => {
                if (success['status']) {
                    row['id'] = success['text']['worktime']['id'];
                }
            }, error => {
                row['isChecked'] = false;
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }

    deleteDays(row) {
        this.apiS.delete_work_days(row['id']).subscribe(
            success => {
            }, error => {
                row['isChecked'] = true;
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }

    addDays(row: any) {
        let object = {
            day_id: row['day_id'],
            start: this.comS.formatTime(row['timeStart'], 'HH:mm', 'H:mm'),
            end: this.comS.formatTime(row['timeEnd'], 'HH:mm', 'H:mm')
        };
        this.apiS.add_work_days(object).subscribe(
            success => {
                if (success['status']) {
                    row['id'] = success['text']['worktime']['id'];
                }
            }, error => {
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }

}
