import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MessagesPage } from './messages.page';
import { MainGroupSharedModule } from '../../../modules/main-group-shared.module';

const routes: Routes = [
  {
    path: '',
    component: MessagesPage
  },
  { path: 'dialogue/:id', loadChildren: './dialogue/dialogue.module#DialoguePageModule' },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MainGroupSharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MessagesPage]
})
export class MessagesPageModule {}
