import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {SocketService} from '../../../services/socket-service.service';
import {ApiService} from '../../../services/api-service.service';
import {SharedService} from '../../../services/shared.service';
import {Subscription} from 'rxjs';
import Inputmask from 'inputmask';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.page.html',
  styleUrls: ['./messages.page.scss'],
  providers: [SocketService]
})
export class MessagesPage implements OnInit {

    public im: any;
    public headerConfig: any;
    public message: any;
    public currentDialog: any = 2;
    public dialogs: any = {
        data: [],
        current_page: 1,
        last_page: 1
    };
    public alreadyJoined: any = [];
    public alreadyAdded: any = [];
    public newMessageInDialogue: any = [];
    public subscription: Subscription;
    public isOnDialogLoaded: any = false;
    public isLastPage: any = false;
    public currentPage: any = 0;

    constructor(public comS: CommonService,
                public apiS: ApiService,
                private shared: SharedService) {
        this.headerConfig = {
            title: this.comS.lang['nav_title_messages'],
            showBack: false,
            showNotif: true
        };

        this.shared.alertCallbackMethodFired.subscribe(
            methodObj => {
                if (methodObj['methodName']) {
                    if (typeof this[methodObj['methodName']] !== 'undefined') {
                        this[methodObj['methodName']]();
                    }
                }
            }
        );

        this.subscription = this.shared.onGetMessage.subscribe(
            data => {
                this.getMessage(data);
            }
        );
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.im = new Inputmask(this.comS.phoneMask);
        this.getDialogsList(this.currentPage + 1);
        this.addMessagesScrollListener();
    }

    initPhoneMask() {
        setTimeout(() => {
            for (let dialog of this.dialogs.data) {
                // phone
                let selector = document.querySelector('[data-custphone="' + dialog.dialog_users[0]['phone'] + '"]');
                if (selector) {
                    this.im.mask(selector);
                    selector['inputmask'].setValue(dialog.dialog_users[0]['phone'].replace('380', ''));
                }
            }
        }, 300);
        // app-messages .contacts-new__quantity input
    }

    ionViewDidLeave() {
        this.dialogs = {
            data: [],
            current_page: 1,
            last_page: 1
        };
        this.alreadyJoined = [];
        this.alreadyAdded = [];
        this.newMessageInDialogue = [];
        this.subscription.unsubscribe();
    }

    getDialogsList(page: any) {
        this.isOnDialogLoaded = true;
        this.apiS.get_dialog_list(page).subscribe(
            success => {
                this.isOnDialogLoaded = false;
                if (success['status']) {
                    this.currentPage = success['text']['dialogs']['current_page'];
                    this.isLastPage = success['text']['dialogs']['current_page'] === success['text']['dialogs']['last_page'];
                    for (let dialog of success['text']['dialogs']['data']) {
                        if (this.alreadyAdded.indexOf(dialog['id']) === -1) {
                            this.dialogs.data.push(dialog);
                            this.alreadyAdded.push(dialog['id']);
                        }
                    }
                    this.initPhoneMask();
                    this.dialogs.last_page = success['text']['dialogs']['last_page'];
                    this.dialogs.current_page = success['text']['dialogs']['current_page'];
                }
            }, error => {
                this.isOnDialogLoaded = false;
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }

    addMessagesScrollListener() {
        let scrollElem = document.querySelector('app-messages main');

        if (scrollElem) {
            scrollElem.addEventListener('scroll', (ev) => {
                if (ev.target['scrollTop'] === scrollElem.scrollHeight - window.innerHeight) {
                    if (!this.isOnDialogLoaded && !this.isLastPage) {
                        this.getDialogsList(this.currentPage + 1);
                    }
                }

            });
        } else {
            setTimeout(() => {
               this.addMessagesScrollListener();
            }, 200);
        }

        // app-messages
    }

    join_to_dialogs() {
        for (let dialog of this.dialogs.data) {
            if (this.alreadyJoined.indexOf(dialog['id']) === -1) {
                this.connectToRoom(dialog['id']);
                this.currentDialog = dialog['id'];
                this.alreadyJoined.push(dialog['id']);
            }
        }
    }

    createDialog() {
        if (this.comS.updateUser()) {
            let data = {
                token: this.comS.getToken(),
                id_from: this.comS.userInfo['id'],
                id_to: '31',
                company_id: null
            };
        } else {
            console.log('user not exists');
        }
    }

    connectToRoom(dialog_id: any = null) {
        let data = {
            token: this.comS.getToken(),
            dialog_id: dialog_id !== null ? dialog_id : this.currentDialog
        };
        this.shared.connectToDialogue.next(data);
    }

    getMessage(data: any) {
        if (data['dialog_id']) {
            for (let dialog of this.dialogs.data) {
                if (parseInt(dialog['id'], null) === parseInt(data['dialog_id'], null)) {
                    dialog.messages = [data['message']];
                    if (this.newMessageInDialogue.indexOf(parseInt(dialog['id'], null)) === -1) {
                        this.newMessageInDialogue.push(parseInt(dialog['id'], null));
                    }

                }
            }
        }
    }

}
