import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../../services/common.service';
import {ActivatedRoute} from '@angular/router';
import {SocketService} from '../../../../services/socket-service.service';
import {ApiService} from '../../../../services/api-service.service';
import {SharedService} from '../../../../services/shared.service';
import {LocalNotificationService} from '../../../../services/local-notification.service';
import SimpleBar from 'simplebar';
import Inputmask from 'inputmask';
import 'simplebar/dist/simplebar.css';

@Component({
  selector: 'app-dialogue',
  templateUrl: './dialogue.page.html',
  styleUrls: ['./dialogue.page.scss'],
  providers: [SocketService]
})
export class DialoguePage implements OnInit {
    public im: any;
    public focusObject: any;
    public focusOnField = false;
    public noMoreMessages = false;
    public message: any;
    public currentRoom: any;
    public messages: any = [];
    public dialogInfo: any;
    public subscriptions: any = [];
    public simple: any;
    public oldMessages: any = [];
    public prevMessageId: any;
    public heightBeforePagination: any = 0;
    private newLoaded = false;
    private currentScrollValue = 0;
    private scrollBeforePagination = 0;

  constructor(public comS: CommonService,
              private router: ActivatedRoute,
              public sockS: SocketService,
              private apiS: ApiService,
              private shared: SharedService,
              private localN: LocalNotificationService) {

      let sub_2 = this.shared.onGetMessage.subscribe(
          data => {
              this.getChatMessage(data);
          }
      );

      this.subscriptions = [sub_2];
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
      this.im = new Inputmask(this.comS.phoneMask);
      this.router.params.subscribe(params => {
        // console.log(params);
          if (params['id']) {
              this.currentRoom = params['id'];
              let data = {
                  token: this.comS.getToken(),
                  dialog_id: this.currentRoom
              };
              this.scrollLoadListener();
              this.getDialogInfo(this.currentRoom);
              this.getDialogMessages(this.currentRoom);
          } else {
          }
      });

  }

  ionViewDidLeave() {
      for (let sub of this.subscriptions) {
          sub.unsubscribe();
      }
  }

  scrollLoadListener(count: any = 0) {
      this.simple = new SimpleBar(document.querySelector('app-dialogue .section-messages__dialogue.dark-bg'));
      if (this.simple.getScrollElement()) {
          this.simple.getScrollElement().addEventListener('scroll', (ev) => {
              this.currentScrollValue = ev.target['scrollTop'];
              console.log(ev.target['scrollTop'], this.newLoaded);
              if (ev.target['scrollTop'] === 0 && !this.noMoreMessages && this.messages.length > 0 && !this.newLoaded) {
                  this.newLoaded = true;
                  this.scrollBeforePagination = ev.target['scrollTop'];
                  this.getDialogMessages(this.currentRoom, false, this.messages[this.messages.length - 1]['id']);
              }
          });
      } else {
          if (count < 15) {
              count++;
              setTimeout(() => {
                  this.scrollLoadListener(count);
              }, 100);
          } else {
              console.log('unable to add scroll listener');
              return false;
          }
      }
  }

    fieldOnFocus(field: any) {
        this.focusObject = field;
        this.focusOnField = true;
    }

    fieldOutOfFocus() {
        this.focusOnField = false;
        setTimeout(() => {
            if (!this.focusOnField) {
                this.focusObject = null;
            }
        }, 200);
    }

    clearFieldValue(field: any = null) {
        this.message = '';
        this.resetTextAreaRows();
    }

    resetTextAreaRows() {
        let textbox = document.querySelector('.form-item textarea') as HTMLElement;
        if (textbox) {
            textbox['rows'] = 1;
        }
    }

    getDialogInfo(id: any) {
      this.apiS.get_dialog_info(id).subscribe(
          success => {
              if (success['status']) {
                  this.dialogInfo = success['text']['dialog'];
                  this.initPhoneMask();
              }
          }, error => {
              console.log(error);
          }
      );
    }

    initPhoneMask() {
        setTimeout(() => {
            let selector = document.querySelector('app-dialogue .personal-info__heading input');
            if (selector) {
                this.im.mask(selector);
                selector['inputmask'].setValue(this.dialogInfo.users[0]['phone'].replace('380', ''));
            }
        }, 300);
    }

    getDialogMessages(id: any, scroll: boolean = true, last_msg_id: any = null) {
        this.apiS.get_dialog_messages(id, last_msg_id).subscribe(
            success => {
                if (success['status']) {
                    let lastDialogueMessage;
                    if (last_msg_id) {
                        this.heightBeforePagination = this.simple['getContentElement']() ? this.simple['getContentElement']().scrollHeight : 0;
                        lastDialogueMessage = document.querySelector('.dialogue li:last-child');
                    }
                    this.messages = this.messages.concat(success['text']['messages']);
                    this.noMoreMessages = success['text']['messages'].length === 0;
                    if (!this.noMoreMessages) {
                        this.addMessagesToChat(success['text']['messages']);
                    }
                    if (scroll) {
                        this.scrollToBottomOfChat();
                    }
                    // this.scrollToLast(lastDialogueMessage);
                }
            }, error => {

            }
        );
    }

    getChatMessage(data: any) {
        if (parseInt(this.currentRoom, null) === parseInt(data['dialog_id'], null)) {
            this.messages.unshift(data['message']);
            this.addMessagesToChat([data['message']], true);
            if (this.comS.userInfo['id'] === data['message']['author']) {
                this.clearFieldValue();
            }
        }
    }

    sendMessageToDialogue(ev: any) {

        ev.preventDefault();
        ev.stopPropagation();

        let data = {
            'token': this.comS.getToken(),
            'dialog_id': this.currentRoom,
            'message': this.message
        };

        this.shared.sendMessage.next(data);
    }

    formatMessageTime(time: any) {
      let format = this.comS.isCurrentDay(this.comS.getMomentObject(time)) ? 'HH:mm' : 'DD MMM. HH:mm';
      return this.comS.formatDateTime(time, format);
    }

    scrollToBottomOfChat() {

      let scrollWrapper = document.querySelector('.simplebar-content-wrapper');
      if (scrollWrapper) {
          scrollWrapper.scrollTo(0, this.simple['getContentElement']().scrollHeight - window.innerHeight - 1);
      }
    }

    scrollToPreviousMessage() {
        let scrollWrapper = document.querySelector('.simplebar-content-wrapper');
        if (scrollWrapper) {
            // scrollWrapper.scrollTop = (this.simple['getContentElement']().scrollHeight + this.scrollBeforePagination) - (this.heightBeforePagination + this.currentScrollValue);
            scrollWrapper.scrollTo(0, (this.simple['getContentElement']().scrollHeight + this.scrollBeforePagination) - (this.heightBeforePagination + this.currentScrollValue));
            console.log('scrollBeforePagination = ' + this.scrollBeforePagination, 'currentScrollValue = ' + this.currentScrollValue, 'scrollWrapper.scrollTop = ' + scrollWrapper.scrollTop);
            this.scrollBeforePagination = this.currentScrollValue;
            // setTimeout(() => {
                this.newLoaded = false;
            // }, 1000);
        }
    }

    addMessagesToChat(messagesToAdd: any, scroll: any = false) {
      let dialogue = document.querySelector('.dialogue');
      if (dialogue) {
          for (let message of messagesToAdd) {
              let main_div = document.createElement('div');
              main_div.classList.add(this.comS.userInfo['id'] === message['author'] ? 'dialogue-outcome' : 'dialogue-outbox');
              main_div.setAttribute('data-m-i', message['id']);

              if (scroll) {
                  dialogue.prepend(main_div);
              } else {
                  dialogue.appendChild(main_div);
              }

              let box_div = document.createElement('div');
              box_div.classList.add(this.comS.userInfo['id'] === message['author'] ? 'outcome-box' : 'outbox-box');

              main_div.appendChild(box_div);

              let content = document.createElement('span');
              content.classList.add('content');
              content.textContent = message['text'];

              box_div.appendChild(content);

              let time = document.createElement('span');
              time.classList.add('time');
              time.textContent = this.formatMessageTime(message['created_at']);

              box_div.appendChild(time);
          }
          if (!scroll) {
              this.scrollToPreviousMessage();
          } else {
              this.scrollToBottomOfChat();
          }
      } else {
          console.log('page not loaded fully yet');
      }
    }

    scrollToLast(last: any) {
      if (last) {
          setTimeout(() => {
              last['scrollIntoView']();
          }, 200);
      }
    }



}
