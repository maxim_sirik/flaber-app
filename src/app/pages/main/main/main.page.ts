import { Component, OnInit } from '@angular/core';
import {SharedService} from '../../../services/shared.service';
import {CommonService} from '../../../services/common.service';
import {ApiService} from '../../../services/api-service.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {
  public currentTab = 'schedule';
  public currentTabObj: any;
  public changedTab: any;
  public showQuickAccess = false;
  public showNewRecordModal = false;
  public tabs = {
      left: [
          {
            route: '/main/schedule',
            classNames: 'menu-bottom__schedule icon-options',
            title: this.comS.lang['tab_title_schedule'],
            tab: 'schedule',
            headerTitle: this.comS.lang['nav_title_schedule']
          },
          {
            route: '/main/records',
            classNames: 'menu-bottom__calendar icon-tascs',
            title: this.comS.lang['tab_title_records'],
            tab: 'records',
            headerTitle: this.comS.lang['nav_title_schedule']
          }
      ],
      right: [
          {
            route: '/main/messages',
            classNames: 'menu-bottom__messages icon-comment',
            title: this.comS.lang['tab_title_chat'],
            tab: 'messages',
            headerTitle: this.comS.lang['nav_title_messages']
          },
          {
            route: '/main/contacts',
            classNames: 'menu-bottom__user icon-user',
            title: this.comS.lang['tab_title_contacts'],
            tab: 'contacts',
            headerTitle: this.comS.lang['nav_title_contacts']
          }
      ]
  };

  constructor(public shared: SharedService,
              public comS: CommonService,
              public apiS: ApiService) {
      this.shared.mainTabChanged.subscribe((data) => {
          this.currentTab = data;
      });

      this.shared.showNewRecord.subscribe(
          data => {
              this.showNewRecordModal = data['show'] !== null ? data['show'] : false;
          }
      );

      this.shared.quickAccessOverlayShow.subscribe(
        overlay => {
          this.showQuickAccess = typeof overlay['show'] !== 'undefined' ? overlay['show'] : false;
        }
      );

      this.currentTabObj = this.tabs.left[0];

      
  }

  ionViewWillEnter() {
      console.log('main page');
      if (this.currentTabObj) {
          this.shared.headerMenuStatus.next({isBack: false, title: this.currentTabObj['headerTitle'], showNotif: true});
      }
  }

  ngOnInit() {}

  selectTab(obj) {
      this.comS.pushPage(obj['route']);
      this.currentTab = obj['tab'];
      this.changedTab = obj['tab'];
      this.currentTabObj = obj;
  }

  openNewRecord() {
      this.shared.newRecord.next({show: true});
  }

}
