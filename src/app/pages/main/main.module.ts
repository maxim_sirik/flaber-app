import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { MainPage } from './main/main.page';
import { SharedModule } from '../../modules/shared.module';

const routes: Routes = [
  {
    path: '',
    component: MainPage,
    redirectTo: 'records'
  },
    { path: 'contacts', loadChildren: './contacts/contacts_page.module#ContactsPageModule' },
    { path: 'schedule', loadChildren: './schedule/schedule.module#SchedulePageModule' },
    { path: 'records', loadChildren: './records/records.module#RecordsPageModule' },
    { path: 'messages', loadChildren: './messages/messages.module#MessagesPageModule' },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MainPage]
})
export class MainPageModule {}
