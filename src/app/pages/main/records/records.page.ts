import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api-service.service';
import {SharedService} from '../../../services/shared.service';
import {CommonService} from '../../../services/common.service';
import SimpleBar from 'simplebar';
import Inputmask from 'inputmask';
import 'simplebar/dist/simplebar.css';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-records',
    templateUrl: './records.page.html',
    styleUrls: ['./records.page.scss'],
})
export class RecordsPage implements OnInit {

    public im: any;
    public timeArray = [];
    public todayString = '';
    public scheduleObject: any;
    public swipeDelta: any;
    public nearestEvent: any = null;
    public currentWorkTime: any = null;
    public dayStartTime: any = null;
    public dayFinishTime: any = null;
    public containerHeight: any = 'auto';
    public headerConfig: any;
    public selectedDayBar: any;
    public selectedDate = this.comS.getMomentObject().format('YYYY-MM-DD');
    public selectedDateString = this.selectedDate ? 'start_date=' + this.selectedDate + '&end_date=' + this.selectedDate : '';
    public scrollAttempts = 0;
    public currentHour: any = new Date().getHours();

    public headerConfSub: Subscription;

    constructor(public shared: SharedService,
                public comS: CommonService,
                public apiS: ApiService) {
        this.im = new Inputmask(this.comS.phoneMask);
        this.shared.newRecordAddedHandler.subscribe(
            data => {
                if (data['added']) {
                    this.timeArray = [];
                    this.getSchedule();
                }
            }
        );

        this.headerConfig = {
            title: this.comS.lang['nav_title_records'],
            showBack: false,
            showNotif: true,
            className: 'header-triple header-shadow header-mini header-main header-calendar',
            showCalendar: true
        };

        this.shared.showNewRecord.subscribe(
            data => {
                this.headerConfig.calendarBlur = typeof data['show'] !== 'undefined' ? data['show'] : false;
            }
        );
    }

    ngOnInit() {}

    ionViewWillEnter() {
        this.todayString = this.comS.formatDateTime(new Date(this.selectedDate), 'YYYY-MM-DD');
        this.currentHour = new Date().getHours();
        if (this.comS.updateWorker()) {
            this.getSchedule();
        } else {
            this.comS.pushPage('/main');
        }

        this.headerConfig = {
            title: this.comS.lang['nav_title_records'],
            showBack: false,
            showNotif: true,
            className: 'header-triple header-shadow header-mini header-main header-calendar',
            showCalendar: true
        };
    }

    daysDiff(timeI): boolean {
        let day1 = this.comS.getMomentObject();
        let day2 = this.comS.getMomentObject(this.selectedDate).add(timeI - 1, 'days');
        return Math.ceil(this.comS.getDaysDiff(day2, day1)) < 0;
    }

    getSchedule() {
        this.dayStartTime = 0;
        this.dayFinishTime = 23;
        this.getRecordsByDay(this.selectedDateString);
    }

    fillTimeArray(before: boolean = false, initial: boolean = true, swiped: boolean = false) {
        let timeArrayItem = [];
        let keys = Object.keys(this.scheduleObject);
        if (initial) {
            this.timeArray = [];
            if (this.selectedDate === this.comS.getMomentObject().format('YYYY-MM-DD')) {
                this.getNearestEvent(keys);
            }
        }

        for (let i = this.dayStartTime; i <= this.dayFinishTime; i++) {
            timeArrayItem.push(this.getClosestFinish(i, keys));
        }
        this.timeArray[before ? 'unshift' : 'push'](timeArrayItem);

        this.initPhoneMasks();

        if (initial) {
            let dayBefore = this.comS.getMomentObject(this.selectedDate).add(-1, 'days').format('YYYY-MM-DD');
            let dayAfter = this.comS.getMomentObject(this.selectedDate).add(1, 'days').format('YYYY-MM-DD');

            this.getRecordsByDay('start_date=' + dayBefore + '&end_date=' + dayBefore, dayBefore,  true, false);
            this.getRecordsByDay('start_date=' + dayAfter + '&end_date=' + dayAfter, dayAfter, false, false);
        }

        setTimeout(() => {
            this.containerHeight = this.comS.getElementCompleteHeight('.lists-container .workflow-list:nth-child(2)', true);
        }, 1000);
    }

    initPhoneMasks() {
        let phones = document.querySelectorAll('[data-day="' + this.selectedDate + '"] [data-phone]');

        for (let i = 0; i < phones.length; i++) {
            this.im.mask(phones[i]);
            phones[i]['inputmask'].setValue(phones[i].getAttribute('data-phone').replace('380', ''));
        }
    }

    getClosestFinish(time: any, keys: any) {
        keys = this.getUpcomingEvents(time, keys);
        if (keys.length > 0) {
            let records = [];
            let showTimeLabel = false;
            keys.forEach((key) => {
                this.scheduleObject[key].records.forEach((record) => {
                    if (parseInt(key, null) === time) {
                        if (parseInt(record['start']['hour'], null) === parseInt(key, null)) {
                            records.push({time: key, status: 'start', record});
                            records.push({time: key, status: 'finish', record});
                            showTimeLabel = true;
                        } else {
                            records.push({time: key, status: 'finish', record});
                            showTimeLabel = true;
                        }
                    } else if (parseInt(key, null) > time) {
                        if (parseInt(record['start']['hour'], null) === time) {
                            records.push({time: key, status: 'start', record});
                            showTimeLabel = true;
                        } else if (parseInt(record['start']['hour'], null) < time) {
                            records.push({time: key, status: 'include', record});
                        }
                    }
                });
            });

            records.forEach((record) => {
                if ((time < parseInt(record['time'])) && (record['status'] === 'include')) {
                    showTimeLabel = false;
                }
            });

            return {
                isInTime: (records.length > 0) ? (!((records.length === 1) && (records[0].status === 'finish'))) : false,
                hour: time,
                record: records.length > 0 ? records : null,
                showTimeLabel
            };
        } else {
            return {
                isInTime: false,
                hour: time,
                record: null
            };
        }
    }

    getUpcomingEvents(time: any, keys: any) {
        let predicate = (x) => x >= time;
        return keys.filter(predicate);
    }

    getNearestEvent(keys: any) {
        this.nearestEvent = null;
        let currentTime = this.comS.formatDateTime(new Date(), 'H:mm');
        let timeSplited = currentTime.split(':');
        keys = this.getUpcomingEvents(parseInt(timeSplited[0], null), keys);

        if (keys.length > 0) {

            for (let i = 0; i < keys.length; i++) {
                for (let j = 0; j < this.scheduleObject[keys[i]].records.length; j++) {
                    let record = this.scheduleObject[keys[i]].records[j];
                    if (parseInt(record['start']['hour'], null) === parseInt(timeSplited[0], null)) {
                        if (parseInt(record['start']['minute'], null) > parseInt(timeSplited[1], null)) {
                            this.nearestEvent = record;
                            return true;
                        }
                    } else if (parseInt(record['start']['hour'], null) > parseInt(timeSplited[0], null)) {
                        this.nearestEvent = record;
                        return true;
                    }
                }
            }
        }
    }

    getRecordsByDay(dayString = '', day: any = this.selectedDate, before: boolean = false, initial: boolean = true, swiped: boolean = false) {
        this.apiS.get_records_by_date(dayString, this.comS.workerInfo['id']).subscribe(
            success => {
                if (success['status']) {
                    if (success['text'] && success['text']['result']) {
                        if (success['text']['result'][day]) {
                            this.scheduleObject = success['text']['result'][day];
                        } else {
                            this.scheduleObject = [];
                        }
                        this.fillTimeArray(before, initial, swiped);
                    }
                }
            }, error => {
                if (error.error) {
                    this.comS.showHint(error.error.text.errors);
                }
            }
        );
    }

    getIntValue(value: any) {
        return parseInt(value, null);
    }

    getRecordStatus(record: any, rec_i: any, hour: any) {
        let status = null;

        if (record['status'] === 'start') {
            status = 'current_record';
            if (this.getIntValue(record['record']['start']['minute']) > 0) {
                if (rec_i > 0) {
                    if (hour['record'][rec_i - 1]['status'] === 'finish') {
                        if ((this.getIntValue(hour['record'][rec_i - 1]['record']['finish']) < this.getIntValue(record['record']['start']['minute']))) {
                            status = 'between_records';
                        }
                    }
                } else if (rec_i === 0) {
                    status = 'first_record';
                }
            }
        } else {
            if (record['status'] === 'finish') {
                if (rec_i === hour['record'].length - 1) {
                    status = 'after_record';
                }
            }
        }
        return status;
    }

    openNewRecord(from: any = null, to: any = null) {
        this.shared.newRecord.next({show: true, time: from, finish_time: to, date: this.selectedDate});
    }

    openQuickAccess(record: any = null) {
        let obj = {
            show: true,
            record
        };
        this.comS.showQuickAcess(obj);
    }

    dateSelected(ev: any) {
        this.todayString = this.selectedDate = ev;
        this.selectedDateString = 'start_date=' + this.selectedDate + '&end_date=' + this.selectedDate;
        this.getSchedule();
    }

    dateSwiped(ev: any) {
        this.selectedDate = ev['date'];
        this.swipeDelta = ev['delta'];
        let dayBefore = this.comS.getMomentObject(this.selectedDate).add(-1, 'days').format('YYYY-MM-DD');
        let dayAfter = this.comS.getMomentObject(this.selectedDate).add(1, 'days').format('YYYY-MM-DD');

        this.comS.resetSwipeState();
        if (this.swipeDelta === 'less') {
            this.timeArray.splice(1, 2);
        } else if (this.swipeDelta === 'more') {
            this.timeArray.splice(0, 2);
        }
        this.getRecordsByDay('start_date=' + dayBefore + '&end_date=' + dayBefore, dayBefore, true, false, true);
        this.getRecordsByDay('start_date=' + dayAfter + '&end_date=' + dayAfter, dayAfter, false, false, true);
    }

    scrollToFirst() {
        let daySelector = '[data-day="' + this.selectedDate + '"] [data-rec-id]';
        let records = document.querySelectorAll(daySelector);
        if (records.length) {
            let elem = records[0] as HTMLElement;
            if (this.nearestEvent) {
                let nearestRecord = document.querySelector('[data-day="' + this.selectedDate + '"] [data-rec-id="record-' + this.nearestEvent['record_id'] +'"]');
                if (nearestRecord) {
                    this.scrollToFirstUsingBar(nearestRecord);
                } else {
                    this.scrollToFirstUsingBar(elem);
                }
            } else {
                this.scrollToFirstUsingBar(elem);
            }
        } else {
            if (this.scrollAttempts < 5) {
                setTimeout(() => {
                    this.scrollAttempts += 1;
                    this.scrollToFirst();
                }, 400);
            } else {
                this.scrollAttempts = 0;
                return false;
            }
        }
    }

    scrollToFirstUsingBar(elem: any) {
        if (this.selectedDayBar && elem) {
            let scrollElem = this.selectedDayBar.getScrollElement();

            if (scrollElem) {
                scrollElem.scrollTop = elem.offsetParent.offsetTop - 228;
            } else {
               setTimeout(() => {
                   this.scrollToFirstUsingBar(elem);
               }, 100);
            }
        }
    }


}
