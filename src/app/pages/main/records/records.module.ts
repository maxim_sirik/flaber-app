import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RecordsPage } from './records.page';
import { MainGroupSharedModule } from '../../../modules/main-group-shared.module';

const routes: Routes = [
  {
    path: '',
    component: RecordsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MainGroupSharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RecordsPage]
})
export class RecordsPageModule {}
