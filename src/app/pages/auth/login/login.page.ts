import { Component, OnInit, AfterViewInit } from '@angular/core';
import {ApiService} from '../../../services/api-service.service';
import {CommonService} from '../../../services/common.service';

import Inputmask from 'inputmask';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['../auth-section.scss'],
})
export class LoginPage implements OnInit, AfterViewInit {
  public animationPlayed = false;
  public currentComponent: any;
  // public currentComponent = 'login';

  constructor(public apiS: ApiService,
              public comS: CommonService) { }

  ngOnInit() {}

  ionViewWillEnter() {
      // this.comS.setStatusBarStyle('#F883A3');
      // this.currentComponent = 'code';
      this.currentComponent = 'login';
  }

  ionViewDidLeave() {
      this.currentComponent = null;
  }

  ngAfterViewInit() {
      setTimeout(() => {
         this.animationPlayed = true;
      }, 2000);
  }

  setCurrentComponent(ev: any) {
      this.currentComponent = ev;
  }

}
