import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {pairwise} from 'rxjs/operators';
import {CommonService} from '../../../../services/common.service';
import {ApiService} from '../../../../services/api-service.service';

@Component({
  selector: 'app-code-component',
  templateUrl: './code.component.html',
  styleUrls: ['./code.component.scss'],
})
export class CodeComponent implements OnInit {

    @Output() componentChanged = new EventEmitter();
    public signUpForm: FormGroup;
    public oneNumber = '^\\d{1,1}$';
    public codeType = null;
    public isZero = {
        '0' : true,
        '1' : true,
        '2' : true,
        '3' : true
    };
    public onCodeChangeTime = false;

    constructor(public fb: FormBuilder,
                public apiS: ApiService,
                public comS: CommonService) {
        this.signUpForm = fb.group({
            '0': ['', Validators.compose([Validators.required, Validators.pattern(this.oneNumber)])],
            '1': ['', Validators.compose([Validators.required, Validators.pattern(this.oneNumber)])],
            '2': ['', Validators.compose([Validators.required, Validators.pattern(this.oneNumber)])],
            '3': ['', Validators.compose([Validators.required, Validators.pattern(this.oneNumber)])],
        });
        this.codeType = localStorage.getItem('code_type') ? localStorage.getItem('code_type') : null;

        for (let key of Object.keys(this.signUpForm.value)) {
            this.signUpForm.get(key).valueChanges
                .pipe(pairwise())
                .subscribe(([prev, next]: [any, any]) => {
                    this.cursorBack(prev, next, key);
                });
        }

    }
    ngOnInit() {}

    cursorBack(prev, next, index) {
        this.isZero[index] = (next.length === 0);
        this.onCodeChangeTime = true;
        setTimeout(() => {
            this.onCodeChangeTime = false;
        }, 200);
    }

    confirmNumber() {
        if (this.signUpForm.valid) {
            let code = Object.values(this.signUpForm.value).join('');

            let codeObject = {
                phone: localStorage.getItem('phone'),
                sms_code: code
            };

            if (this.codeType === 'restore') {
                codeObject['restore'] = true;
            }

            this.apiS.confirm_sms(codeObject).subscribe(
                (success) => {
                    if (success['status']) {
                        // this.comS.pushPage('/login/password');
                        this.changeAuthComponent('password');
                    }
                }, (error) => {
                    this.comS.showHint(error['error']['text']['errors']);
                }
            );
        }
    }
    changeNumber() {
        this.changeAuthComponent('login');
        // this.comS.pushBack();
    }
    sendCode() {
        let smsObject = {
            phone: localStorage.getItem('phone')
        };
        if (this.codeType === 'restore') {
            smsObject['restore'] = true;
        }
        this.apiS.send_sms(smsObject).subscribe(
            (success) => {
                if (success['status']) {
                    // успешно отправлено
                } else {
                    if (success['errorCode'] === 113) {
                        this.comS.showHint(this.comS.lang['error_code_sended'] + this.comS.getTimeTo(success['text']['timestamp']) + '.');
                    }
                }
            }, (error) => {
                if (error['error'] && error['error']['errorCode'] === 113) {
                    this.comS.showHint(this.comS.lang['error_code_sended'] + this.comS.getTimeTo(error['error']['text']['timestamp']) + '.');
                }
            }
        );
    }

    validCodeSegment(control, index, ev: any) {
        let nav = [37, 39];
        if (ev.keyCode && nav.indexOf(ev.keyCode) === -1) {
            if (ev.keyCode !== 8) {
                let inputToFocus = document.querySelector('.confirmation-code .code-label:nth-child(' + (index + 2) + ') .code-input') as HTMLElement;
                if (inputToFocus) {
                    inputToFocus.focus();
                }
            } else {
                if (this.isZero[index] && !this.onCodeChangeTime) {
                    let inputToFocus = document.querySelector('.confirmation-code .code-label:nth-child(' + index + ') .code-input') as HTMLElement;
                    if (inputToFocus) {
                        inputToFocus.focus();
                    }
                }
            }
        }
    }

    changeAuthComponent(component: any) {
        this.componentChanged.next(component);
    }

}
