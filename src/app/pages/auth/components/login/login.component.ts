import {AfterViewInit, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CommonService} from '../../../../services/common.service';
import {ApiService} from '../../../../services/api-service.service';

import Inputmask from 'inputmask';
import {NotificationsService} from '../../../../services/notifications.service';
import {SharedService} from '../../../../services/shared.service';

@Component({
  selector: 'app-login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

    @Output() componentChanged = new EventEmitter();
    public isExist = false;
    public phoneValid = false;
    public password: any;
    public passwordEntered = false;
    public im: any;
    public selector: any;

    constructor(public apiS: ApiService,
                public comS: CommonService,
                public notifications: NotificationsService,
                public shared: SharedService) { }

    ngOnInit() {
        this.selector = document.querySelector('.inputMask');
        // this.im = new Inputmask(this.comS.phoneMask);/
        this.im = new Inputmask(this.comS.phoneMask, {placeholder: this.comS.phoneMask, clearMaskOnLostFocus: false, colorMask: true});
        this.im.mask(this.selector);
        // this.selector['inputmask'].setValue('0955548989');
        // console.log(this.im, this.selector['inputmask'].setvalue(''), this.selector, 'LoginComponent');
        // this.selector['inputmask']('getemptymask');
    }

    checkUser() {
        if (this.isExist) {
            this.login();
        } else {
            this.apiS.login_request({
                phone: '380' + this.selector['inputmask'].unmaskedvalue()
            }).subscribe(
                (success) => {
                    this.isExist = success['status'] !== null ? success['status'] : false;
                    localStorage.setItem('phone', '380' + this.selector['inputmask'].unmaskedvalue());
                }, (error) => {
                    localStorage.setItem('phone', '380' + this.selector['inputmask'].unmaskedvalue());
                    if (error['error'] && error['error']['errorCode'] === 108) {
                        this.comS.showHint(this.comS.lang['error_not_verified']);
                        localStorage.setItem('code_type', 'registration');
                        this.sendSmsCode();
                    } else if (error['error'] && error['error']['errorCode'] === 100) {
                        localStorage.setItem('code_type', 'registration');
                        this.comS.showHint(this.comS.lang['error_no_user']);
                        this.sendSmsCode();
                    } else if (error['error'] && error['error']['errorCode'] === 113) {
                        // localStorage.setItem('code_type', 'registration');
                        // this.comS.showHint(this.comS.lang['error_no_user']);
                        // this.sendSmsCode();
                        this.comS.showHint(this.comS.lang['error_code_sended'] + this.comS.getTimeTo(error['error']['text']['timestamp']) + '.');
                    }
                }
            );
        }
    }

    sendSmsCode() {
        this.apiS.send_sms({
            phone: '380' + this.selector['inputmask'].unmaskedvalue()
            // phone: localStorage.getItem('phone')
        }).subscribe(
            (success) => {
                if (success['status']) {
                    // this.comS.pushPage('/login/code');
                    this.changeAuthComponent('code');
                }
            }, (error) => {
                console.log(error);
                if (error['error']) {
                    if (error['error']['errorCode'] === 108) {
                        this.comS.showHint(this.comS.lang['error_number_not_verified']);
                    } else if (error['error']['errorCode'] === 111) {
                        this.comS.showHint(this.comS.lang['error_code_not_sended']);
                    } else if (error['error'] && error['error']['errorCode'] === 113) {
                        this.comS.showHint(this.comS.lang['error_code_sended'] + this.comS.getTimeTo(error['error']['text']['timestamp']) + '.');
                    }
                }
            }
        );
    }

    login() {
        this.apiS.login_request({
            phone: '380' + this.selector['inputmask'].unmaskedvalue(),
            // phone: localStorage.getItem('phone'),
            password: this.password
        }).subscribe(
            (success) => {
                if (success['status']) {
                    if (success['text'] && success['text']['success'] && success['text']['success']['token']) {
                        console.log(success, 'login');

                        localStorage.setItem('token', success['text']['success']['token']);
                        if (success['text']['success']['worker']) {
                            localStorage.setItem('worker_obj', JSON.stringify(success['text']['success']['worker']));
                            this.comS.workerInfo = success['text']['success']['worker'];
                        }
                        setTimeout(() => {
                            if (success['text']['success']['user']['firebase_uid']) {
                                this.notifications.requestPermission(success['text']['success']['user']['firebase_uid']);
                            } else {
                                this.notifications.addUserToFirebase({email: 'flaber-user-' + success['text']['success']['user']['id'] + '@mail.com', password: this.password });
                            }
                            this.shared.login.next(true);
                            this.isExist = false;
                            this.selector['inputmask'].getemptymask();
                            this.password = '';
                        }, 300);

                        this.apiS.update_user_info();
                        setTimeout(() => {
                            this.comS.pushPage('/main/records');
                        }, 500);
                        
                    }
                }
            }, (error) => {
                if (error['error'] && error['error']['errorCode'] === 109) {
                    this.comS.showHint(this.comS.lang['error_wrong_credentials']);
                } else if (error['error'] && error['error']['errorCode'] === 108) {
                    this.comS.showHint(this.comS.lang['error_not_verified']);
                    localStorage.setItem('code_type', 'registration');
                    this.sendSmsCode();
                } else if (error['error'] && error['error']['errorCode'] === 100) {
                    localStorage.setItem('code_type', 'registration');
                    this.comS.showHint(this.comS.lang['error_no_user']);
                    this.sendSmsCode();
                } else if (error['error'] && error['error']['errorCode'] === 113) {
                    this.comS.showHint(this.comS.lang['error_code_sended'] + this.comS.getTimeTo(error['error']['text']['timestamp']) + '.');
                }
            }
        );
    }

    sendRestoreCode() {
        this.apiS.send_sms(
            {
                phone: '380' + this.selector['inputmask'].unmaskedvalue(),
                // phone: localStorage.getItem('phone'),
                restore: true
            }
        ).subscribe(
            success => {
                if (success['status']) {
                    localStorage.setItem('code_type', 'restore');
                    // this.comS.pushPage('/login/code');
                    this.changeAuthComponent('code');
                } else {
                    if (success['errorCode'] === 113) {
                        this.comS.showHint(this.comS.lang['error_code_sended'] + this.comS.getTimeTo(success['text']['timestamp']) + '.');
                    }
                }
            }, error => {
                if (error['error'] && error['error']['errorCode'] === 113) {
                    this.comS.showHint(this.comS.lang['error_code_sended'] + this.comS.getTimeTo(error['error']['text']['timestamp']) + '.');
                }
            }
        );
    }

    changeAuthComponent(component: any) {
        this.componentChanged.next(component);
    }

}
