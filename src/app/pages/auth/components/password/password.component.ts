import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommonService} from '../../../../services/common.service';
import {ApiService} from '../../../../services/api-service.service';
import {SharedService} from '../../../../services/shared.service';

@Component({
  selector: 'app-password-component',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss'],
})
export class PasswordComponent implements OnInit {

    @Output() componentChanged = new EventEmitter();
    public signUpForm: FormGroup;
    public codeType = null;

    public formFields = [
        {
            control: 'password',
            type: true
        },
        {
            control: 'password_confirmation',
            type: true
        }
    ];

    constructor(public fb: FormBuilder,
                public apiS: ApiService,
                public comS: CommonService,
                public shared: SharedService) {
        this.signUpForm = fb.group({
            'password': ['', Validators.compose([Validators.required, Validators.minLength(6)])],
            'password_confirmation': ['', Validators.required],
        }, {validator: this.checkPasswords});
        this.codeType = localStorage.getItem('code_type') ? localStorage.getItem('code_type') : null;

        this.shared.alertCallbackMethodFired.subscribe(
            methodObj => {
                if (methodObj['methodName']) {
                    if (typeof this[methodObj['methodName']] !== 'undefined') {
                        this[methodObj['methodName']]();
                    }
                }
            }
        );
    }
    ngOnInit() {}

    register() {
        if (this.codeType === 'restore') {
            this.apiS.password_restore({
                phone: localStorage.getItem('phone'),
                password: this.signUpForm.controls['password'].value,
                c_password: this.signUpForm.controls['password_confirmation'].value,
                restore: true

            }).subscribe(
                success => {
                    this.comS.showHint(this.comS.lang['hint_password_changed']);
                    this.changeAuthComponent('login');
                }, error => {
                    if (error.error) {
                        this.comS.showHint(error.error.text.errors);
                    }
                }
            );
        } else {
            this.apiS.register({
                phone: localStorage.getItem('phone'),
                password: this.signUpForm.controls['password'].value
            }).subscribe(
                (success) => {
                    if (success['status']) {
                        if (success['text'] && success['text']['success']['token']) {
                            localStorage.setItem('token', success['text']['success']['token']);
                            this.apiS.update_user_info();
                            this.openAlert();
                        }
                    }
                }, (error) => {
                    if (error.error) {
                        this.comS.showHint(error['error']['text']['errors']);
                    }
                }
            );
        }
    }
    openAlert() {

        let object = {
            title: 'Поздравляем',
            text: {
                top: 'Вы только что успешно авторизовались в системе',
                message: 'Вы можете продолжить регистрацию заполнив необходимую информацию (это займёт пару минут) или перейти к ознакомлению c интерфейсом и возможностями приложения, после чего заполнить недостающее данные для завершения регистрации'
            },
            buttons: {
                array: [
                    {
                        title: 'Продолжить',
                        callback: { method: 'goToPersonal' }
                    },
                    {
                        title: 'Ознакомиться',
                        callback: { method: 'goToMain' }
                    }
                ],
                containerClassName: ''
            },
            show: true
        };

        this.comS.showAlert(object);
    }

    goToMain() {
        this.comS.pushPage('/main');
        this.comS.showAlert({show: false});
    }

    goToPersonal() {
        this.comS.pushPage('/personal-information');
        this.comS.showAlert({show: false});
    }

    checkPasswords(group: FormGroup) {
        const pass = group.controls.password.value;
        const confirmPass = group.controls.password_confirmation.value;
        return pass === confirmPass ? null : { notSame: true };
    }

    changeAuthComponent(component: any) {
        this.componentChanged.next(component);
    }

    changeFieldType(field: any) {
        field['type'] = !field['type'];
    }

}
