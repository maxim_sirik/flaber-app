import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { AuthGroupModule } from '../../modules/auth-group.module';
import { LoginPage } from './login/login.page';

const routes: Routes = [
    {
        path: '',
        component: LoginPage,
        // children:
        //     [
        //         {
        //             path: '',
        //             pathMatch: 'full',
        //             redirectTo: 'signin'
        //         },
        //         {
        //             path: 'signin',
        //             component: LoginComponent,
        //         },
        //         {
        //             path: 'code',
        //             component: CodeComponent,
        //         },
        //         {
        //             path: 'password',
        //             component: PasswordComponent,
        //         },
        //     ]
    }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    AuthGroupModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LoginPage]
})
export class LoginPageModule {}
