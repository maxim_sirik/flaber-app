document.addEventListener("DOMContentLoaded", function(event) {


    // INPUT MASK
    var selector = document.getElementsByClassName("inputMask");
    var im = new Inputmask("+(380)-99-999-99-99");

    im.mask(selector);

    // PROGRESS BAR
    function random(min, max) {
        var rand = min + Math.random() * (max + 1 - min);
        rand = Math.floor(rand);
        return rand;
    };

    setTimeout(function() {
        document.querySelector('progress').value = random(100, 100);
    }, 100);
});