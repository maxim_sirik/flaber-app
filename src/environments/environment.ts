// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
      apiKey: 'AIzaSyCyzuhb-5rnV006HAv4PYGySbs-2PoJAP4',
      authDomain: 'localhost',
      databaseURL: 'https://flaber-48461.firebaseio.com',
      projectId: 'flaber-48461',
      storageBucket: 'flaber-48461.appspot.com',
      messagingSenderId: '918616834002',
      appId: '1:918616834002:web:5d15bb61e411affe66d20b'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
